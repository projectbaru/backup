/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : db_hris_proses

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 26/05/2022 22:00:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (2, 'Rangga Adi Pratama', 'masterrangga@gmail.com', NULL, '$2y$10$r8jV2hEQhKAdfvM.Ns9OA.d97wasfVPx724RoTNdFUeZvqTf9MDH6', NULL, '2022-05-25 23:05:43', '2022-05-25 23:05:43', NULL);

-- ----------------------------
-- Table structure for wsalamatperusahaan
-- ----------------------------
DROP TABLE IF EXISTS `wsalamatperusahaan`;
CREATE TABLE `wsalamatperusahaan`  (
  `id_alamat` int NOT NULL AUTO_INCREMENT,
  `kode_alamat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kode_perusahaan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status_alamat` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `jenis_alamat` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `alamat` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kota` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_pos` int NULL DEFAULT NULL,
  `telepon` int NULL DEFAULT NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_alamat`) USING BTREE,
  UNIQUE INDEX `kode_alamat`(`kode_alamat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsalamatperusahaan
-- ----------------------------

-- ----------------------------
-- Table structure for wsgolongan
-- ----------------------------
DROP TABLE IF EXISTS `wsgolongan`;
CREATE TABLE `wsgolongan`  (
  `id_golongan` int NOT NULL AUTO_INCREMENT,
  `kode_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tingkat_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `urutan_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tingkat_posisi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `deskripsi` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `urutan_tampilan` int NULL DEFAULT NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_golongan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsgolongan
-- ----------------------------

-- ----------------------------
-- Table structure for wsgruplokasikerja
-- ----------------------------
DROP TABLE IF EXISTS `wsgruplokasikerja`;
CREATE TABLE `wsgruplokasikerja`  (
  `id_grup_lokasi_kerja` int NOT NULL AUTO_INCREMENT,
  `kode_grup_lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_grup_lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tipe_grup_lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsgruplokasikerja
-- ----------------------------

-- ----------------------------
-- Table structure for wsjabatan
-- ----------------------------
DROP TABLE IF EXISTS `wsjabatan`;
CREATE TABLE `wsjabatan`  (
  `id_jabatan` int NOT NULL AUTO_INCREMENT,
  `kode_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tipe_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_kelompok_jabatan` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `grup_jabatan` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deskripsi_jabatan` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai` date NULL DEFAULT NULL,
  `tanggal_selesai` date NULL DEFAULT NULL,
  `dari_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sampai_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `magang` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsjabatan
-- ----------------------------

-- ----------------------------
-- Table structure for wskelascabang
-- ----------------------------
DROP TABLE IF EXISTS `wskelascabang`;
CREATE TABLE `wskelascabang`  (
  `id_kelas_cabang` int NOT NULL AUTO_INCREMENT,
  `kode_kelas_cabang` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_kelas_cabang` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `area_kelas_cabang` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_kelas_cabang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wskelascabang
-- ----------------------------

-- ----------------------------
-- Table structure for wskelompokjabatan
-- ----------------------------
DROP TABLE IF EXISTS `wskelompokjabatan`;
CREATE TABLE `wskelompokjabatan`  (
  `id_kelompok_jabatan` int NOT NULL AUTO_INCREMENT,
  `kode_kelompok_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_kelompok_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `deskripsi_kelompok_jabatan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `dari_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sampai_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `grup_jabatan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tipe_jabatan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_jabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wskelompokjabatan
-- ----------------------------

-- ----------------------------
-- Table structure for wslokasikerja
-- ----------------------------
DROP TABLE IF EXISTS `wslokasikerja`;
CREATE TABLE `wslokasikerja`  (
  `id_lokasi_kerja` int NOT NULL AUTO_INCREMENT,
  `kode_lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `id_grup_lokasi_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `id_kantor` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `zona_waktu` int NULL DEFAULT NULL,
  `alamat` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `provinsi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kecamatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kelurahan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kabupaten_kota` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_pos` int NULL DEFAULT NULL,
  `negara` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nomor_telepon` int NULL DEFAULT NULL,
  `email` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `fax` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_lokasi_kerja`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wslokasikerja
-- ----------------------------

-- ----------------------------
-- Table structure for wsorganisasi
-- ----------------------------
DROP TABLE IF EXISTS `wsorganisasi`;
CREATE TABLE `wsorganisasi`  (
  `id_organisasi` int NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tipe_area` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `grup_organisasi` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `unit_kerja` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai` date NULL DEFAULT NULL,
  `tanggal_selesai` date NULL DEFAULT NULL,
  `id_induk_organisasi` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `induk_organisasi` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_struktur_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `versi_struktur_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_tingkat_organisasi` int NULL DEFAULT NULL,
  `nomor_indeks_organisasi` int NULL DEFAULT NULL,
  `tingkat_organisasi` int NULL DEFAULT NULL,
  `urutan_organisasi` int NULL DEFAULT NULL,
  `keterangan_organisasi` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `leher_struktur` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `aktif` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_organisasi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsorganisasi
-- ----------------------------

-- ----------------------------
-- Table structure for wsperusahaan
-- ----------------------------
DROP TABLE IF EXISTS `wsperusahaan`;
CREATE TABLE `wsperusahaan`  (
  `id_perusahaan` int NOT NULL AUTO_INCREMENT,
  `perusahaan_logo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `singkatan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `visi_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `misi_perusahaan` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nilai_perusahaan` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan_perusahaan` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_perusahaan` date NULL DEFAULT NULL,
  `tanggal_selesai_perusahaan` date NULL DEFAULT NULL,
  `jenis_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `jenis_bisnis_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `jumlah_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nomor_npwp_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `lokasi_pajak` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `npp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `npkp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `id_logo_perusahaan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` int NULL DEFAULT NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  `jumlah_karyawan` int NULL DEFAULT NULL,
  PRIMARY KEY (`id_perusahaan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsperusahaan
-- ----------------------------
INSERT INTO `wsperusahaan` VALUES (1, NULL, '3 Temple', '3WT', '3WT', 'mau kaya', 'cari duit', 'mau kaya', NULL, '2021-01-26', '2028-12-26', 'Abal-abal', 'Abal-abal', NULL, '743657436573465436', 'Teuing', '3453463432', '475475834759834', '1132312', 'tes', NULL, '2021-01-26', '2028-12-26', NULL, NULL, NULL, NULL, NULL, NULL, 10000000);
INSERT INTO `wsperusahaan` VALUES (2, NULL, 'a', 'dwede', 'ssad', 'dcbdsvbcjhws', 'sbaxhjbsjxjb', 'dcbdsvbcjhws', NULL, '2019-06-05', '2021-07-08', 'Abal-abal', 'Abal-abal', NULL, '346534534', 'Bandung', '23764326', '2716387326', 'dferfre', 'lorem', NULL, '2021-01-26', '2028-12-26', NULL, NULL, NULL, NULL, NULL, NULL, 10);

-- ----------------------------
-- Table structure for wsposisi
-- ----------------------------
DROP TABLE IF EXISTS `wsposisi`;
CREATE TABLE `wsposisi`  (
  `id_posisi` int NOT NULL AUTO_INCREMENT,
  `kode_perusahaan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kode_posisi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_posisi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_mpp` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tingkat_posisi` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `detail_tingkat_posisi` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kode_jabatan` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tingkat_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tipe_posisi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `deskripsi_pekerjaan_posisi` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kode_posisi_atasan` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tipe_area` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `dari_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sampai_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `posisi_aktif` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `komisi` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kepala_fungsional` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `flag_operasional` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status_posisi` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nomor_surat` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `jumlah_karyawan_dengan_posisi_ini` int NULL DEFAULT NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_posisi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wsposisi
-- ----------------------------

-- ----------------------------
-- Table structure for wstampilantabledashboarduser
-- ----------------------------
DROP TABLE IF EXISTS `wstampilantabledashboarduser`;
CREATE TABLE `wstampilantabledashboarduser`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `select` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `nama_perusahaan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kode_perusahaan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `query_field` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `query_operator` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `query_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wstampilantabledashboarduser
-- ----------------------------
INSERT INTO `wstampilantabledashboarduser` VALUES (4, 2, '[\"nama_perusahaan\",\"perusahaan_logo\",\"visi_perusahaan\",\"misi_perusahaan\"]', NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `wstampilantabledashboarduser` VALUES (5, 2, '[\"nama_perusahaan\",\"perusahaan_logo\",\"visi_perusahaan\",\"misi_perusahaan\"]', '3 Temple', NULL, '[null]', '[null]', '[null]', 0);

-- ----------------------------
-- Table structure for wstingkatgolongan
-- ----------------------------
DROP TABLE IF EXISTS `wstingkatgolongan`;
CREATE TABLE `wstingkatgolongan`  (
  `nama_perusahaan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_tingkat_golongan` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_tingkat_golongan` double NULL DEFAULT NULL,
  `urutan_tampilan` int NULL DEFAULT NULL,
  `kode_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wstingkatgolongan
-- ----------------------------

-- ----------------------------
-- Table structure for wstingkatkelompokjabatan
-- ----------------------------
DROP TABLE IF EXISTS `wstingkatkelompokjabatan`;
CREATE TABLE `wstingkatkelompokjabatan`  (
  `id_tingkat_kelompok_jabatan` int NOT NULL AUTO_INCREMENT,
  `kode_tingkat_kelompok_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_tingkat_kelompok_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `kode_kelompok_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_kelompok_jabatan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `dari_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `sampai_golongan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `keterangan` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal_mulai` date NULL DEFAULT NULL,
  `tanggal_selesai` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` date NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_kelompok_jabatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wstingkatkelompokjabatan
-- ----------------------------

-- ----------------------------
-- Table structure for wstingkatorganisasi
-- ----------------------------
DROP TABLE IF EXISTS `wstingkatorganisasi`;
CREATE TABLE `wstingkatorganisasi`  (
  `id_tingkat_organisasi` int NOT NULL AUTO_INCREMENT,
  `kode_tingkat_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `nama_tingkat_organisasi` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `urutan_tingkat` int NULL DEFAULT NULL,
  `urutan_tampilan` int NULL DEFAULT NULL,
  `keterangan` tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `status_rekaman` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal_mulai_efektif` date NULL DEFAULT NULL,
  `tanggal_selesai_efektif` date NULL DEFAULT NULL,
  `pengguna_masuk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_masuk` date NULL DEFAULT NULL,
  `pengguna_ubah` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_ubah` date NULL DEFAULT NULL,
  `pengguna_hapus` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waktu_hapus` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_organisasi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wstingkatorganisasi
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
