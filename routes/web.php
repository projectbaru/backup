<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\DB;

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// perusahaan
Route::get('/', 'HomeController@index', 'index');
Route::get('/list_p', 'p_c@index')->name('list_p');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/tambah_p','p_c@tambah')->name('tambah_p');
Route::post('/simpan_p','p_c@simpan')->name('simpan_p');
Route::post('/simpan_pd','p_c@simpan_duplikat')->name('simpan_pd');

Route::get('list_p/detail/{id_perusahaan}', 'p_c@detail')->name('detail_p');
Route::get('list_p/edit/{id_perusahaan}', 'p_c@edit')->name('edit_p');
Route::post('list_p/update', 'p_c@update')->name('update_p');
Route::get('list_p/hapus/{id_perusahaan}', 'p_c@hapus')->name('hapus_p');
Route::get('/list_p/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_p/hapus_banyak', 'p_c@multiDelete');
Route::get('/filter_perusahaan','p_c@filter')->name('ubah_tampilanperusahaan');

Route::post('/filter_result','p_c@filterSubmit')->name('update_tampilanperusahaan');
Route::delete('deleteAll', 'p_c@deleteAll');

//alamat
Route::get('/list_a', 'a_c@index')->name('list_a');
Route::get('/list_a/cari', 'a_c@cari')->name('cari_a');
Route::get('/tambah_a','a_c@tambah')->name('tambah_a');
Route::get('list_a/edit/{id_alamat}', 'a_c@edit')->name('edit_a');
Route::get('list_a/detail/{id_alamat}', 'a_c@detail')->name('detail_a');
Route::post('list_a/update', 'a_c@update')->name('update_a');

Route::post('/simpan_a','a_c@simpan')->name('simpan_a');
Route::get('/filter_alamat','a_c@filter')->name('ubah_tampilanalamat');
Route::post('/filter_alamat','a_c@filterSubmit')->name('filter_alamat');
Route::delete('deleteAll', 'a_c@deleteAll');
Route::post('list_a/hapus_banyak', 'a_c@multiDelete');
Route::get('list_a/hapus/{id_alamat}', 'a_c@hapus')->name('hapus_a');

//organisasi
Route::get('/list_o','o_c@index')->name('list_o');
Route::post('/delete_multi', 'o_c@delete_multi')->name('deletemulti_organisasi');
Route::get('/tambah_o','o_c@tambah')->name('tambah_o');
Route::get('list_o/edit/{id_organisasi}', 'o_c@edit')->name('edit_o');
Route::get('list_o/hapus/{id_organisasi}', 'o_c@hapus')->name('hapus_o');
Route::get('list_o/detail/{id_organisasi}', 'o_c@detail')->name('detail_o');
Route::post('/simpan_o', 'o_c@simpan')->name('simpan_o');
Route::get('cari_o', 'o_c@cari')->name('cari_o');
Route::post('update_o','o_c@update')->name('update_o');
Route::delete('deleteAll', 'o_c@deleteAll');
Route::get('list_o/detail/{id_organisasi}', 'o_c@detail')->name('detail_o');
Route::get('/filter_organisasi','o_c@filter')->name('ubah_tampilanorganisasi');
Route::post('/filter_submitorganisasi','o_c@filterSubmit')->name('update_tampilanorganisasi');
Route::post('/list_o/hapus_banyak', 'o_c@multiDelete');
Route::get('/set_back_display', 'o_c@reset');

Route::get('/list_to','to_c@index')->name('list_to');
Route::post('/delete_multi', 'to_c@delete_multi')->name('deletemulti_torganisasi');
Route::get('/tambah_to','to_c@tambah')->name('tambah_to');
Route::get('list_to/edit/{id_tingkat_organisasi}', 'to_c@edit')->name('edit_to');
Route::get('list_to/hapus/{id_tingkat_organisasi}', 'to_c@hapus')->name('hapus_to');
Route::get('list_to/detail/{id_tingkat_organisasi}', 'to_c@detail')->name('detail_to');
Route::post('/simpan_to', 'to_c@simpan')->name('simpan_to');
Route::get('/list_to/cari', 'to_c@cari')->name('cari_to');
Route::post('update_to','to_c@update')->name('update_to');
Route::delete('deleteAll', 'to_c@deleteAll');
Route::get('list_to/detail/{id_tingkat_organisasi}', 'to_c@detail')->name('detail_wsto');
Route::get('/filter_to','to_c@filter')->name('ubah_tampilanto');
Route::post('/filter_to','to_c@filterSubmit')->name('update_tto');
Route::post('/list_to/hapus_banyak', 'to_c@multiDelete');
Route::get('/set_back_display', 'to_c@reset');

Route::get('/list_to', 'to_c@index')->name('list_to');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');
Route::get('/tambah_wsperusahaan','p_c@tambah')->name('tambah_wsperusahaan');
Route::post('/simpan_wsperusahaan','p_c@simpan')->name('simpan_wsperusahaan');
Route::get('list_p/detail/{id_perusahaan}', 'p_c@detail')->name('detail_p');
Route::get('list_p/edit/{id_perusahaan}', 'p_c@edit')->name('edit_p');
Route::post('list_p/update', 'p_c@update')->name('update_p');
Route::get('list_p/hapus/{id_perusahaan}', 'p_c@hapus')->name('hapus_p');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('-', 'p_c@multiDelete');

Route::get('/filter_perusahaan','p_c@filter')->name('ubah_tampilanperusahaan');
Route::post('/filter_perusahaan','p_c@filterSubmit')->name('update_tampilanperusahaan');
Route::delete('deleteAll', 'p_c@deleteAll');

// golongan
Route::get('/list_g', 'g_c@index')->name('list_g');
Route::post('/delete_multi', 'g_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'g_c@allData');
Route::get('/set_back_display', 'g_c@reset');

Route::get('/list_g/tambah_g','g_c@tambah')->name('tambah_g');
Route::post('/simpan_g','g_c@simpan')->name('simpan_g');
Route::get('/list_g/detail/{id_golongan}', 'g_c@detail')->name('detail_g');
Route::get('/list_g/edit/{id_golongan}', 'g_c@edit')->name('edit_g');
Route::post('list_g/update', 'g_c@update')->name('update_g');
Route::get('list_g/hapus/{id_golongan}', 'g_c@hapus')->name('hapus_g');
Route::get('/list_g/cari', 'p_c@cari')->name('cari_g');
Route::post('/list_g/hapus_banyak', 'g_c@multiDelete');

Route::get('/filter_golongan','g_c@filter')->name('ubah_tampilangolongan');
Route::post('/filter_golongan','g_c@filterSubmit')->name('update_tampilangolongan');
Route::delete('deleteAll', 'p_c@deleteAll');


// tingkat golongan
Route::get('/list_tg', 'tg_c@index')->name('list_tg');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_semua_wstg', 'tg_c@allData')->name('alldata_tg');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/list_tg/tambah_tg','tg_c@tambah')->name('tambah_tg');
Route::post('/list_tg/simpan_tg','tg_c@simpan')->name('simpan_tg');
Route::get('/list_tg/detail/{id_tingkat_golongan}', 'tg_c@detail')->name('detail_tg');
Route::get('/list_tg/edit/{id_tingkat_golongan}', 'tg_c@edit')->name('edit_tg');
Route::post('list_tg/update/', 'tg_c@update')->name('update_tg');
Route::get('list_tg/hapus/{id_tingkat_golongan}', 'tg_c@hapus')->name('hapus_tg');
Route::get('/list_wsperusahaan/cari', 'tg_c@cari')->name('cari_wsperusahaan');
Route::post('/list_tg/hapus_banyak', 'tg_c@multiDelete');

Route::get('/filter_tg','tg_c@filter')->name('ubah_tamtg');
Route::post('/filter_tg','tg_c@filterSubmit')->name('update_tamtg');
Route::delete('deleteAll', 'p_c@deleteAll');

//tingkatposisi
Route::get('/list_tp', 'tp_c@index')->name('list_tp');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/tambah_tp','tp_c@tambah')->name('tambah_tp');
Route::post('/simpan_tp','tp_c@simpan')->name('simpan_tp');
Route::get('/list_tp/detail/{id_tingkat_posisi}', 'tp_c@detail')->name('detail_tp');
Route::get('/list_tp/edit/{id_tingkat_posisi}', 'tp_c@edit')->name('edit_tp');
Route::post('list_tp/update', 'tp_c@update')->name('update_tp');
Route::get('list_tp/hapus/{id_tingkat_posisi}', 'tp_c@hapus')->name('hapus_tp');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_tp/hapus_banyak', 'tp_c@multiDelete');

Route::get('/filter_tp','tp_c@filter')->name('ubah_tampilantp');
Route::post('/filter_tp','tp_c@filterSubmit')->name('update_tampiltp');
Route::delete('deleteAll', 'p_c@deleteAll');

//posisi
Route::get('/list_po', 'po_c@index')->name('list_po');
Route::post('/delete_multi', 'po_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'po_c@allData');
Route::get('/set_back_display', 'po_c@reset');

Route::get('/tambah_po','po_c@tambah')->name('tambah_po');
Route::post('/simpan_po','po_c@simpan')->name('simpan_po');
Route::get('list_po/detail/{id_posisi}', 'po_c@detail')->name('detail_po');
Route::get('list_po/edit/{id_posisi}', 'po_c@edit')->name('edit_po');
Route::post('list_po/update', 'po_c@update')->name('update_po');
Route::get('list_po/hapus/{id_posisi}', 'po_c@hapus')->name('hapus_po');
Route::get('/list_po/cari', 'po_c@cari')->name('cari_wsperusahaan');
Route::post('/list_po/hapus_banyak', 'po_c@multiDelete');
Route::get('/filter_posisi','po_c@filter')->name('ubah_tampilanpo');
Route::post('/filter_posisi','po_c@filterSubmit')->name('update_tampilanpo');
Route::delete('deleteAll', 'po_c@deleteAll');

//jabatan
Route::get('/list_j', 'j_c@index')->name('list_j');
Route::post('/delete_multi', 'j_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'j_c@allData');
Route::get('/set_back_display', 'j_c@reset');

Route::get('/tambah_j','j_c@tambah')->name('tambah_j');
Route::post('/simpan_j','j_c@simpan')->name('simpan_j');
// Route::get('/detail','HomeController@detail');
Route::get('list_j/detail/{id_jabatan}', 'j_c@detail')->name('detail_j');
Route::get('list_j/edit/{id_jabatan}', 'j_c@edit')->name('edit_j');
Route::post('list_j/update', 'j_c@update')->name('update_j');
Route::get('list_j/hapus/{id_perusahaan}', 'j_c@hapus')->name('hapus_j');
Route::get('/list_j/cari', 'j_c@cari')->name('cari_j');
Route::post('/list_j/hapus_banyak', 'j_c@multiDelete');

Route::get('/filter_j','j_c@filter')->name('ubah_tampilanj');
Route::post('/filter_j','j_c@filterSubmit')->name('update_tampilanj');
Route::delete('deleteAll', 'p_c@deleteAll');

//kelompok jabatan
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_kj', 'kj_c@index')->name('list_kj');
Route::post('/delete_multi', 'p_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'p_c@allData');
Route::get('/set_back_display', 'p_c@reset');

Route::get('/tambah_kj','kj_c@tambah')->name('tambah_kj');
Route::post('/simpan_kj','kj_c@simpan')->name('simpan_kj');
// Route::get('/detail','HomeController@detail');
Route::get('list_kj/detail/{id_kelompok_jabatan}', 'kj_c@detail')->name('detail_kj');
Route::get('list_kj/edit/{id_kelompok_jabatan}', 'kj_c@edit')->name('edit_kj');
Route::post('list_kj/update', 'kj_c@update')->name('update_kj');
Route::get('list_kj/hapus/{id_kelompok_jabatan}', 'kj_c@hapus')->name('hapus_kj');
Route::get('/list_wsperusahaan/cari', 'kj_c@cari')->name('cari_wsperusahaan');
Route::post('/list_kj/hapus_banyak', 'kj_c@multiDelete');

Route::get('/filter_kj','kj_c@filter')->name('ubah_tampilankj');
Route::post('/filter_kj','kj_c@filterSubmit')->name('filter_kj');
Route::delete('deleteAll', 'kj_c@deleteAll');

//lokasi kerja
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_lk', 'lk_c@index')->name('list_lk');
Route::post('/delete_multi', 'lk_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'lk_c@allData');
Route::get('/set_back_display', 'lk_c@reset');

Route::get('/tambah_lk','lk_c@tambah')->name('tambah_lk');
Route::post('/simpan_lk','lk_c@simpan')->name('simpan_lk');
// Route::get('/detail','HomeController@detail');
Route::get('list_lk/detail/{id_perusahaan}', 'lk_c@detail')->name('detail_wsperusahaan');
Route::get('list_lk/edit/{id_lokasi_kerja}', 'lk_c@edit')->name('edit_lk');
Route::post('list_lk/update', 'lk_c@update')->name('update_lk');
Route::get('list_lk/hapus/{id_lk}', 'lk_c@hapus')->name('hapus_lk');
Route::get('/list_wsperusahaan/cari', 'lk_c@cari')->name('cari_wsperusahaan');
Route::post('/list_lk/hapus_banyak', 'lk_c@multiDelete');
Route::get('/filter_lk','lk_c@filter')->name('ubah_tampilanlk');
Route::post('/filter_lk','lk_c@filterSubmit')->name('update_tampilanlk');
Route::delete('deleteAll', 'p_c@deleteAll');


//tkj
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_tkj', 'tkj_c@index')->name('list_tkj');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');

Route::get('/tambah_tkj','tkj_c@tambah')->name('tambah_tkj');
Route::post('/simpan_tkj','tkj_c@simpan')->name('simpan_tkj');
// Route::get('/detail','HomeController@detail');
Route::get('list_tkj/detail/{id_tingkat_kelompok_jabatan}', 'tkj_c@detail')->name('detail_tkj');
Route::get('list_tkj/edit/{id_tingkat_kelompok_jabatan}', 'tkj_c@edit')->name('edit_tkj');
Route::post('/list_tkj/update', 'tkj_c@update')->name('update_tkj');
Route::get('list_tkj/hapus/{id_tingkat_kelompok_jabatan}', 'tkj_c@hapus')->name('hapus_tkj');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_tkj/hapus_banyak', 'tkj_c@multiDelete');

Route::get('/filter_tkj','tkj_c@filter')->name('ubah_tampilantkj');
Route::post('/filter_tkj','tkj_c@filterSubmit')->name('update_tampilantkj');
Route::delete('deleteAll', 'p_c@deleteAll');

//upah standar minimum
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_usm', 'usm_c@index')->name('list_usm');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');
Route::get('/tambah_usm','usm_c@tambah')->name('tambah_usm');
Route::post('/simpan_usm','usm_c@simpan')->name('simpan_usm');
// Route::get('/detail','HomeController@detail');
Route::get('/list_usm/detail/{id_standar_upah_minimum}', 'usm_c@detail')->name('detail_usm');
Route::get('/list_usm/edit/{id_standar_upah_minimum}', 'usm_c@edit')->name('edit_usm');
Route::post('list_usm/update', 'usm_c@update')->name('update_usm');
Route::get('list_usm/hapus/{id_standar_upah_minimum}', 'usm_c@hapus')->name('hapus_usm');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_usm/hapus_banyak', 'usm_c@multiDelete');

Route::get('/list_usm/filter_standar_upah_minimum','usm_c@filter')->name('filter_standar_upah_minimum');
Route::post('/list_usm/update_filter','usm_c@filterSubmit')->name('updatefilter');
Route::delete('deleteAll', 'p_c@deleteAll');

//kantor cabang
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_kc', 'kc_c@index')->name('list_kc');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');

Route::get('/tambah_kc','kc_c@tambah')->name('tambah_kc');
Route::post('/simpan_kc','kc_c@simpan')->name('simpan_kc');
// Route::get('/detail','HomeController@detail');
Route::get('list_kc/detail/{id_kantor_cabang}', 'kc_c@detail')->name('detail_kc');
Route::get('list_kc/edit/{id_kelas_cabang}', 'kc_c@edit')->name('edit_kc');
Route::post('list_kc/update', 'kc_c@update')->name('update_kc');
Route::get('list_kc/hapus/{id_kelas_cabang}', 'kc_c@hapus')->name('hapus_kc');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_kc/hapus_banyak', 'kc_c@multiDelete');

Route::get('/filter_kc','kc_c@filter')->name('ubah_tampilankc');
Route::post('/filter_kc','kc_c@filterSubmit')->name('update_tampilankc');
Route::delete('deleteAll', 'p_c@deleteAll');


//kantor cabang
// Route::get('/', 'HomeController@index', 'index');
Route::get('/list_kac', 'kac_c@index')->name('list_kac');
Route::post('/delete_multi', 'tkj_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'tkj_c@allData');
Route::get('/set_back_display', 'tkj_c@reset');

Route::get('/tambah_kac','kac_c@tambah')->name('tambah_kac');
Route::post('/simpan_kac','kac_c@simpan')->name('simpan_kac');
// Route::get('/detail','HomeController@detail');
Route::get('list_kac/detail/{id_kantor}', 'kac_c@detail')->name('detail_kac');
Route::get('list_kac/edit/{id_kantor}', 'kac_c@edit')->name('edit_kac');
Route::post('list_kac/update', 'kac_c@update')->name('update_kac');
Route::get('list_kac/hapus/{id_kantor}', 'kac_c@hapus')->name('hapus_kac');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_kc/hapus_banyak', 'kc_c@multiDelete');

Route::get('/filter_kac','kc_c@filter')->name('ubah_tampilankac');
Route::post('/filter_kac','kc_c@filterSubmit')->name('update_tampilankac');
Route::delete('deleteAll', 'p_c@deleteAll');

//deskripsi pekerjaan
Route::get('/', 'HomeController@index', 'index');
Route::get('/list_dp', 'dp_c@index')->name('list_dp');
Route::get('/list_hdp', 'dp_c@index_hdp')->name('list_hdp');
Route::post('/delete_multi', 'dp_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'dp_c@allData');
Route::get('/set_back_display', 'dp_c@reset');
Route::get('list_hdp/tambah','dp_c@tambah_hdp')->name('tambah_hdp');
Route::get('list_dp/tambah','dp_c@tambah')->name('tambah_dp');
Route::post('/simpan_dp','dp_c@simpan')->name('simpan_dp');
Route::post('/simpan_hdp','dp_c@simpan_hdp')->name('simpan_hdp');
Route::get('list_dp/detail/{id_deskripsi_pekerjaan}', 'dp_c@detail_des')->name('detail_des');
Route::get('list_hdp/detail/{id_deskripsi_pekerjaan}', 'dp_c@detail')->name('detail_hdp');
Route::get('list_hdp/edit/{id_deskripsi_pekerjaan}', 'dp_c@edit_hdp')->name('edit_hdp');
Route::get('list_dp/edit/{id_deskripsi_pekerjaan}', 'dp_c@edit')->name('edit_dp');

Route::post('list_dp/update', 'dp_c@update')->name('update_dp');
Route::get('list_dp/hapus/{id_deskripsi_pekerjaan}', 'dp_c@hapus')->name('hapus_dp');
Route::get('list_dp/hapus_des/{id_deskripsi_pekerjaan}', 'dp_c@hapus')->name('hapus_des');

Route::get('/list_wsperusahaan/cari', 'dp_c@cari')->name('cari_wsperusahaan');
Route::post('/list_dp/hapus_banyak', 'dp_c@multiDelete');

Route::get('/filter_dp','dp_c@filter')->name('ubah_tamdp');
Route::post('/filter_dp','dp_c@filterSubmit')->name('update_tamdp');
Route::get('/filter_hdp','dp_c@filter_hdp')->name('ubah_tamhdp');
Route::post('/filter_hdp','dp_c@filterSubmit_hdp')->name('update_tamhdp');

Route::delete('deleteAll', 'dp_c@deleteAll');

//grup lokasi kerja
Route::get('/', 'HomeController@index', 'index');
Route::get('/list_glk', 'glk_c@index')->name('list_glk');
Route::post('/delete_multi', 'glk_c@delete_multi');
Route::get('/list_semua_wsperusahaan', 'glk_c@allData');
Route::get('/set_back_display', 'glk_c@reset');

Route::get('/tambah_glk','glk_c@tambah')->name('tambah_glk');
Route::post('/simpan_glk','glk_c@simpan')->name('simpan_glk');
// Route::get('/detail','HomeController@detail');
Route::get('list_glk/detail/{id_grup_lokasi_kerja}', 'glk_c@detail')->name('detail_glk');
Route::get('list_glk/edit/{id_grup_lokasi_kerja}', 'glk_c@edit')->name('edit_glk');
Route::post('list_glk/update', 'glk_c@update')->name('update_glk');
Route::get('list_glk/hapus/{id_grup_lokasi_kerja}', 'glk_c@hapus')->name('hapus_glk');
Route::get('/list_wsperusahaan/cari', 'p_c@cari')->name('cari_wsperusahaan');
Route::post('/list_glk/hapus_banyak', 'glk_c@multiDelete');
Route::get('/filter_glk','glk_c@filter')->name('ubah_tampilanglk');
Route::post('/filter_glk','glk_c@filterSubmit')->name('update_tampilanglk');
Route::delete('deleteAll', 'p_c@deleteAll');