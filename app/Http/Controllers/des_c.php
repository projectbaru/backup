<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class des_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsdeskripsipekerjaan")->where('id_deskripsi_pekerjaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsdeskripsipekerjaan_tanggung_jawab')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan_wewenang')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->where('wsdeskripsipekerjaan_id', $request->multiDelete[$i])->delete();
            DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function hapus($id_deskripsi_pekerjaan){
        DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->delete();
        return redirect('/list_des');
    }

    public function index(Request $request)
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsdeskripsipekerjaan')->select($select);

            if($hasPersonalTable->nama_posisi) {
                $query->where('nama_posisi', $hasPersonalTable->nama_posisi);
            }
            if($hasPersonalTable->kode_posisi) {
                $query->where('kode_posisi', $hasPersonalTable->kode_posisi);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wsdeskripsipekerjaan.filterResult', $data);
        } else {
            $wsdeskripsi = DB::table('wsdeskripsipekerjaan')->get();

            if($request->has('keyword')) {
                $wsdeskripsi = DB::table('wsdeskripsipekerjaan')
                    ->where(function($query) use ($request) {
                        $query->where('kode_posisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nama_posisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('keterangan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('tanggal_mulai_efektif', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('tanggal_selesai_efektif', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nomor_dokumen', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('edisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('tanggal_edisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nomor_revisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('tanggal_revisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nama_jabatan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nama_karyawan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('divisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('lokasi_kerja', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('fungsi_jabatan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nama_lokasi_kerja', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nama_pengawas', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('kode_posisi', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('lingkup_aktivitas', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('id_logo_perusahaan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('departemen', 'LIKE', '%'.$request->keyword.'%');
                    })
                    ->get();
            }

            return view('wsdeskripsipekerjaan.index',['wsdeskripsi'=>$wsdeskripsi]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_dp')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsdeskripsi=DB::table('wsdeskripsipekerjaan')->get();
        return view('wsdeskripsi',['wsdeskripsi'=>$wsdeskripsi]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Deskripsi Pekerjaan',
                'value' => 'id_deskripsi_pekerjaan'
            ],
            [
                'text'  => 'Kode Deskripsi',
                'value' => 'kode_deskripsi'
            ],
            [
                'text'  => 'Nama Posisi',
                'value' => 'nama_posisi'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan',
                'value' => 'deskripsi_pekerjaan'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
            [
                'text'  => 'Nomor Dokumen',
                'value' => 'nomor_dokumen'
            ],
            [
                'text'  => 'Edisi',
                'value' => 'edisi'
            ],
            [
                'text'  => 'Tanggal Edisi',
                'value' => 'tanggal_edisi'
            ],
            [
                'text'  => 'Nomor Revisi',
                'value' => 'nomor_revisi'
            ],
            [
                'text'  => 'Tanggal Revisi',
                'value' => 'tanggal_revisi'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Karyawan',
                'value' => 'nama_karyawan'
            ],
            [
                'text'  => 'Divisi',
                'value' => 'divisi'
            ],
            [
                'text'  => 'Lokasi Pajak',
                'value' => 'lokasi_pajak'
            ],
            [
                'text'  => 'Dibuat Oleh',
                'value' => 'dibuat_oleh'
            ],
            [
                'text'  => 'Diperiksa Oleh',
                'value' => 'diperiksa_oleh'
            ],
            [
                'text'  => 'Fungsi Jabatan',
                'value' => 'fungsi_jabatan'
            ],
            [
                'text'  => 'Tanggung Jawab',
                'value' => 'tanggung_jawab'
            ],
            [
                'text'  => 'Departemen',
                'value' => 'departemen'
            ],
            [
                'text'  => 'Kode Posisi',
                'value' => 'kode_posisi'
            ],
            [
                'text'  => 'Nama Lokasi Kerja',
                'value' => 'nama_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Pengawas',
                'value' => 'nama_pengawas'
            ],
            [
                'text'  => 'Lingkup Aktivitas',
                'value' => 'lingkup_aktivitas'
            ],
            [
                'text'  => 'ID Logo Perusahaan',
                'value' => 'id_logo_perusahaan'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsdeskripsipekerjaan.filter', $data);
    }

    public function detail($id_deskripsi_pekerjaan){
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $deskripsiPekerjaan = [];
        $kualifikasiJabatan = [];
        $tanggungJawab      = [];
        $wewenang           = [];

        foreach($wsdeskripsipekerjaan_tanggung_jawab as $row) {
            array_push($tanggungJawab, $row->tanggung_jawab);
            // if(!$tanggungJawab) {
            //     $tanggungJawab = $row->tanggung_jawab;
            // } else {
            //     $tanggungJawab .= ', '.$row->tanggung_jawab;
            // }
        }

        foreach($wsdeskripsipekerjaan_wewenang as $row) {
            array_push($wewenang, $row->wewenang);
            // if(!$wewenang) {
            //     $wewenang = $row->wewenang;
            // } else {
            //     $wewenang .= ', '.$row->wewenang;
            // }
        }

        foreach($wsdeskripsipekerjaan_kualifikasi_jabatan as $row) {
            array_push($kualifikasiJabatan, array("jabatan" => $row->kualifikasi_jabatan, "tipe" =>$row->tipe_kualifikasi ));
            // if(!$kualifikasiJabatan) {
            //     $kualifikasiJabatan = $row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // } else {
            //     $kualifikasiJabatan .= ', '.$row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // }
        }

        foreach($wsdeskripsipekerjaan_deskripsi_pekerjaan as $row) {
            array_push( $deskripsiPekerjaan, array(
                "deskripsi"=>$row->deskripsi_pekerjaan,
                "pdca" => $row->pdca,
                "bsc" => $row->bsc,
            ));

            // if(!$deskripsiPekerjaan) {
            //     $deskripsiPekerjaan = $row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            // } else {
            //     $deskripsiPekerjaan .= '; '.$row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            //     //pdca,bsc
            // }
        }

        $data = [
            'deskripsiPekerjaan'    => $deskripsiPekerjaan,
            'kualifikasiJabatan'    => $kualifikasiJabatan,
            'tanggungJawab'         => $tanggungJawab,
            'wewenang'              => $wewenang,
            'wsdeskripsipekerjaan'  => $wsdeskripsipekerjaan
        ];

        return view('wsdeskripsipekerjaan.detail', $data);
    }

    public function detail_des($id_deskripsi_pekerjaan){
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $deskripsiPekerjaan = [];
        $kualifikasiJabatan = [];
        $tanggungJawab      = [];
        $wewenang           = [];

        foreach($wsdeskripsipekerjaan_tanggung_jawab as $row) {
            array_push($tanggungJawab, $row->tanggung_jawab);
            // if(!$tanggungJawab) {
            //     $tanggungJawab = $row->tanggung_jawab;
            // } else {
            //     $tanggungJawab .= ', '.$row->tanggung_jawab;
            // }
        }

        foreach($wsdeskripsipekerjaan_wewenang as $row) {
            array_push($wewenang, $row->wewenang);
            // if(!$wewenang) {
            //     $wewenang = $row->wewenang;
            // } else {
            //     $wewenang .= ', '.$row->wewenang;
            // }
        }

        foreach($wsdeskripsipekerjaan_kualifikasi_jabatan as $row) {
            array_push($kualifikasiJabatan, array("jabatan" => $row->kualifikasi_jabatan, "tipe" =>$row->tipe_kualifikasi ));
            // if(!$kualifikasiJabatan) {
            //     $kualifikasiJabatan = $row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // } else {
            //     $kualifikasiJabatan .= ', '.$row->kualifikasi_jabatan.' tipe '.$row->tipe_kualifikasi;
            // }
        }

        foreach($wsdeskripsipekerjaan_deskripsi_pekerjaan as $row) {
            array_push( $deskripsiPekerjaan, array(
                "deskripsi"=>$row->deskripsi_pekerjaan,
                "pdca" => $row->pdca,
                "bsc" => $row->bsc,
            ));

            // if(!$deskripsiPekerjaan) {
            //     $deskripsiPekerjaan = $row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            // } else {
            //     $deskripsiPekerjaan .= '; '.$row->deskripsi_pekerjaan.' ; '.$row->pdca.' ; '.$row->bsc;
            //     //pdca,bsc
            // }
        }

        $data = [
            'deskripsiPekerjaan'    => $deskripsiPekerjaan,
            'kualifikasiJabatan'    => $kualifikasiJabatan,
            'tanggungJawab'         => $tanggungJawab,
            'wewenang'              => $wewenang,
            'wsdeskripsipekerjaan'  => $wsdeskripsipekerjaan
        ];

        return view('wsdeskripsipekerjaan.detail_des', $data);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_deskripsi_pekerjaan';
        } else {
            $select = ['id_deskripsi_pekerjaan', 'kode_posisi', 'nama_posisi', 'nama_kelompok_jabatan'];
        }

        $query = DB::table('wsdeskripsipekerjaan')->select($select);

        if($request->nama_posisi) {
            $query->where('nama_posisi', $request->nama_posisi);
        }

        if($request->kode_posisi) {
            $query->where('kode_posisi', $request->kode_posisi);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_dp')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_dp')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_dp')->insert([
            'kode_posisi'   => $request->kode_posisi,
            'nama_posisi'   => $request->nama_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_des');
    }

    public function simpan(Request $request)
    {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->insert([
            'kode_deskripsi'            => $request->kode_deskripsi,
            'nama_posisi'               => $request->nama_posisi,
            'keterangan'                => $request->keterangan,
            'tanggal_mulai_efektif'     => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'nomor_dokumen'             => $request->nomor_dokumen,
            'edisi'                     => $request->edisi,
            'tanggal_edisi'             => $request->tanggal_edisi ? date('Y-m-d', strtotime($request->tanggal_edisi)) : null,
            'nomor_revisi'              => $request->nomor_revisi,
            'tanggal_revisi'            => $request->tanggal_revisi ? date('Y-m-d', strtotime($request->tanggal_revisi)) : null,
            'nama_jabatan'              => $request->nama_jabatan,
            'nama_karyawan'             => $request->nama_karyawan,
            'divisi'                    => $request->divisi,
            'lokasi_kerja'              => $request->lokasi_kerja,
            'fungsi_jabatan'            => $request->fungsi_jabatan,
            'nama_lokasi_kerja'         => $request->nama_lokasi_kerja,
            'nama_pengawas'             => $request->nama_pengawas,
            'kode_posisi'               => $request->kode_posisi,
            'lingkup_aktivitas'         => $request->lingkup_aktivitas,
            'id_logo_perusahaan'        => $request->id_logo_perusahaan,
            'departemen'                => $request->departemen,
            'pengguna_masuk'            => Auth::user()->id,
            'waktu_masuk'               => date('Y-m-d')
        ]);

        $wsdeskripsipekerjaan = \DB::table('wsdeskripsipekerjaan')
            ->where('kode_deskripsi', $request->kode_deskripsi)
            ->orderBy('id_deskripsi_pekerjaan', 'DESC')
            ->first();

        if($request->tanggung_jawab && count($request->tanggung_jawab) > 0) {
            for($i = 0; $i < count($request->tanggung_jawab); $i++) {
                DB::table('wsdeskripsipekerjaan_tanggung_jawab')->insert([
                        'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                        'tanggung_jawab'            => $request->tanggung_jawab[$i]
                    ]);
            }
        }

        if($request->wewenang && count($request->wewenang) > 0) {
            for($i = 0; $i < count($request->wewenang); $i++) {
                DB::table('wsdeskripsipekerjaan_wewenang')->insert([
                        'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                        'wewenang'                  => $request->wewenang[$i]
                    ]);
            }
        }

        if($request->kualifikasi_jabatan && count($request->kualifikasi_jabatan) > 0) {
            for($i = 0; $i < count($request->kualifikasi_jabatan); $i++) {
                DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->insert([
                        'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                        'kualifikasi_jabatan'       => $request->kualifikasi_jabatan[$i],
                        'tipe_kualifikasi'          => $request->tipe_kualifikasi[$i]
                    ]);
            }
        }

        if($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
            for($i = 0; $i < count($request->deskripsi_pekerjaan); $i++) {
                DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->insert([
                        'wsdeskripsipekerjaan_id'   => $wsdeskripsipekerjaan->id_deskripsi_pekerjaan,
                        'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$i],
                        'pdca'                      => $request->pdca[$i],
                        'bsc'                       => $request->bsc[$i]
                    ]);
            }
        }

        return redirect('/list_des');
    }

    public function update(Request $request, $id)
    {
        DB::table('wsdeskripsipekerjaan_tanggung_jawab')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('wsdeskripsipekerjaan_wewenang')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->where('wsdeskripsipekerjaan_id', $id)->delete();
        DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->where('wsdeskripsipekerjaan_id', $id)->delete();

        DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id)->update([
            'kode_deskripsi'            => $request->kode_deskripsi,
            'nama_posisi'               => $request->nama_posisi,
            'keterangan'                => $request->keterangan,
            'tanggal_mulai_efektif'     => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'nomor_dokumen'             => $request->nomor_dokumen,
            'edisi'                     => $request->edisi,
            'tanggal_edisi'             => $request->tanggal_edisi ? date('Y-m-d', strtotime($request->tanggal_edisi)) : null,
            'nomor_revisi'              => $request->nomor_revisi,
            'tanggal_revisi'            => $request->tanggal_revisi ? date('Y-m-d', strtotime($request->tanggal_revisi)) : null,
            'nama_jabatan'              => $request->nama_jabatan,
            'nama_karyawan'             => $request->nama_karyawan,
            'divisi'                    => $request->divisi,
            'lokasi_kerja'              => $request->lokasi_kerja,
            'fungsi_jabatan'            => $request->fungsi_jabatan,
            'nama_lokasi_kerja'         => $request->nama_lokasi_kerja,
            'nama_pengawas'             => $request->nama_pengawas,
            'kode_posisi'               => $request->kode_posisi,
            'lingkup_aktivitas'         => $request->lingkup_aktivitas,
            'id_logo_perusahaan'        => $request->id_logo_perusahaan,
            'departemen'                => $request->departemen,
            'pengguna_ubah'             => Auth::user()->id,
            'waktu_ubah'                => date('Y-m-d')
        ]);

        if($request->tanggung_jawab && count($request->tanggung_jawab) > 0) {
            for($i = 0; $i < count($request->tanggung_jawab); $i++) {
                DB::table('wsdeskripsipekerjaan_tanggung_jawab')->insert([
                        'wsdeskripsipekerjaan_id'   => $id,
                        'tanggung_jawab'            => $request->tanggung_jawab[$i]
                    ]);
            }
        }

        if($request->wewenang && count($request->wewenang) > 0) {
            for($i = 0; $i < count($request->wewenang); $i++) {
                DB::table('wsdeskripsipekerjaan_wewenang')->insert([
                        'wsdeskripsipekerjaan_id'   => $id,
                        'wewenang'                  => $request->wewenang[$i]
                    ]);
            }
        }

        if($request->kualifikasi_jabatan && count($request->kualifikasi_jabatan) > 0) {
            for($i = 0; $i < count($request->kualifikasi_jabatan); $i++) {
                DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->insert([
                        'wsdeskripsipekerjaan_id'   => $id,
                        'kualifikasi_jabatan'       => $request->kualifikasi_jabatan[$i],
                        'tipe_kualifikasi'          => $request->tipe_kualifikasi[$i]
                    ]);
            }
        }

        if($request->deskripsi_pekerjaan && count($request->deskripsi_pekerjaan) > 0) {
            for($i = 0; $i < count($request->deskripsi_pekerjaan); $i++) {
                DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->insert([
                        'wsdeskripsipekerjaan_id'   => $id,
                        'deskripsi_pekerjaan'       => $request->deskripsi_pekerjaan[$i],
                        'pdca'                      => $request->pdca[$i],
                        'bsc'                       => $request->bsc[$i]
                    ]);
            }
        }

        return redirect('/list_des');
    }

    public function tambah() {
        
        $result = DB::table("users")->get();
        return view('wsdeskripsipekerjaan.tambah')->with("data", $result);
    }

    public function tambah_des() {
        $look = DB::table('wsposisi')->select('id_posisi','kode_posisi','nama_posisi')->get();
        $datapos['looks'] = $look;
        $result = DB::table("users")->get();
        return view('wsdeskripsipekerjaan.tambah_des',['datapos'=>$datapos]);
    }

    public function edit($id_deskripsi_pekerjaan) {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $data = [
            'id'                                        => $id_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan'                      => $wsdeskripsipekerjaan,
            'wsdeskripsipekerjaan_deskripsi_pekerjaan'  => $wsdeskripsipekerjaan_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan_kualifikasi_jabatan'  => $wsdeskripsipekerjaan_kualifikasi_jabatan,
            'wsdeskripsipekerjaan_tanggung_jawab'       => $wsdeskripsipekerjaan_tanggung_jawab,
            'wsdeskripsipekerjaan_wewenang'             => $wsdeskripsipekerjaan_wewenang
        ];
        $result = DB::table("users")->get();
        return view('wsdeskripsipekerjaan.edit', $data)->with("data", $result);
    }

    public function edit_des($id_deskripsi_pekerjaan) {
        $wsdeskripsipekerjaan = DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->get()[0];

        $wsdeskripsipekerjaan_tanggung_jawab    = DB::table('wsdeskripsipekerjaan_tanggung_jawab')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_wewenang    = DB::table('wsdeskripsipekerjaan_wewenang')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_kualifikasi_jabatan    = DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $wsdeskripsipekerjaan_deskripsi_pekerjaan    = DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')
            ->where('wsdeskripsipekerjaan_id', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)
            ->get();

        $data = [
            'id'                                        => $id_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan'                      => $wsdeskripsipekerjaan,
            'wsdeskripsipekerjaan_deskripsi_pekerjaan'  => $wsdeskripsipekerjaan_deskripsi_pekerjaan,
            'wsdeskripsipekerjaan_kualifikasi_jabatan'  => $wsdeskripsipekerjaan_kualifikasi_jabatan,
            'wsdeskripsipekerjaan_tanggung_jawab'       => $wsdeskripsipekerjaan_tanggung_jawab,
            'wsdeskripsipekerjaan_wewenang'             => $wsdeskripsipekerjaan_wewenang
        ];
        $result = DB::table("users")->get();
        return view('wsdeskripsipekerjaan.edit_des', $data)->with("data", $result);
    }

    public function delete($id_deskripsi_pekerjaan) {
        DB::table('wsdeskripsipekerjaan_tanggung_jawab')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan_wewenang')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan_kualifikasi_jabatan')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan_deskripsi_pekerjaan')->where('wsdeskripsipekerjaan_id', $id_deskripsi_pekerjaan)->delete();
        DB::table('wsdeskripsipekerjaan')->where('id_deskripsi_pekerjaan', $id_deskripsi_pekerjaan)->delete();
        return redirect('/list_des')->with('success', 'data berhasil dihapus');
    }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wsdeskripsi');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsdeskripsi")->whereIn('id_perusahaan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }

    public function cari(Request $request){
        $cari = $request->cari;
        $wsdeskripsi = DB::table('wsdeskripsipekerjaan')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('singkatan','like',"%".$cari."%")
        ->orWhere('visi_perusahaan','like',"%".$cari."%")
        ->orWhere('misi_perusahaan','like',"%".$cari."%")
        ->orWhere('nilai_perusahaan','like',"%".$cari."%")
        ->orWhere('keterangan_perusahaan','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")
        ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")
        ->orWhere('jenis_perusahaan','like',"%".$cari."%")
        ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")
        ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
        ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")
        ->orWhere('lokasi_pajak','like',"%".$cari."%")
        ->orWhere('npp','like',"%".$cari."%")
        ->orWhere('npkp','like',"%".$cari."%")
        ->orWhere('id_logo_perusahaan','like',"%".$cari."%")
        ->orWhere('keterangan','like',"%".$cari."%")
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")
        ->orWhere('pengguna_masuk','like',"%".$cari."%")
        ->orWhere('waktu_masuk','like',"%".$cari."%")
        ->orWhere('pengguna_ubah','like',"%".$cari."%")
        ->orWhere('waktu_ubah','like',"%".$cari."%")
        ->orWhere('pengguna_hapus','like',"%".$cari."%")
        ->orWhere('waktu_hapus','like',"%".$cari."%")
        ->orWhere('jumlah_karyawan','like',"%".$cari."%")
        ->paginate();

        return view('wsdeskripsi',['wsdeskripsi' =>$wsdeskripsi]);
    }

}
