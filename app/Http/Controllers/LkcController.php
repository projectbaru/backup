<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class WsAlamatController extends Controller
{
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);

            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wsalamatperusahaan')->select($select);

            if($hasPersonalTable->kode_alamat) {
                $query->where('kode_alamat', $hasPersonalTable->kode_alamat);
            }
            if($hasPersonalTable->kode_perusahaan) {
                $query->where('kode_perusahaan', $hasPersonalTable->kode_perusahaan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('wsalamat.index.filterResult', $data);
        } else {
            $wsalamat=DB::table('wsalamatperusahaan')->get();
            return view('wsalamat.index',['wsalamat'=>$wsalamat]);
        }
    }

    public function simpan(Request $request){
        DB::table('wsalamatperusahaan')->insert([
            'kode_alamat'   =>$request->kode_alamat,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'nama_perusahaan'    =>$request->nama_perusahaan,
            'status_alamat' =>$request->status_alamat,
            'jenis_alamat'  =>$request->jenis_alamat,
            'alamat'        =>$request->alamat,
            'kota'          =>$request->kota,
            'kode_pos'      =>$request->kode_pos,
            'telepon'       =>$request->telepon,
            'keterangan'=>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'pengguna_masuk' =>$request->pengguna_masuk,
            'waktu_masuk' =>$request->waktu_masuk,
            'pengguna_ubah' =>$request->pengguna_ubah,
        ]);

        DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_alamat'   => NULL,
                'kode_perusahaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_wsalamat');
    }

    public function edit($id_alamat) {
        $wsalamat = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->get();
        return view('wsalamat.editAlamat',['wsalamat'=> $wsalamat]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode Alamat',
                'value' => 'kode_alamat'
            ],
            [
                'text'  => 'Kode Perusahaan',
                'value' => 'kode_perusahaan'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Status Alamat',
                'value' => 'status_alamat'
            ],
            [
                'text'  => 'Jenis Alamat',
                'value' => 'jenis_alamat'
            ],
            [
                'text'  => 'Alamat',
                'value' => 'alamat'
            ],
            [
                'text'  => 'Kota',
                'value' => 'kota'
            ],
            [
                'text'  => 'Kode Pos',
                'value' => 'kode_pos'
            ],
            [
                'text'  => 'Telepon',
                'value' => 'telepon'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
            [
                'text'  => 'Pengguna Masuk',
                'value' => 'pengguna_masuk'
            ],
            [
                'text'  => 'Waktu Masuk',
                'value' => 'waktu_masuk'
            ],
            [
                'text'  => 'Pengguna Ubah',
                'value' => 'pengguna_ubah'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsalamat.filter', $data);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;
        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }
        } else {
            $select = ['kode_alamat', 'jenis_alamat', 'nama_perusahaan'];
        }
        $query = DB::table('wsalamatperusahaan')->select($select);
        if($request->kode_alamat) {
            $query->where('kode_alamat', $request->kode_alamat);
        }
        if($request->kode_perusahaan) {
            $query->where('kode_perusahaan', $request->kode_perusahaan);
        }
        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        DB::table('wstampilantabledashboarduser_alamat')->insert([
            'kode_alamat'   => $request->kode_alamat,
            'kode_perusahaan'   => $request->kode_perusahaan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_wsalamat');
    }
    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wsalamat');
    }

    // public function deleteAll(Request $request)
    // {
    //     $ids = $request->ids;
    //     DB::table("wsalamat")->whereIn('id_alamat',explode(",",$ids))->delete();
    //     return response()->json(['success'=>"Products Deleted successfully."]);
    // }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wsalamat=DB::table('wsalamatperusahaan')->get();
        return view('wsalamat',['wsalamat'=>$wsalamat]);
    }
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsalamatperusahaan")->where('id_alamat', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function tambah() {
        return view('wsalamat.tambahAlamat');
    }
    
}
