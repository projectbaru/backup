<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class p_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsperusahaan")->where('id_perusahaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsperusahaan')->where('id_perusahaan', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsperusahaan')->select($select);

            if($hasPersonalTable->nama_perusahaan) {
                $query->where('nama_perusahaan', $hasPersonalTable->nama_perusahaan);
            }
            if($hasPersonalTable->kode_perusahaan) {
                $query->where('kode_perusahaan', $hasPersonalTable->kode_perusahaan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wsperusahaan.filterResult', $data);
        } else {
            $wsperusahaan=DB::table('wsperusahaan')->get();
            return view('wsperusahaan.index',['wsperusahaan'=>$wsperusahaan]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsperusahaan=DB::table('wsperusahaan')->get();
        return view('wsperusahaan',['wsperusahaan'=>$wsperusahaan]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Perusahaan',
                'value' => 'id_perusahaan'
            ],
            [
                'text'  => 'Logo Perusahaan',
                'value' => 'perusahaan_logo'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Perusahaan',
                'value' => 'kode_perusahaan'
            ],
            [
                'text'  => 'Singkatan',
                'value' => 'singkatan'
            ],
            [
                'text'  => 'Visi Perusahaan',
                'value' => 'visi_perusahaan'
            ],
            [
                'text'  => 'Misi Perusahaan',
                'value' => 'misi_perusahaan'
            ],
            [
                'text'  => 'Nilai Perusahaan',
                'value' => 'nilai_perusahaan'
            ],
            [
                'text'  => 'Keterangan Perusahaan',
                'value' => 'keterangan_perusahaan'
            ],
            [
                'text'  => 'Tanggal Mulai Perusahaan',
                'value' => 'tanggal_mulai_perusahaan'
            ],
            [
                'text'  => 'Tanggal Selesai Perusahaan',
                'value' => 'tanggal_selesai_perusahaan'
            ],
            [
                'text'  => 'Jenis Perusahaan',
                'value' => 'jenis_perusahaan'
            ],
            [
                'text'  => 'Jenis Bisnis Perusahaan',
                'value' => 'jenis_bisnis_perusahaan'
            ],
            [
                'text'  => 'Jumlah Perusahaan',
                'value' => 'jumlah_perusahaan'
            ],
            [
                'text'  => 'Nomor NPWP Perusahaan',
                'value' => 'nomor_npwp_perusahaan'
            ],
            [
                'text'  => 'Lokasi Pajak',
                'value' => 'lokasi_pajak'
            ],
            [
                'text'  => 'NPP',
                'value' => 'npp'
            ],
            [
                'text'  => 'NPKP',
                'value' => 'npkp'
            ],
            [
                'text'  => 'ID Logo Perusahaan',
                'value' => 'id_logo_perusahaan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
            [
                'text'  => 'Jumlah Karyawan',
                'value' => 'jumlah_karyawan'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsperusahaan.filter', $data);
    }
    public function detail($id_perusahaan){
        $wsperusahaan = DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->get();
        return view('wsperusahaan.detail',['wsperusahaan'=> $wsperusahaan]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_perusahaan';
        } else {
            $select = ['id_perusahaan', 'nama_perusahaan', 'perusahaan_logo', 'visi_perusahaan', 'misi_perusahaan'];
        }

        $query = DB::table('wsperusahaan')->select($select);

        if($request->nama_perusahaan) {
            $query->where('nama_perusahaan', $request->nama_perusahaan);
        }

        if($request->kode_perusahaan) {
            $query->where('kode_perusahaan', $request->kode_perusahaan);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser')->insert([
            'kode_perusahaan'   => $request->kode_perusahaan,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_p');
    }
    public function simpan(Request $request){
        $logo_perusahaan = optional($request->file('logo_perusahaan'));
        $nama_file=time().".".$logo_perusahaan->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $logo_perusahaan->move($tujuan_upload,$nama_file);
        DB::table('wsperusahaan')->insert([
            'perusahaan_logo'   =>$nama_file,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'singkatan'         =>$request->singkatan,
            'visi_perusahaan'   =>$request->visi_perusahaan,
            'misi_perusahaan'   =>$request->misi_perusahaan,
            'nilai_perusahaan'  =>$request->visi_perusahaan,
            'tanggal_mulai_perusahaan'=>$request->tanggal_mulai_perusahaan,
            'tanggal_selesai_perusahaan'=>$request->tanggal_selesai_perusahaan,
            'jenis_perusahaan'=>$request->jenis_perusahaan,
            'jenis_bisnis_perusahaan'=>$request->jenis_bisnis_perusahaan,
            'jumlah_karyawan'=>$request->jumlah_karyawan,
            'nomor_npwp_perusahaan'=>$request->nomor_npwp_perusahaan,
            'lokasi_pajak'=>$request->lokasi_pajak,
            'npp' =>$request->npp,
            'npkp' =>$request->npkp,
            'id_logo_perusahaan' =>$request->id_logo_perusahaan,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_perusahaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_p');
    }

    public function simpan_duplikat(Request $request){
        // $logo_perusahaan = optional($request->file('logo_perusahaan'));
        // $nama_file=time().".".$logo_perusahaan->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $logo_perusahaan->move($tujuan_upload,$nama_file);
        DB::table('wsperusahaan')->insert([
            'perusahaan_logo'   =>$request->perusahaan_logo,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'singkatan'         =>$request->singkatan,
            'visi_perusahaan'   =>$request->visi_perusahaan,
            'misi_perusahaan'   =>$request->misi_perusahaan,
            'nilai_perusahaan'  =>$request->visi_perusahaan,
            'tanggal_mulai_perusahaan'=>$request->tanggal_mulai_perusahaan,
            'tanggal_selesai_perusahaan'=>$request->tanggal_selesai_perusahaan,
            'jenis_perusahaan'=>$request->jenis_perusahaan,
            'jenis_bisnis_perusahaan'=>$request->jenis_bisnis_perusahaan,
            'jumlah_karyawan'=>$request->jumlah_karyawan,
            'nomor_npwp_perusahaan'=>$request->nomor_npwp_perusahaan,
            'lokasi_pajak'=>$request->lokasi_pajak,
            'npp' =>$request->npp,
            'npkp' =>$request->npkp,
            'id_logo_perusahaan' =>$request->id_logo_perusahaan,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_perusahaan'   => NULL,
                'kode_perusahaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_p');
    }

    public function update(Request $request) {
        $perusahaan_logo = optional($request->file('perusahaan_logo'));
        $nama_file=time().".".$perusahaan_logo->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $perusahaan_logo->move($tujuan_upload,$nama_file);
        DB::table('wsperusahaan')->where('id_perusahaan', $request->id_perusahaan)->update([
            'id_perusahaan'         =>$request->id_perusahaan,
            'perusahaan_logo'       =>$nama_file,
            'nama_perusahaan'       =>$request->nama_perusahaan,
            'kode_perusahaan'       =>$request->kode_perusahaan,
            'singkatan'         =>$request->singkatan,
            'visi_perusahaan'   =>$request->visi_perusahaan,
            'misi_perusahaan'   =>$request->misi_perusahaan,
            'nilai_perusahaan'  =>$request->visi_perusahaan,
            'keterangan_perusahaan'=>$request->keterangan_perusahaan,
            'tanggal_mulai_perusahaan'=>$request->tanggal_mulai_perusahaan,
            'tanggal_selesai_perusahaan'=>$request->tanggal_selesai_perusahaan,
            'jenis_perusahaan'=>$request->jenis_perusahaan,
            'jenis_bisnis_perusahaan'=>$request->jenis_bisnis_perusahaan,
            'jumlah_karyawan'=>$request->jumlah_karyawan,
            'nomor_npwp_perusahaan'=>$request->nomor_npwp_perusahaan,
            'lokasi_pajak'=>$request->lokasi_pajak,
            'npp' =>$request->npp,
            'npkp' =>$request->npkp,
            'id_logo_perusahaan' =>$request->id_logo_perusahaan,
            'keterangan' =>$request->keterangan,
            'status_rekaman' =>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'pengguna_masuk'=>$request->pengguna_masuk,
            'waktu_masuk'=>$request->waktu_masuk,
            'pengguna_ubah'=>$request->pengguna_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'pengguna_hapus'=>$request->pengguna_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);

        return redirect('/list_p');
    }
    public function tambah() {
        return view('wsperusahaan.tambah');
    }
    public function edit($id_perusahaan) {
        
        $wsperusahaan = DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->get();
        return view('wsperusahaan.edit',['wsperusahaan'=> $wsperusahaan]);
    }
    public function delete($id_perusahaan) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_perusahaan){
        DB::table('wsperusahaan')->where('id_perusahaan', $id_perusahaan)->delete();
        return redirect('/list_p');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wsperusahaan');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsperusahaan")->whereIn('id_perusahaan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wsperusahaan = DB::table('wsperusahaan')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('singkatan','like',"%".$cari."%")    
        ->orWhere('visi_perusahaan','like',"%".$cari."%")    
        ->orWhere('misi_perusahaan','like',"%".$cari."%")    
        ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
        ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
        ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
        ->orWhere('lokasi_pajak','like',"%".$cari."%") 
        ->orWhere('npp','like',"%".$cari."%") 
        ->orWhere('npkp','like',"%".$cari."%") 
        ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
        ->orWhere('keterangan','like',"%".$cari."%") 
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
        ->orWhere('pengguna_masuk','like',"%".$cari."%") 
        ->orWhere('waktu_masuk','like',"%".$cari."%") 
        ->orWhere('pengguna_ubah','like',"%".$cari."%") 
        ->orWhere('waktu_ubah','like',"%".$cari."%") 
        ->orWhere('pengguna_hapus','like',"%".$cari."%") 
        ->orWhere('waktu_hapus','like',"%".$cari."%") 
        ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
        ->paginate();

        return view('wsperusahaan',['wsperusahaan' =>$wsperusahaan]);
    }

}
