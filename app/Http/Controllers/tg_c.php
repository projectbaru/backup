<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class tg_c extends Controller
{
    public function index(Request $request) {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);

            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wstingkatgolongan')->select($select);

            if($hasPersonalTable->nama_tingkat_golongan) {
                $query->where('nama_tingkat_golongan', $hasPersonalTable->nama_tingkat_golongan);
            }
            if($hasPersonalTable->kode_tingkat_golongan) {
                $query->where('kode_tingkat_golongan', $hasPersonalTable->kode_tingkat_golongan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('wstingkatgolongan.filterResult', $data);
        } else {
            $wstingkatgolongan = DB::table('wstingkatgolongan')->get();

            if($request->has('keyword')) {
                $wstingkatgolongan = DB::table('wstingkatgolongan')
                    ->where(function($query) use ($request) {
                        $query->where('nama_perusahaan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('kode_tingkat_golongan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('nama_tingkat_golongan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('urutan_tampilan', 'LIKE', '%'.$request->keyword.'%')
                            ->orWhere('keterangan', 'LIKE', '%'.$request->keyword.'%');
                    })
                    ->get();
            }

            $data = [
                'wstingkatgolongan' => $wstingkatgolongan
            ];

            return view('wstingkatgolongan.index', $data);
        }
    }

    public function detail($id_tingkat_golongan)
    {
        $wstingkatgolongan = DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->first();

        $items = '';

        if($wstingkatgolongan->items) {
            $selectedItems = str_replace('[', '', $wstingkatgolongan->items);
            $selectedItems = str_replace(']', '', $selectedItems);
            $selectedItems = str_replace('"', '', $selectedItems);
            $selectedItems = explode(',', $selectedItems);

            $query = DB::table('wstingkatgolongan_item')->whereIn('id', $selectedItems)->get();

            foreach($query as $row) {
                if($items == '') {
                    $items .= $row->name;
                } else {
                    $items .= ', '.$row->name;
                }
            }

            $items .= '.';
        }

        $data = [
            'items' => $items,
            'wstingkatgolongan' => $wstingkatgolongan
        ];


        return view('wstingkatgolongan.detail', $data);
    }

    public function tambah() {
        $items = DB::table('wstingkatgolongan_item')->get();

        $data = [
            'items' => $items
        ];

        return view('wstingkatgolongan.tambah', $data);
    }

    public function simpan(Request $request) {
        $items = null;

        if($request->items) {
            $items = explode(',', $request->items);
            $items = json_encode($items);
        }

        DB::table('wstingkatgolongan')->insert([
            'nama_perusahaan'   => $request->nama_perusahaan,
            'kode_tingkat_golongan'   => $request->kode_tingkat_golongan,
            'nama_tingkat_golongan'    => $request->nama_tingkat_golongan,
            'urutan_tampilan'   => $request->urutan_tampilan,
            // 'tanggal_mulai'   => $request->tanggal_mulai ? date('Y-m-d', strtotime($request->tanggal_mulai)) : null,
            'tanggal_mulai_efektif'   => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'keterangan'   => $request->keterangan,
            'items'   => $items,
            'pengguna_masuk' => Auth::user()->id,
            'waktu_masuk' => date('Y-m-d')
        ]);

        return redirect('/list_tg');
    }

    public function edit($id_tingkat_golongan) {
        $wstingkatgolongan = DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->first();

        $selectedItems = '';
        $selectedOptionItems = [];

        if($wstingkatgolongan->items) {
            $selectedItems = str_replace('[', '', $wstingkatgolongan->items);
            $selectedItems = str_replace(']', '', $selectedItems);
            $selectedItems = str_replace('"', '', $selectedItems);

            $selectedOptionItems = explode(',', $selectedItems);
        }

        $items = DB::table('wstingkatgolongan_item')->get();

        $data = [
            'id' => $id_tingkat_golongan,
            'items' => $items,
            'selectedItems' => $selectedItems,
            'selectedOptionItems' => $selectedOptionItems,
            'wstingkatgolongan' => $wstingkatgolongan
        ];


        return view('wstingkatgolongan.edit', $data);
    }

    public function update(Request $request, $id_tingkat_golongan) {
        $items = null;

        if($request->items) {
            $items = explode(',', $request->items);
            $items = json_encode($items);
        }

        DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->update([
            'id_tingkat_golongan' => $request->id_tingkat_golongan,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'kode_tingkat_golongan'   => $request->kode_tingkat_golongan,
            'nama_tingkat_golongan'    => $request->nama_tingkat_golongan,
            'urutan_tampilan'   => $request->urutan_tampilan,
            'tanggal_mulai'   => $request->tanggal_mulai ? date('Y-m-d', strtotime($request->tanggal_mulai)) : null,
            'tanggal_mulai_efektif'   => $request->tanggal_mulai_efektif ? date('Y-m-d', strtotime($request->tanggal_mulai_efektif)) : null,
            'tanggal_selesai_efektif'   => $request->tanggal_selesai_efektif ? date('Y-m-d', strtotime($request->tanggal_selesai_efektif)) : null,
            'keterangan'   => $request->keterangan,
            'items'   => $items,
            'pengguna_ubah' => Auth::user()->id,
            'waktu_ubah' => date('Y-m-d')
        ]);

        return redirect('/list_tg');
    }

    public function delete($id_tingkat_golongan)
    {
      
        DB::table("wstingkatgolongan")->where('id_tingkat_golongan', $id_tingkat_golongan)->delete();
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function hapus($id_tingkat_golongan){
       
        DB::table('wstingkatgolongan')->where('id_tingkat_golongan', $id_tingkat_golongan)->delete();
        return redirect('/list_tg');
    }

    public function multiDelete(Request $request) {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        foreach ($request->multiDelete as $id_tingkat_golongan) {
            DB::table("wstingkatgolongan")->where('id_tingkat_golongan', $id_tingkat_golongan)->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Tingkat Golongan',
                'value' => 'kode_tingkat_golongan'
            ],
            [
                'text'  => 'Nama Tingkat Golongan',
                'value' => 'nama_tingkat_golongan'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Kode Golongan',
                'value' => 'kode_golongan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wstingkatgolongan.filter', $data);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;
        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }
            $select[] = 'id_tingkat_golongan';

        } else {
            $select = ['id_tingkat_golongan','nama_tingkat_golongan', 'kode_tingkat_golongan', 'nama_perusahaan'];
        }
        $query = DB::table('wstingkatgolongan')->select($select);
        if($request->kode_tingkat_golongan) {
            $query->where('kode_tingkat_golongan', $request->kode_tingkat_golongan);
        }
        if($request->nama_tingkat_golongan) {
            $query->where('nama_tingkat_golongan', $request->nama_tingkat_golongan);
        }
        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tg')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        DB::table('wstampilantabledashboarduser_tg')->insert([
            'kode_tingkat_golongan'  => $request->kode_tingkat_golongan,
            'nama_tingkat_golongan'  => $request->nama_tingkat_golongan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_tg');
    }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tg')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_tg');
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tg')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tg')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wstingkatgolongan=DB::table('wstingkatgolongan')->get();
        return view('wstingkatgolongan.index',['wstingkatgolongan'=>$wstingkatgolongan]);
    }

}
