<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class a_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsalamatperusahaan")->where('id_alamat', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsalamatperusahaan')->where('id_alamat', $request->multiDelete[$i])->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus..']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsalamatperusahaan')->select($select);

            if($hasPersonalTable->kode_alamat) {
                $query->where('kode_alamat', $hasPersonalTable->kode_alamat);
            }
            if($hasPersonalTable->kode_perusahaan) {
                $query->where('kode_perusahaan', $hasPersonalTable->kode_perusahaan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wsalamat.filterResult', $data);
        } else {
            $wsalamat=DB::table('wsalamatperusahaan')->get();
            return view('wsalamat.index',['wsalamat'=>$wsalamat]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsalamat=DB::table('wsalamatperusahaan')->get();
        return view('wsalamat',['wsalamat'=>$wsalamat]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode Alamat',
                'value' => 'kode_alamat'
            ],
            [
                'text'  => 'Kode Perusahaan',
                'value' => 'kode_perusahaan'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Status Alamat',
                'value' => 'status_alamat'
            ],
            [
                'text'  => 'Jenis Alamat',
                'value' => 'jenis_alamat'
            ],
            [
                'text'  => 'Alamat',
                'value' => 'alamat'
            ],
            [
                'text'  => 'Kota',
                'value' => 'kota'
            ],
            [
                'text'  => 'Kode Pos',
                'value' => 'kode_pos'
            ],
            [
                'text'  => 'Telepon',
                'value' => 'telepon'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ],
            [
                'text'  => 'Pengguna Masuk',
                'value' => 'pengguna_masuk'
            ],
            [
                'text'  => 'Waktu Masuk',
                'value' => 'waktu_masuk'
            ],
            [
                'text'  => 'Pengguna Ubah',
                'value' => 'pengguna_ubah'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsalamat.filter', $data);
    }
    public function detail($id_alamat){
        $wsalamat = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->get();
        return view('wsalamat.detail',['wsalamat'=> $wsalamat]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_alamat';
        } else {
            $select = ['id_alamat', 'kode_alamat', 'jenis_alamat', 'nama_perusahaan'];
        }
        $query = DB::table('wsalamatperusahaan')->select($select);

        if($request->kode_alamat) {
            $query->where('kode_alamat', $request->kode_alamat);
        }

        if($request->kode_perusahaan) {
            $query->where('kode_perusahaan', $request->kode_perusahaan);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_alamat')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_alamat')->insert([
            'kode_alamat'   => $request->kode_perusahaan,
            'kode_perusahaan'   => $request->nama_perusahaan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_a');
    }
    public function simpan(Request $request){
        DB::table('wsalamatperusahaan')->insert([
            'kode_alamat'   =>$request->kode_alamat,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'status_alamat'   =>$request->status_alamat,
            'jenis_alamat'   =>$request->jenis_alamat,
            'alamat'  =>$request->alamat,
            'kota'  =>$request->provinsi,
            'kode_pos'=>$request->kode_pos,
            'telepon'=>$request->telepon,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_alamat')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_alamat'   => NULL,
                'kode_perusahaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_a');
    }

    public function update(Request $request) {
        DB::table('wsalamatperusahaan')->where('id_alamat', $request->id_alamat)->update([
            'kode_alamat'   =>$request->kode_alamat,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'status_alamat'   =>$request->status_alamat,
            'jenis_alamat'   =>$request->jenis_alamat,
            'alamat'  =>$request->alamat,
            'kota'  =>$request->kota,
            'kode_pos'=>$request->kode_pos,
            'telepon'=>$request->telepon,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        return redirect('/list_a');
    }
    public function tambah() {
        $look = DB::table('wsperusahaan')->select('id_perusahaan','kode_perusahaan')->get();
        $data['looks'] = $look;
        return view('wsalamat.tambah',['data'=>$data]);
    }
    public function edit($id_alamat) {
        $look = DB::table('wsperusahaan')->select('id_perusahaan','kode_perusahaan')->get();
        $datas['looks'] = $look;
        $wsalamat = DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->get();
        return view('wsalamat.edit',['wsalamat'=> $wsalamat],['datas'=>$datas]);
    }
    public function delete($id_alamat) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_alamat){
        DB::table('wsalamatperusahaan')->where('id_alamat', $id_alamat)->delete();
        return redirect('/list_a');
    }
    // public function deleteAll($id_alamat){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_wsalamat');
    }
    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsalamatperusahaan")->whereIn('id_alamatperusahaan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wsalamat = DB::table('wsalamatperusahaan')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('singkatan','like',"%".$cari."%")    
        ->orWhere('visi_perusahaan','like',"%".$cari."%")    
        ->orWhere('misi_perusahaan','like',"%".$cari."%")    
        ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
        ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
        ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
        ->orWhere('lokasi_pajak','like',"%".$cari."%") 
        ->orWhere('npp','like',"%".$cari."%") 
        ->orWhere('npkp','like',"%".$cari."%") 
        ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
        ->orWhere('keterangan','like',"%".$cari."%") 
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
        ->orWhere('pengguna_masuk','like',"%".$cari."%") 
        ->orWhere('waktu_masuk','like',"%".$cari."%") 
        ->orWhere('pengguna_ubah','like',"%".$cari."%") 
        ->orWhere('waktu_ubah','like',"%".$cari."%") 
        ->orWhere('pengguna_hapus','like',"%".$cari."%") 
        ->orWhere('waktu_hapus','like',"%".$cari."%") 
        ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
        ->paginate();
        return view('wsalamatperusahaan',['wsalamatperusahaan' =>$wsalamat]);
    }

}
