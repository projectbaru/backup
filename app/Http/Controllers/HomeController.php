<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('home');
    }
    public function tologin(){
        return view('tologin');
    }

}
