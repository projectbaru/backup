<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class usm_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsusm")->where('id_perusahaan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsstandarupahminimum')->where('id_standar_upah_minimum', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_usm')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsstandarupahminimum')->select($select);

            if($hasPersonalTable->kode_posisi) {
                $query->where('kode_posisi', $hasPersonalTable->kode_posisi);
            }
            if($hasPersonalTable->nama_posisi) {
                $query->where('nama_posisi', $hasPersonalTable->nama_posisi);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('wsstandarupahminimum.filterResult', $data);
        } else {
            $wsusm=DB::table('wsstandarupahminimum')->get();
            return view('wsstandarupahminimum.index',['wsusm'=>$wsusm]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsusm=DB::table('wsstandarupahminimum')->get();
        return view('wsusm',['wsusm'=>$wsusm]);
    }

    public function filter()
    {
        $fields = [
            
            [
                'text'  => 'Kode Posisi',
                'value' => 'kode_posisi'
            ],
            [
                'text'  => 'Nama Posisi',
                'value' => 'nama_posisi'
            ],
            [
                'text'  => 'Tingkat Pendidikan',
                'value' => 'tingkat_pendidikan'
            ],
            [
                'text'  => 'Kode Lokasi',
                'value' => 'kode_lokasi'
            ],
            [
                'text'  => 'Nama Lokasi',
                'value' => 'nama_lokasi'
            ],
            [
                'text'  => 'Upah Minimum',
                'value' => 'upah_minimum'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]  
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsstandarupahminimum.filter', $data);
    }
    public function detail($id_standar_upah_minimum){
        $wsusm = DB::table('wsstandarupahminimum')->where('id_standar_upah_minimum', $id_standar_upah_minimum)->get();
        return view('wsstandarupahminimum.detail',['wsusm'=> $wsusm]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_standar_upah_minimum';
        } else {
            $select = ['kode_posisi', 'nama_posisi', 'tingkat_pendidikan', 'kode_lokasi', 'nama_lokasi', 'upah_minimum'];
        }

        $query = DB::table('wsstandarupahminimum')->select($select);

        if($request->kode_posisi) {
            $query->where('kode_posisi', $request->kode_posisi);
        }

        if($request->nama_posisi) {
            $query->where('nama_posisi', $request->nama_posisi);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_usm')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_usm')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_usm')->insert([
            'kode_posisi'   => $request->kode_posisi,
            'nama_posisi'   => $request->nama_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_usm');
    }
    public function simpan(Request $request){
        // $logo_perusahaan = $request->file('logo_perusahaan');
        // $nama_file=time().".".$logo_perusahaan->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $logo_perusahaan->move($tujuan_upload,$nama_file);
        DB::table('wsstandarupahminimum')->insert([
            'kode_posisi'   =>$request->kode_posisi,
            'nama_posisi'   =>$request->nama_posisi,
            'tingkat_pendidikan' =>$request->tingkat_pendidikan,
            'kode_lokasi'   =>$request->kode_lokasi,
            'nama_lokasi'   =>$request->nama_lokasi,
            'upah_minimum'  =>$request->upah_minimum,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
        ]);

        DB::table('wstampilantabledashboarduser_usm')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_posisi'   => NULL,
                'nama_posisi'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_usm');
    }

    public function update(Request $request) {
        DB::table('wsstandarupahminimum')->where('id_standar_upah_minimum', $request->id_standar_upah_minimum)->update([
            'id_standar_upah_minimum'         =>$request->id_standar_upah_minimum,
            'kode_posisi'           =>$request->kode_posisi,
            'nama_posisi'           =>$request->nama_posisi,
            'tingkat_pendidikan'    =>$request->tingkat_pendidikan,
            'kode_lokasi'           =>$request->kode_lokasi,
            'nama_lokasi'               =>$request->nama_lokasi,
            'upah_minimum'              =>$request->upah_minimum,
            'tanggal_mulai_efektif'     =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_usm');
    }
    public function tambah() {
        $lookp = DB::table('wsposisi')->select('id_posisi','nama_posisi')->get();
        $posisi['looks'] = $lookp;
        $lookl = DB::table('wsgruplokasikerja')->select('id_grup_lokasi_kerja','kode_grup_lokasi_kerja','nama_grup_lokasi_kerja')->get();
        $lokasi['looks'] = $lookl;
        return view('wsstandarupahminimum.tambah',['posisi'=>$posisi],['lokasi'=>$lokasi]);
    }
    public function edit($id_standar_upah_minimum) {
        $lookp = DB::table('wsposisi')->select('id_posisi','nama_posisi')->get();
        $posisi['looks'] = $lookp;
        $lookl = DB::table('wsgruplokasikerja')->select('id_grup_lokasi_kerja','nama_grup_lokasi_kerja','kode_grup_lokasi_kerja')->get();
        $lokasi['looks'] = $lookl;
        $wsusm = DB::table('wsstandarupahminimum')->where('id_standar_upah_minimum', $id_standar_upah_minimum)->get();
        return view('wsstandarupahminimum.edit',['wsusm'=> $wsusm,'posisi'=> $posisi,'lokasi'=> $lokasi]);
    }
    public function delete($id_perusahaan) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_standar_upah_minimum){
        DB::table('wsstandarupahminimum')->where('id_standar_upah_minimum', $id_standar_upah_minimum)->delete();
        return redirect('/list_usm');
    }
    // public function deleteAll($id_perusahaan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wsusm');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsusm")->whereIn('id_perusahaan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wsusm = DB::table('wsstandarupahminimum')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('singkatan','like',"%".$cari."%")    
        ->orWhere('visi_perusahaan','like',"%".$cari."%")    
        ->orWhere('misi_perusahaan','like',"%".$cari."%")    
        ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
        ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
        ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
        ->orWhere('lokasi_pajak','like',"%".$cari."%") 
        ->orWhere('npp','like',"%".$cari."%") 
        ->orWhere('npkp','like',"%".$cari."%") 
        ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
        ->orWhere('keterangan','like',"%".$cari."%") 
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
        ->orWhere('pengguna_masuk','like',"%".$cari."%") 
        ->orWhere('waktu_masuk','like',"%".$cari."%") 
        ->orWhere('pengguna_ubah','like',"%".$cari."%") 
        ->orWhere('waktu_ubah','like',"%".$cari."%") 
        ->orWhere('pengguna_hapus','like',"%".$cari."%") 
        ->orWhere('waktu_hapus','like',"%".$cari."%") 
        ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
        ->paginate();

        return view('wsusm',['wsusm' =>$wsusm]);
    }

}
