<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class glk_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsgruplokasikerja")->where('id_grup_lokasi_kerja', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsgruplokasikerja')->where('id_grup_lokasi_kerja', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_glk')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsgruplokasikerja')->select($select);

            if($hasPersonalTable->kode_grup_lokasi_kerja) {
                $query->where('kode_grup_lokasi_kerja', $hasPersonalTable->kode_grup_lokasi_kerja);
            }
            if($hasPersonalTable->nama_grup_lokasi_kerja) {
                $query->where('nama_grup_lokasi_kerja', $hasPersonalTable->nama_grup_lokasi_kerja);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wsgruplokasikerja.filterResult', $data);
        } else {
            $wsgruplokasikerja=DB::table('wsgruplokasikerja')->get();
            return view('wsgruplokasikerja.index',['wsgruplokasikerja'=>$wsgruplokasikerja]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_glk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsgruplokasikerja=DB::table('wsgruplokasikerja')->get();
        return view('wsgruplokasikerja',['wsgruplokasikerja'=>$wsgruplokasikerja]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Grup Lokasi Kerja',
                'value' => 'id_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Kode Grup Lokasi Kerja',
                'value' => 'kode_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Grup Lokasi Kerja',
                'value' => 'nama_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Tipe Grup Lokasi Kerja',
                'value' => 'tipe_grup_lokasi_kerja'
            ],
            [
                'text'  => 'Lokasi Kerja',
                'value' => 'lokasi_kerja'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsgruplokasikerja.filter', $data);
    }
    public function detail($id_grup_lokasi_kerja){
        $wsgruplokasikerja = DB::table('wsgruplokasikerja')->where('id_grup_lokasi_kerja', $id_grup_lokasi_kerja)->get();
        return view('wsgruplokasikerja.detail',['wsgruplokasikerja'=> $wsgruplokasikerja]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_grup_lokasi_kerja';
        } else {
            $select = ['id_grup_lokasi_kerja', 'kode_grup_lokasi_kerja', 'nama_grup_lokasi_kerja'];
        }

        $query = DB::table('wsgruplokasikerja')->select($select);

        if($request->kode_grup_lokasi_kerja) {
            $query->where('kode_grup_lokasi_kerja', $request->kode_grup_lokasi_kerja);
        }

        if($request->nama_grup_lokasi_kerja) {
            $query->where('nama_grup_lokasi_kerja', $request->nama_grup_lokasi_kerja);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_glk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_glk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_glk')->insert([
            'kode_grup_lokasi_kerja'   => $request->kode_grup_lokasi_kerja,
            'nama_grup_lokasi_kerja'   => $request->nama_grup_lokasi_kerja,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_glk');
    }
    public function simpan(Request $request){
      
        DB::table('wsgruplokasikerja')->insert([
            'kode_grup_lokasi_kerja'   =>$request->kode_grup_lokasi_kerja,
            'nama_grup_lokasi_kerja'   =>$request->nama_grup_lokasi_kerja,
            'tipe_grup_lokasi_kerja'    =>$request->tipe_grup_lokasi_kerja,
            'lokasi_kerja'               =>$request->lokasi_kerja,
            'keterangan'               =>$request->keterangan,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'  =>$request->tanggal_selesai_efektif,
        ]);
        DB::table('wstampilantabledashboarduser_glk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_grup_lokasi_kerja'   => NULL,
                'nama_grup_lokasi_kerja'   => NULL,
                'query_field'              => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_glk');
    }
    public function update(Request $request) {
        DB::table('wsgruplokasikerja')->where('id_grup_lokasi_kerja', $request->id_grup_lokasi_kerja)->update([
            'id_grup_lokasi_kerja'         =>$request->id_grup_lokasi_kerja,
            'kode_grup_lokasi_kerja'       =>$request->kode_grup_lokasi_kerja,
            'nama_grup_lokasi_kerja'       =>$request->nama_grup_lokasi_kerja,
            'tipe_grup_lokasi_kerja'         =>$request->tipe_grup_lokasi_kerja,
            'lokasi_kerja'              =>$request->lokasi_kerja,
            'keterangan'               =>$request->keterangan,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'  =>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_glk');
    }
    public function tambah() {
        $look = DB::table('wslokasikerja')->select('id_lokasi_kerja','nama_lokasi_kerja')->get();
        $data['looks'] = $look;
        return view('wsgruplokasikerja.tambah',['data'=>$data]);
    }
    public function edit($id_grup_lokasi_kerja) {
        $look = DB::table('wslokasikerja')->select('id_lokasi_kerja','nama_lokasi_kerja')->get();
        $datas['looks'] = $look;
        $wsgruplokasikerja = DB::table('wsgruplokasikerja')->where('id_grup_lokasi_kerja', $id_grup_lokasi_kerja)->get();
        return view('wsgruplokasikerja.edit',['wsgruplokasikerja'=> $wsgruplokasikerja, 'datas' => $datas]);
    }
    public function delete($id_grup_lokasi_kerja) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_grup_lokasi_kerja){
        DB::table('wsgruplokasikerja')->where('id_grup_lokasi_kerja', $id_grup_lokasi_kerja)->delete();
        return redirect('/list_glk');
    }
    

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_glk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_glk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_glk');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsgruplokasikerja")->whereIn('id_grup_lokasi_kerja',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wsgruplokasikerja = DB::table('wsgruplokasikerja')
        ->where('kode_grup_lokasi_kerja','like',"%".$cari."%")
        ->orWhere('nama_grup_lokasi_kerja','like',"%".$cari."%")
        ->orWhere('tipe_grup_lokasi_kerja','like',"%".$cari."%")    
        ->orWhere('lokasi_kerja','like',"%".$cari."%")    
        ->orWhere('keterangan','like',"%".$cari."%")    
        ->orWhere('status_rekaman','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")    
        ->paginate();

        return view('wsgruplokasikerja',['wsgruplokasikerja' =>$wsgruplokasikerja]);
    }
}
