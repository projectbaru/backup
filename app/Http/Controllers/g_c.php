<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;


class g_c extends Controller
{
    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsgolongan')->where('id_golongan', $request->multiDelete[$i])->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_golongan')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);

            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wsgolongan')->select($select);

            if($hasPersonalTable->kode_golongan) {
                $query->where('kode_golongan', $hasPersonalTable->kode_golongan);
            }
            if($hasPersonalTable->nama_golongan) {
                $query->where('nama_golongan', $hasPersonalTable->nama_golongan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('wsgolongan.filterResult', $data);
        } else {
            $wsgolongan=DB::table('wsgolongan')->get();
            return view('wsgolongan.index',['wsgolongan'=>$wsgolongan]);
        }
    }

    public function simpan(Request $request){
        DB::table('wsgolongan')->insert([
            'kode_golongan'   =>$request->kode_golongan,
            'nama_golongan'   =>$request->nama_golongan,
            'tingkat_golongan'    =>$request->tingkat_golongan,
            'urutan_golongan' =>$request->urutan_golongan,
            'urutan_tampilan'  =>$request->urutan_tampilan,
            'tingkat_posisi'        =>$request->tingkat_posisi,
            'jabatan'          =>$request->jabatan,
            'deskripsi'      =>$request->deskripsi,
            'tanggal_mulai_efektif'       =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
            
        ]);

        DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_golongan'   => NULL,
                'nama_golongan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_g');
    }

    public function edit($id_golongan) {
        $wsgolongan = DB::table('wsgolongan')->where('id_golongan', $id_golongan)->get();
        return view('wsgolongan.edit',['wsgolongan'=> $wsgolongan]);
    }

    public function detail($id_golongan) {
        $wsgolongan = DB::table('wsgolongan')->where('id_golongan', $id_golongan)->get();
        return view('wsgolongan.detail',['wsgolongan'=> $wsgolongan]);
    }

    public function hapus($id_golongan){
        DB::table('wsgolongan')->where('id_golongan', $id_golongan)->delete();
        return redirect('/list_g');
    } 

    public function filter_tambah()
    {
        $fields_tambah = [
            [
                'text'  => 'Kode Golongan',
                'value' => 'kode_golongan'
            ],
            [
                'text'  => 'Nama Golongan',
                'value' => 'nama_golongan'
            ],
            [
                'text'  => 'Tingkat Golongan',
                'value' => 'tingkat_golongan'
            ],
            [
                'text'  => 'Urutan Golongan',
                'value' => 'urutan_golongan'
            ],
            [
                'text'  => 'Tingkat Posisi',
                'value' => 'tingkat_posisi'
            ],
            [
                'text'  => 'Jabatan',
                'value' => 'jabatan'
            ],
            [
                'text'  => 'Deskripsi',
                'value' => 'deskripsi'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields_tambah,
            'operators' => $operators,
        ];

        return view('wsgolongan.tambah', $data);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Kode Golongan',
                'value' => 'kode_golongan'
            ],
            [
                'text'  => 'Nama Golongan',
                'value' => 'nama_golongan'
            ],
            [
                'text'  => 'Tingkat Golongan',
                'value' => 'tingkat_golongan'
            ],
            [
                'text'  => 'Urutan Golongan',
                'value' => 'urutan_golongan'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Tingkat Posisi',
                'value' => 'tingkat_posisi'
            ],
            [
                'text'  => 'Jabatan',
                'value' => 'jabatan'
            ],
            [
                'text'  => 'Deskripsi',
                'value' => 'deskripsi'
            ],
            [
                'text'  => 'Urutan Tampilan',
                'value' => 'urutan_tampilan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsgolongan.filter', $data);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;
        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }
            $select[] = 'id_golongan';

        } else {
            $select = ['id_golongan','kode_golongan', 'nama_golongan', 'nama_perusahaan'];
        }
        $query = DB::table('wsgolongan')->select($select);
        if($request->kode_golongan) {
            $query->where('kode_golongan', $request->kode_golongan);
        }
        if($request->nama_golongan) {
            $query->where('nama_golongan', $request->nama_golongan);
        }
        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_golongan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        DB::table('wstampilantabledashboarduser_golongan')->insert([
            'kode_golongan'   => $request->kode_golongan,
            'nama_golongan'   => $request->nama_golongan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_g');
    }
    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_golongan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_golongan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_g');
    }

    public function update(Request $request) {
        // $perusahaan_logo = optional($request->file('perusahaan_logo'));
        // $nama_file=time().".".$perusahaan_logo->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $perusahaan_logo->move($tujuan_upload,$nama_file);
        DB::table('wsgolongan')->where('id_golongan', $request->id_golongan)->update([
            'kode_golongan'   =>$request->kode_golongan,
            'nama_golongan'   =>$request->nama_golongan,
            'tingkat_golongan'    =>$request->tingkat_golongan,
            'urutan_golongan' =>$request->urutan_golongan,
            'urutan_tampilan'  =>$request->urutan_tampilan,
            'tingkat_posisi'        =>$request->tingkat_posisi,
            'jabatan'          =>$request->jabatan,
            'deskripsi'      =>$request->deskripsi,
            'tanggal_mulai_efektif'       =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        return redirect('/list_g');
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_g')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_g')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wsgolongan=DB::table('wsgolongan')->get();
        return view('wsgolongan',['wsgolongan'=>$wsgolongan]);
    }
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsgolongan")->where('id_golongan', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function tambah() {
        return view('wsgolongan.tambah');
    }
    
}
