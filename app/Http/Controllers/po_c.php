<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class po_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsposisi")->where('id_posisi', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsposisi')->where('id_posisi', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_po')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsposisi')->select($select);

            if($hasPersonalTable->kode_posisi) {
                $query->where('kode_posisi', $hasPersonalTable->kode_posisi);
            }
            if($hasPersonalTable->nama_posisi) {
                $query->where('nama_posisi', $hasPersonalTable->nama_posisi);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wsposisi.filterResult', $data);
        } else {
            $wsposisi=DB::table('wsposisi')->get();
            return view('wsposisi.index',['wsposisi'=>$wsposisi]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsposisi=DB::table('wsposisi')->get();
        return view('wsposisi',['wsposisi'=>$wsposisi]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Posisi',
                'value' => 'id_posisi'
            ],
            [
                'text'  => 'Kode Perusahaan',
                'value' => 'kode_perusahaan'
            ],
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Posisi',
                'value' => 'kode_posisi'
            ],
            [
                'text'  => 'Nama Posisi',
                'value' => 'nama_posisi'
            ],
            [
                'text'  => 'Kode MPP',
                'value' => 'kode_mpp'
            ],
            [
                'text'  => 'Tingkat Posisi',
                'value' => 'tingkat_posisi'
            ],
            [
                'text'  => 'Detail Tingkat Posisi',
                'value' => 'detail_tingkat_posisi'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Tipe Posisi',
                'value' => 'tipe_posisi'
            ],
            [
                'text'  => 'Detail Tingkat Posisi',
                'value' => 'detail_tingkat_posisi'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Tipe Posisi',
                'value' => 'tipe_posisi'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan Posisi',
                'value' => 'deskripsi_pekerjaan_posisi'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Tipe Posisi',
                'value' => 'tipe_posisi'
            ],
            [
                'text'  => 'Deskripsi Pekerjaan Posisi',
                'value' => 'deskripsi_pekerjaan_posisi'
            ],
            [
                'text'  => 'Kode Posisi Atasan',
                'value' => 'kode_posisi_atasan'
            ],
            [
                'text'  => 'Nama Posisi Atasan',
                'value' => 'nama_posisi_atasan'
            ],
            [
                'text'  => 'Tipe Area',
                'value' => 'tipe_area'
            ],
            [
                'text'  => 'Dari Golongan',
                'value' => 'dari_golongan'
            ],
            [
                'text'  => 'Sampai Golongan',
                'value' => 'sampai_golongan'
            ],
            [
                'text'  => 'Posisi Aktif',
                'value' => 'posisi_aktif'
            ],
            [
                'text'  => 'Komisi',
                'value' => 'komisi'
            ],
            [
                'text'  => 'Kepala Fungsional',
                'value' => 'kepala_fungsional'
            ],
            [
                'text'  => 'Flag Operasional',
                'value' => 'flag_operasional'
            ],
            [
                'text'  => 'Status Posisi',
                'value' => 'status_posisi'
            ],
            [
                'text'  => 'Nomor Surat',
                'value' => 'nomor_surat'
            ],
            [
                'text'  => 'Jumlah Karyawan Posisi Ini',
                'value' => 'jumlah_karyawan_dengan_posisi_ini'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsposisi.filter', $data);
    }
    public function detail($id_posisi){
        $wsposisi = DB::table('wsposisi')->where('id_posisi', $id_posisi)->get();
        return view('wsposisi.detail',['wsposisi'=> $wsposisi]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_posisi';
        } else {
            $select = ['id_posisi', 'kode_posisi', 'nama_posisi', 'nama_jabatan'];
        }

        $query = DB::table('wsposisi')->select($select);

        if($request->kode_posisi) {
            $query->where('kode_posisi', $request->kode_posisi);
        }

        if($request->nama_posisi) {
            $query->where('nama_posisi', $request->nama_posisi);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_po')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_po')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_po')->insert([
            'kode_posisi'   => $request->kode_posisi,
            'nama_posisi'   => $request->nama_posisi,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_po');
    }
    public function simpan(Request $request){
 
        DB::table('wsposisi')->insert([
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_posisi'       =>$request->kode_posisi,
            'nama_posisi'       =>$request->nama_posisi,
            'kode_mpp'         =>$request->kode_mpp,
            'tingkat_posisi'   =>$request->tingkat_posisi,
            'detail_tingkat_posisi'   =>$request->detail_tingkat_posisi,
            'nama_jabatan'  =>$request->nama_jabatan,
            'nama_organisasi'=>$request->nama_organisasi,
            'tingkat_organisasi'=>$request->tingkat_organisasi,
            'tipe_posisi'=>$request->tipe_posisi,
            'deskripsi_pekerjaan_posisi'=>$request->deskripsi_pekerjaan_posisi,
            'nama_posisi_atasan'=>$request->nama_posisi_atasan,
            'dari_golongan'=>$request->dari_golongan,
            'sampai_golongan'=>$request->sampai_golongan,
            'tipe_area'=>$request->tipe_area,
            'posisi_aktif' =>$request->posisi_aktif,
            'komisi' =>$request->komisi,
            'kepala_fungsional' =>$request->kepala_fungsional,
            'flag_operasional'=>$request->flag_operasional,
            'status_posisi'=>$request->status_posisi,
            'nomor_surat'=>$request->nomor_surat,
            'jumlah_karyawan_dengan_posisi_ini'=>$request->jumlah_karyawan_dengan_posisi_ini,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'keterangan'=>$request->keterangan,
        ]);

        DB::table('wstampilantabledashboarduser_posisi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_posisi'   => NULL,
                'nama_posisi'   => NULL,
                'query_field'   => NULL,
                'query_operator'=> NULL,
                'query_value'   => NULL,
            ]);

        return redirect('/list_po');
    }

    public function update(Request $request) {
        DB::table('wsposisi')->where('id_posisi', $request->id_posisi)->update([
            'id_posisi'         =>$request->id_posisi,
            'kode_perusahaan'   =>$request->kode_perusahaan,
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_posisi'       =>$request->kode_posisi,
            'nama_posisi'           =>$request->nama_posisi,
            'kode_mpp'                =>$request->kode_mpp,
            'tingkat_posisi'            =>$request->tingkat_posisi,
            'detail_tingkat_posisi'     =>$request->detail_tingkat_posisi,
            'kode_jabatan'          =>$request->kode_jabatan,
            'nama_jabatan'          =>$request->nama_jabatan,
            'nama_organisasi'   =>$request->nama_organisasi,
            'tingkat_organisasi'   =>$request->tingkat_organisasi,
            'tipe_posisi'   =>$request->tipe_posisi,
            'deskripsi_pekerjaan_posisi'   =>$request->deskripsi_pekerjaan_posisi,
            'kode_posisi_atasan'   =>$request->kode_posisi_atasan,
            'nama_posisi_atasan'   =>$request->nama_posisi_atasan,
            'tipe_area'   =>$request->tipe_area,
            'dari_golongan'   =>$request->dari_golongan,
            'sampai_golongan'   =>$request->sampai_golongan,
            'posisi_aktif'   =>$request->posisi_aktif,
            'komisi'   =>$request->komisi,
            'kepala_fungsional'   =>$request->kepala_fungsional,
            'flag_operasional'   =>$request->flag_operasional,
            'status_posisi'   =>$request->status_posisi,
            'nomor_surat'   =>$request->nomor_surat,
            'jumlah_karyawan_dengan_posisi_ini'   =>$request->jumlah_karyawan_dengan_posisi_ini,
            'keterangan'   =>$request->keterangan,
            'status_rekaman'   =>$request->status_rekaman,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_po');
    }
    public function tambah() {
        $lookp = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $datap['looks'] = $lookp;
        $lookpo = DB::table('wsposisi')->select('id_posisi','kode_posisi','tingkat_posisi', 'nama_posisi_atasan')->get();
        $datapo['looks'] = $lookpo;
        $looktp = DB::table('wstingkatposisi')->select('id_tingkat_posisi','urutan_tingkat')->get();
        $datatp['looks'] = $looktp;
        $lookj = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dataj['looks'] = $lookj;
        $looko = DB::table('wsorganisasi')->select('id_organisasi','nama_organisasi','tingkat_organisasi')->get();
        $datao['looks'] = $looko;
        $lookg = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datag['looks'] = $lookg;
        
        return view('wsposisi.tambah',['datap'=>$datap,'datapo'=>$datapo,'dataj'=>$dataj,'datao'=>$datao,'datag'=>$datag,'datatp'=>$datatp]);
    }
    public function edit($id_posisi) {
        $lookp = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $datap['looks'] = $lookp;
        $looktp = DB::table('wstingkatposisi')->select('id_tingkat_posisi','urutan_tingkat')->get();
        $datatp['looks'] = $looktp;
        $lookj = DB::table('wsjabatan')->select('id_jabatan','nama_jabatan')->get();
        $dataj['looks'] = $lookj;
        $lookdg = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datadg['looks'] = $lookdg;
        $looko = DB::table('wsorganisasi')->select('id_organisasi','nama_organisasi','tingkat_organisasi')->get();
        $datao['looks'] = $looko;
        $wsposisi = DB::table('wsposisi')->where('id_posisi', $id_posisi)->get();
        return view('wsposisi.edit',['wsposisi'=> $wsposisi,'datadg'=>$datadg,'datatp'=>$datatp, 'datap'=>$datap, 'dataj'=>$dataj, 'datao'=>$datao]);
    }
    public function delete($id_posisi) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_posisi){
        DB::table('wsposisi')->where('id_posisi', $id_posisi)->delete();
        return redirect('/list_po');
    }
    // public function deleteAll($id_posisi){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wsposisi');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsposisi")->whereIn('id_posisi',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wsposisi = DB::table('wsposisi')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('singkatan','like',"%".$cari."%")    
        ->orWhere('visi_perusahaan','like',"%".$cari."%")    
        ->orWhere('misi_perusahaan','like',"%".$cari."%")    
        ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
        ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
        ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
        ->orWhere('lokasi_pajak','like',"%".$cari."%") 
        ->orWhere('npp','like',"%".$cari."%") 
        ->orWhere('npkp','like',"%".$cari."%") 
        ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
        ->orWhere('keterangan','like',"%".$cari."%") 
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
        ->orWhere('pengguna_masuk','like',"%".$cari."%") 
        ->orWhere('waktu_masuk','like',"%".$cari."%") 
        ->orWhere('pengguna_ubah','like',"%".$cari."%") 
        ->orWhere('waktu_ubah','like',"%".$cari."%") 
        ->orWhere('pengguna_hapus','like',"%".$cari."%") 
        ->orWhere('waktu_hapus','like',"%".$cari."%") 
        ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
        ->paginate();

        return view('wsposisi',['wsposisi' =>$wsposisi]);
    }

}
