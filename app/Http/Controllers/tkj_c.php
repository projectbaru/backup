<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

use DateTime;

class tkj_c extends Controller
{
    public function hapus($id_tingkat_kelompok_jabatan){
        DB::table('wstingkatkelompokjabatan')->where('id_tingkat_kelompok_jabatan', $id_tingkat_kelompok_jabatan)->delete();
        return redirect('/list_tkj');
    }
    public function detail($id_tingkat_kelompok_jabatan){
        $wstingkatkelompokjabatan = DB::table('wstingkatkelompokjabatan')->where('id_tingkat_kelompok_jabatan', $id_tingkat_kelompok_jabatan)->get();
        return view('wstingkatkelompokjabatan.detail',['wstingkatkelompokjabatan'=> $wstingkatkelompokjabatan]);
    }
   

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wstingkatkelompokjabatan')->where('id_tingkat_kelompok_jabatan', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tkj')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);

            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wstingkatkelompokjabatan')->select($select);

            if($hasPersonalTable->kode_tingkat_kelompok_jabatan) {
                $query->where('kode_tingkat_kelompok_jabatan', $hasPersonalTable->kode_tingkat_kelompok_jabatan);
            }
            if($hasPersonalTable->nama_tingkat_kelompok_jabatan) {
                $query->where('nama_tingkat_kelompok_jabatan', $hasPersonalTable->nama_tingkat_kelompok_jabatan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('wstingkatkelompokjabatan.filterResult', $data);
        } else {
            $wstingkatkelompokjabatan=DB::table('wstingkatkelompokjabatan')->get();
            return view('wstingkatkelompokjabatan.index',['wstingkatkelompokjabatan'=>$wstingkatkelompokjabatan]);
        }
    }

    public function simpan(Request $request){
        DB::table('wstingkatkelompokjabatan')->insert([
            'kode_tingkat_kelompok_jabatan'   =>$request->kode_tingkat_kelompok_jabatan,
            'nama_tingkat_kelompok_jabatan'   =>$request->nama_tingkat_kelompok_jabatan,
            'kode_kelompok_jabatan'    =>$request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan' =>$request->nama_kelompok_jabatan,
            'dari_golongan'  =>$request->dari_golongan,
            'sampai_golongan'        =>$request->sampai_golongan,
            'tanggal_mulai'          =>$request->tanggal_mulai,
            'tanggal_selesai'      =>$request->tanggal_selesai,
            'keterangan'        =>$request->keterangan,
            'status_rekaman'        =>$request->status_rekaman,
        ]);

        DB::table('wstampilantabledashboarduser_tkj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_tingkat_kelompok_jabatan'   => NULL,
                'nama_tingkat_kelompok_jabatan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_tkj');
    }

    
    public function filter()
    {
        $fields = [
         
            [
                'text'  => 'Kode Tingkat Kelompok Jabatan',
                'value' => 'kode_tingkat_kelompok_jabatan'
            ],
            [
                'text'  => 'Nama Tingkat Kelompok Jabatan',
                'value' => 'nama_tingkat_kelompok_jabatan'
            ],
            [
                'text'  => 'Kode Kelompok Jabatan',
                'value' => 'kode_kelompok_jabatan'
            ],
            [
                'text'  => 'Nama Kelompok Jabatan',
                'value' => 'nama_kelompok_jabatan'
            ],
            [
                'text'  => 'Dari Golongan',
                'value' => 'dari_golongan'
            ],
            [
                'text'  => 'Sampai Golongan',
                'value' => 'sampai_golongan'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai',
                'value' => 'tanggal_mulai'
            ],
            [
                'text'  => 'Tanggal Selesai',
                'value' => 'tanggal_selesai'
            ]
            ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wstingkatkelompokjabatan.filter', $data);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;
        $displayedColumn = explode(',', $displayedColumn);
        $select = [];
        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }            
            $select[] = 'id_tingkat_kelompok_jabatan';

        } else {
            $select = ['id_tingkat_kelompok_jabatan','kode_tingkat_kelompok_jabatan', 'nama_tingkat_kelompok_jabatan', 'kode_kelompok_jabatan', 'dari_golongan'];
        }
        $query = DB::table('wstingkatkelompokjabatan')->select($select);
        if($request->kode_tingkat_kelompok_jabatan) {
            $query->where('kode_tingkat_kelompok_jabatan', $request->kode_tingkat_kelompok_jabatan);
        }
        if($request->nama_tingkat_kelompok_jabatan) {
            $query->where('nama_tingkat_kelompok_jabatan', $request->nama_tingkat_kelompok_jabatan);
        }
        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tkj')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tkj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        DB::table('wstampilantabledashboarduser_tkj')->insert([
            'kode_tingkat_kelompok_jabatan'   => $request->kode_tingkat_kelompok_jabatan,
            'nama_tingkat_kelompok_jabatan'   => $request->nama_tingkat_kelompok_jabatan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);
        return Redirect::to('/list_tkj');
    }
    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tkj')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tkj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_tkj');
    }
    public function update(Request $request) {
        DB::table('wstingkatkelompokjabatan')->where('id_tingkat_kelompok_jabatan', $request->id_tingkat_kelompok_jabatan)->update([
            'id_tingkat_kelompok_jabatan'  =>$request->id_tingkat_kelompok_jabatan,
            'kode_tingkat_kelompok_jabatan' =>$request->kode_tingkat_kelompok_jabatan,
            'nama_tingkat_kelompok_jabatan' =>$request->nama_tingkat_kelompok_jabatan,
            'kode_kelompok_jabatan' =>$request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan' =>$request->nama_kelompok_jabatan,
            'dari_golongan' =>$request->dari_golongan,
            'sampai_golongan' =>$request->sampai_golongan,
            'tanggal_mulai'=>$request->tanggal_mulai,
            'tanggal_selesai'=>$request->tanggal_selesai,
            'keterangan'   =>$request->keterangan,

            
            
        ]);

        return redirect('/list_tkj');
    }
    // public function deleteAll(Request $request)
    // {
    //     $ids = $request->ids;
    //     DB::table("wstingkatkelompokjabatan")->whereIn('id_alamat',explode(",",$ids))->delete();
    //     return response()->json(['success'=>"Products Deleted successfully."]);
    // }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_tkj')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_tkj')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wstingkatkelompokjabatan=DB::table('wstingkatkelompokjabatan_tkj')->get();
        return view('wstingkatkelompokjabatan',['wstingkatkelompokjabatan'=>$wstingkatkelompokjabatan]);
    }
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wstingkatkelompokjabatan")->where('id_tkj', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function tambah() {
        $datenow = new DateTime('now');
        $look = DB::table('wskelompokjabatan')->select('id_kelompok_jabatan','kode_kelompok_jabatan')->get();
        $datas['looks'] = $look;
        $look = DB::table('wskelompokjabatan')->select('id_kelompok_jabatan','nama_kelompok_jabatan')->get();
        $datakj['looks'] = $look;
        $look = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datang['looks'] = $look;
        return view('wstingkatkelompokjabatan.tambah',['datakj'=>$datakj, 'datas'=>$datas, 'datang'=>$datang, 'datenow'=>$datenow]);
    }
    public function edit($id_tingkat_kelompok_jabatan) {
        // $lookp = DB::table('wsposisi')->select('id_posisi','nama_posisi')->get();
        // $posisi['looks'] = $lookp;
        $looka = DB::table('wskelompokjabatan')->select('id_kelompok_jabatan','kode_kelompok_jabatan')->get();
        $dataa['looks'] = $looka;
        $lookb = DB::table('wskelompokjabatan')->select('id_kelompok_jabatan','nama_kelompok_jabatan')->get();
        $datab['looks'] = $lookb;
        $lookc = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datac['looks'] = $lookc;
        $wstingkatkelompokjabatan = DB::table('wstingkatkelompokjabatan')->where('id_tingkat_kelompok_jabatan', $id_tingkat_kelompok_jabatan)->get();
        return view('wstingkatkelompokjabatan.edit',['wstingkatkelompokjabatan'=> $wstingkatkelompokjabatan,'dataa'=>$dataa,'datab'=>$datab,'datac'=>$datac]);
    }

}
