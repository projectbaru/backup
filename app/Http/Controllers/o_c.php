<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;


class o_c extends Controller
{
    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }
        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsorganisasi')->where('id_organisasi', $request->multiDelete[$i])->delete();
        }
        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_organisasi')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }

            $query = DB::table('wsorganisasi')->select($select);

            if($hasPersonalTable->nama_organisasi) {
                $query->where('nama_organisasi', $hasPersonalTable->nama_organisasi);
            }
            if($hasPersonalTable->nama_perusahaan) {
                $query->where('nama_perusahaan', $hasPersonalTable->nama_perusahaan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }
            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];
            return view('wsorganisasi.filterResult', $data);
        } else {
            $wsorganisasi=DB::table('wsorganisasi')->get();
            return view('wsorganisasi.index',['wsorganisasi'=>$wsorganisasi]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_organisasi')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_organisasi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }
        $wsorganisasi=DB::table('wsorganisasi')->get();
        return view('wsorganisasi',['wsorganisasi'=>$wsorganisasi]);
    }

    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_organisasi';
        } else {
            $select = ['id_organisasi', 'nama_organisasi', 'nama_perusahaan', 'tingkat_organisasi', 'nama_struktur_organisasi', 'versi_struktur_organisasi', 'tanggal_mulai', 'tanggal_selesai', 'keterangan'];
        }

        $query = DB::table('wsorganisasi')->select($select);

        if($request->nama_organisasi) {
            $query->where('nama_organisasi', $request->nama_organisasi);
        }

        if($request->nama_perusahaan) {
            $query->where('nama_perusahaan', $request->nama_perusahaan);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_organisasi')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_organisasi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_organisasi')->insert([
            'nama_organisasi'   => $request->nama_organisasi,
            'nama_perusahaan'   => $request->nama_perusahaan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_o');
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Nama Perusahaan',
                'value' => 'nama_perusahaan'
            ],
            [
                'text'  => 'Kode Organisasi',
                'value' => 'kode_organisasi'
            ],
            [
                'text'  => 'Nama Organisasi',
                'value' => 'nama_organisasi'
            ],
            [
                'text'  => 'Tipe Area',
                'value' => 'tipe_area'
            ],
            [
                'text'  => 'Grup Organisasi',
                'value' => 'grup_organisasi'
            ],
            [
                'text'  => 'Unit_kerja',
                'value' => 'unit_kerja'
            ],
            [
                'text'  => 'Tanggal Mulai',
                'value' => 'tanggal_mulai'
            ],
            [
                'text'  => 'Tanggal Selesai',
                'value' => 'tanggal_selesai'
            ],
            [
                'text'  => 'id_induk_organisasi',
                'value' => 'id_induk_organisasi'
            ],
            [
                'text'  => 'Induk Organisasi',
                'value' => 'induk_organisasi'
            ],
            [
                'text'  => 'Nama Struktur Organisasi',
                'value' => 'nama_struktur_organisasi'
            ],
            [
                'text'  => 'Versi Struktur Organisasi',
                'value' => 'versi_struktur_organisasi'
            ],
            [
                'text'  => 'Kode Tingkat Organisasi',
                'value' => 'kode_tingkat_organisasi'
            ],
            [
                'text'  => 'Nomor Indeks Organisasi',
                'value' => 'nomor_indeks_organisasi'
            ],
            [
                'text'  => 'Tingkat Organisasi',
                'value' => 'tingkat_organisasi'
            ],
            [
                'text'  => 'Urutan Organisasi',
                'value' => 'urutan_organisasi'
            ],
            [
                'text'  => 'Keterangan Organisasi',
                'value' => 'keterangan_organisasi'
            ],
            [
                'text'  => 'Leher Struktur',
                'value' => 'leher_struktur'
            ],
            [
                'text'  => 'Aktif',
                'value' => 'aktif'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_selesai_efektif'
            ]
        ];
        $operators = [
            '=', '<>', '%LIKE%'
        ];
        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];
        return view('wsorganisasi.filter', $data);
    }

    public function cari(Request $request){
        $cari = $request->cari;
        $wsorganisasi = DB::table('wsorganisasi')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('nama_organisasi','like',"%".$cari."%")
        ->orWhere('tipe_area','like',"%".$cari."%")
        ->orWhere('grup_organisasi','like',"%".$cari."%")
        ->orWhere('unit_kerja','like',"%".$cari."%")
        ->orWhere('tanggal_mulai','like',"%".$cari."%")
        ->orWhere('tanggal_selesai','like',"%".$cari."%")
        ->orWhere('id_induk_organisasi','like',"%".$cari."%")
        ->orWhere('induk_organisasi','like',"%".$cari."%")
        ->orWhere('nama_struktur_organisasi','like',"%".$cari."%")
        ->orWhere('versi_struktur_organisasi','like',"%".$cari."%")
        ->orWhere('kode_tingkat_organisasi','like',"%".$cari."%")
        ->orWhere('nomor_indeks_organisasi','like',"%".$cari."%")
        ->orWhere('tingkat_organisasi','like',"%".$cari."%")
        ->orWhere('urutan_organisasi','like',"%".$cari."%")
        ->orWhere('keterangan_organisasi','like',"%".$cari."%")
        ->orWhere('leher_struktur','like',"%".$cari."%")
        ->orWhere('aktif','like',"%".$cari."%")
        ->orWhere('keterangan','like',"%".$cari."%")
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%")
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%")
        ->orWhere('pengguna_masuk','like',"%".$cari."%")
        ->orWhere('waktu_masuk','like',"%".$cari."%")
        ->orWhere('pengguna_ubah','like',"%".$cari."%")
        ->orWhere('waktu_ubah','like',"%".$cari."%")
        ->orWhere('pengguna_hapus','like',"%".$cari."%")
        ->orWhere('waktu_hapus','like',"%".$cari."%")
        ->paginate();
        return view('wsorganisasi',['wsorganisasi' =>$wsorganisasi]);
    }
    public function simpan(Request $request){
        // $logo_perusahaan = $request->file('logo_perusahaan');
        // $nama_file=time().".".$logo_perusahaan->getClientOriginalName();
        // $tujuan_upload = 'data_file';
        // $logo_perusahaan->move($tujuan_upload,$nama_file);
        DB::table('wsorganisasi')->insert([
            'nama_perusahaan'   =>$request->nama_perusahaan,
            'kode_organisasi'   =>$request->kode_organisasi,
            'nama_organisasi'   =>$request->nama_organisasi,
            'tipe_area'         =>$request->tipe_area,
            'grup_organisasi'   =>$request->grup_organisasi,
            'unit_kerja'        =>$request->unit_kerja,
            'tanggal_mulai'  =>$request->tanggal_mulai,
            'tanggal_selesai'=>$request->tanggal_selesai,
            'id_induk_organisasi'=>$request->id_induk_organisasi,
            'induk_organisasi'=>$request->induk_organisasi,
            'nama_struktur_organisasi'=>$request->nama_struktur_organisasi,
            'versi_struktur_organisasi'=>$request->versi_struktur_organisasi,
            'kode_tingkat_organisasi'=>$request->kode_tingkat_organisasi,
            'kode_pusat_biaya'=>$request->kode_pusat_biaya,
            'nomor_indeks_organisasi'=>$request->nomor_indeks_organisasi,
            'tingkat_organisasi' =>$request->tingkat_organisasi,
            'urutan_organisasi' =>$request->urutan_organisasi,
            'keterangan_organisasi' =>$request->keterangan_organisasi,
            'leher_struktur'=>$request->leher_struktur,
            'aktif'=>$request->aktif,
            'keterangan'=>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
            'pengguna_masuk'=>$request->pengguna_masuk,
            'waktu_masuk'=>$request->waktu_masuk,
            'pengguna_ubah'=>$request->pengguna_ubah,
            'waktu_ubah'=>$request->waktu_ubah,
            'pengguna_hapus'=>$request->pengguna_hapus,
            'waktu_hapus'=>$request->waktu_hapus,
        ]);

        DB::table('wstampilantabledashboarduser_organisasi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'nama_organisasi'   => NULL,
                'nama_perusahaan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);
        return redirect('/list_o');
    }
    public function tambah() {
        $lookp = DB::table('wsperusahaan')->select('id_perusahaan','nama_perusahaan')->get();
        $perusahaan['lookp'] = $lookp;
        return view('wsorganisasi.tambah',['perusahaan'=> $perusahaan]);
    }
    public function detail($id_organisasi){
        $wsorganisasi = DB::table('wsorganisasi')->where('id_organisasi', $id_organisasi)->get();
        return view('wsorganisasi.detail',['wsorganisasi'=> $wsorganisasi]);
    }
    public function edit($id_organisasi) {
        $look = DB::table('wsorganisasi')->select('id_organisasi','tipe_area')->get();
        $datas['looks'] = $look;
        $lookkto = DB::table('wsorganisasi')->select('id_organisasi','kode_tingkat_organisasi')->get();
        $datakto['looks'] = $lookkto;
        $wsorganisasi = DB::table('wsorganisasi')->where('id_organisasi', $id_organisasi)->get();
        return view('wsorganisasi.edit',['wsorganisasi'=> $wsorganisasi,'datas'=> $datas,'datakto'=> $datakto]);
    }
    public function hapus($id_organisasi){
        DB::table('wsorganisasi')->where('id_organisasi', $id_organisasi)->delete();
        return redirect('/list_o');
    }
    public function update(Request $request) {
        DB::table('wsorganisasi')->where('id_organisasi', $request->id_organisasi)->update([
            'id_organisasi'             =>$request->id_organisasi,
            'nama_perusahaan'           =>$request->nama_perusahaan,
            'kode_organisasi'           =>$request->kode_organisasi,
            'nama_organisasi'           =>$request->nama_organisasi,
            'tipe_area'                 =>$request->tipe_area,
            'grup_organisasi'           =>$request->grup_organisasi,
            'unit_kerja'                =>$request->unit_kerja,
            'tanggal_mulai'          =>$request->tanggal_mulai,
            'tanggal_selesai'     =>$request->tanggal_selesai,
            'id_induk_organisasi'  =>$request->id_induk_organisasi,
            'induk_organisasi'  =>$request->induk_organisasi,
            'nama_struktur_organisasi'          =>$request->nama_struktur_organisasi,
            'versi_struktur_organisasi'   =>$request->versi_struktur_organisasi,
            'kode_tingkat_organisasi'           =>$request->kode_tingkat_organisasi,
            'nomor_indeks_organisasi'     =>$request->nomor_indeks_organisasi,
            'tingkat_organisasi'              =>$request->tingkat_organisasi,
            'urutan_organisasi'                       =>$request->urutan_organisasi,
            'keterangan_organisasi'                      =>$request->keterangan_organisasi,
            'leher_struktur'        =>$request->leher_struktur,
            'aktif'                =>$request->aktif,
            'keterangan'            =>$request->keterangan,
            'status_rekaman'     =>$request->status_rekaman,
            'tanggal_mulai_efektif'   =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'   =>$request->tanggal_selesai_efektif,
            'pengguna_masuk'            =>$request->pengguna_masuk,
            'waktu_masuk'               =>$request->waktu_masuk,
            'pengguna_ubah'             =>$request->pengguna_ubah,
            'waktu_ubah'                =>$request->waktu_ubah,
            'pengguna_hapus'            =>$request->pengguna_hapus,
            'waktu_hapus'               =>$request->waktu_hapus,
        ]);
        return redirect('/list_o');
    }
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsorganisasi")->where('id_organisasi', '=', $selected)->delete();
        }
        return redirect()->back();
    }
    public function deleteAll(){
        DB::table('wsperusahaan')->insert();
    }
    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_organisasi')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_organisasi')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }
        return Redirect::to('/list_o');
    }
    public function reset_all(){
        $hasPersonalTable = DB::table('wstampilantabledashaboarduser_organisasi')->where('user_id', Auth::user()->id)->orderBy('id', '');
    }
}
