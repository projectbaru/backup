<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class j_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wsjabatan")->where('id_jabatan', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wsjabatan')->where('id_jabatan', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_jabatan')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wsjabatan')->select($select);

            if($hasPersonalTable->kode_jabatan) {
                $query->where('kode_jabatan', $hasPersonalTable->kode_jabatan);
            }
            if($hasPersonalTable->nama_jabatan) {
                $query->where('nama_jabatan', $hasPersonalTable->nama_jabatan);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wsjabatan.filterResult', $data);
        } else {
            $wsjabatan=DB::table('wsjabatan')->get();
            return view('wsjabatan.index',['wsjabatan'=>$wsjabatan]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_jabatan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_jabatan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wsjabatan=DB::table('wsjabatan')->get();
        return view('wsjabatan',['wsjabatan'=>$wsjabatan]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Jabatan',
                'value' => 'id_jabatan'
            ],
            [
                'text'  => 'Kode Jabatan',
                'value' => 'kode_jabatan'
            ],
            [
                'text'  => 'Nama Jabatan',
                'value' => 'nama_jabatan'
            ],
            [
                'text'  => 'Tipe Jabatan',
                'value' => 'tipe_jabatan'
            ],
            [
                'text'  => 'Kode Kelompok Jabatan',
                'value' => 'kode_kelompok_jabatan'
            ],
            [
                'text'  => 'Nama Kelompok Jabatan',
                'value' => 'nama_kelompok_jabatan'
            ],
            [
                'text'  => 'Grup Jabatan',
                'value' => 'grup_jabatan'
            ],
            [
                'text'  => 'Tanggal Mulai',
                'value' => 'tanggal_mulai'
            ],
            [
                'text'  => 'Tanggal Selesai',
                'value' => 'tanggal_selesai'
            ],
            [
                'text'  => 'Dari Golongan',
                'value' => 'dari_golongan'
            ],
            [
                'text'  => 'Sampai Golongan',
                'value' => 'sampai_golongan'
            ],
            [
                'text'  => 'Magang',
                'value' => 'magang'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'Tanggal Selesai Efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wsjabatan.filter', $data);
    }
    public function detail($id_jabatan){
        $wsjabatan = DB::table('wsjabatan')->where('id_jabatan', $id_jabatan)->get();
        return view('wsjabatan.detail',['wsjabatan'=> $wsjabatan]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);
        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_jabatan';
        } else {
            $select = ['id_jabatan','kode_jabatan','nama_jabatan','nama_kelompok_jabatan','nama_jabatan'];
        }

        $query = DB::table('wsjabatan')->select($select);

        if($request->kode_jabatan) {
            $query->where('kode_jabatan', $request->kode_jabatan);
        }

        if($request->nama_jabatan) {
            $query->where('nama_jabatan', $request->nama_jabatan);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_jabatan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_jabatan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_jabatan')->insert([
            'kode_jabatan'   => $request->kode_jabatan,
            'nama_jabatan'   => $request->nama_jabatan,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_j');
    }
    public function simpan(Request $request){
    
        DB::table('wsjabatan')->insert([
            'kode_jabatan'   =>$request->kode_jabatan,
            'nama_jabatan'   =>$request->nama_jabatan,
            'kode_kelompok_jabatan'  =>$request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan'  =>$request->nama_kelompok_jabatan,
            'grup_jabatan'   =>$request->grup_jabatan,
            'deskripsi_jabatan'  =>$request->deskripsi_jabatan,
            'tanggal_mulai'=>$request->tanggal_mulai,
            'tanggal_selesai'=>$request->tanggal_selesai,
            'magang'=>$request->magang,
            'tipe_jabatan'=>$request->tipe_jabatan,
            'dari_golongan'=>$request->dari_golongan,
            'sampai_golongan'=>$request->sampai_golongan,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' =>$request->tanggal_selesai_efektif,
            'keterangan' =>$request->keterangan,
            
        ]);

        DB::table('wstampilantabledashboarduser_jabatan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_jabatan'   => NULL,
                'nama_jabatan'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_j');
    }

    public function update(Request $request) {
        DB::table('wsjabatan')->where('id_jabatan', $request->id_jabatan)->update([
            'id_jabatan'         =>$request->id_jabatan,
            'kode_jabatan'       =>$request->kode_jabatan,
            'nama_jabatan'       =>$request->nama_jabatan,
            'tipe_jabatan'       =>$request->tipe_jabatan,
            'kode_kelompok_jabatan'         =>$request->kode_kelompok_jabatan,
            'nama_kelompok_jabatan'   =>$request->nama_kelompok_jabatan,
            'grup_jabatan'   =>$request->grup_jabatan,
            'deskripsi_jabatan'  =>$request->deskripsi_jabatan,
            'tanggal_mulai'=>$request->tanggal_mulai,
            'tanggal_selesai'=>$request->tanggal_selesai,
            'dari_golongan'=>$request->dari_golongan,
            'sampai_golongan'=>$request->sampai_golongan,
            'magang'=>$request->magang,
            'keterangan'=>$request->keterangan,
            'status_rekaman'=>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' =>$request->tanggal_selesai_efektif,
            'pengguna_masuk' =>$request->pengguna_masuk,
            'waktu_masuk' =>$request->waktu_masuk,
            'pengguna_ubah' =>$request->pengguna_ubah,
            'waktu_ubah' =>$request->waktu_ubah,
            'pengguna_hapus'=>$request->pengguna_hapus,
            'waktu_hapus'=>$request->waktu_hapus,

        ]);

        return redirect('/list_j');
    }
    public function tambah() {
        $look = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datadg['looks'] = $look;
        $look = DB::table('wskelompokjabatan')->select('id_kelompok_jabatan','kode_kelompok_jabatan','nama_kelompok_jabatan','grup_jabatan')->get();
        $datakkj['looks'] = $look;
        return view('wsjabatan.tambah',['datakkj'=>$datakkj,'datadg'=>$datadg]);
    }
    public function edit($id_jabatan) {
        $look = DB::table('wsgolongan')->select('id_golongan','nama_golongan')->get();
        $datadg['looks'] = $look;
        $wsjabatan = DB::table('wsjabatan')->where('id_jabatan', $id_jabatan)->get();
        return view('wsjabatan.edit',['wsjabatan'=> $wsjabatan,'datadg'=>$datadg]);
    }
    public function delete($id_jabatan) {
        $data=ListPerusahaan::drop();
        return redirect('list_perusahaan');
    }
    public function hapus($id_jabatan){
        DB::table('wsjabatan')->where('id_jabatan', $id_jabatan)->delete();
        return redirect('/list_j');
    }
    // public function deleteAll($id_jabatan){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_jabatan')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_jabatan')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_j');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wsjabatan")->whereIn('id_jabatan',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wsjabatan = DB::table('wsjabatan')
        ->where('kode_jabatan','like',"%".$cari."%")
        ->orWhere('nama_jabatan','like',"%".$cari."%")
        ->orWhere('tipe_jabatan','like',"%".$cari."%")    
        ->orWhere('kode_kelompok_jabatan','like',"%".$cari."%")    
        ->orWhere('nama_kelompok_jabatan','like',"%".$cari."%")    
        ->orWhere('grup_jabatan','like',"%".$cari."%")    
        ->orWhere('deskripsi_jabatan','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai','like',"%".$cari."%")    
        ->orWhere('dari_golongan','like',"%".$cari."%")    
        ->orWhere('sampai_golongan','like',"%".$cari."%")    
        ->orWhere('magang','like',"%".$cari."%")
        ->orWhere('keterangan','like',"%".$cari."%")     
        ->orWhere('status_rekaman','like',"%".$cari."%") 
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
        ->orWhere('pengguna_masuk','like',"%".$cari."%") 
        ->orWhere('waktu_masuk','like',"%".$cari."%") 
        ->orWhere('pengguna_ubah','like',"%".$cari."%")
        ->orWhere('waktu_ubah','like',"%".$cari."%") 
        ->orWhere('pengguna_hapus','like',"%".$cari."%") 
        ->orWhere('waktu_hapus','like',"%".$cari."%") 
        ->paginate();

        return view('wsjabatan',['wsjabatan' =>$wsjabatan]);
    }

}
