<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class lk_c extends Controller
{
    public function delete_multi(Request $request){
        foreach ($request->selectedws as $selected) {
            DB::table("wslokasikerja")->where('id_lokasi_kerja', '=', $selected)->delete();
        }
        return redirect()->back();
    }

    public function multiDelete(Request $request)
    {
        if(!$request->multiDelete) {
            return redirect()->back()->with(['danger' => 'Mohon pilih data yang ingin dihapus']);
        }

        for($i = 0; $i < count($request->multiDelete); $i++) {
            DB::table('wslokasikerja')->where('id_lokasi_kerja', $request->multiDelete[$i])->delete();
        }

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }
    public function index()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser_lk')->where('user_id', Auth::user()->id)->where('active', 1)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            $select             = json_decode($hasPersonalTable->select);
            if($hasPersonalTable->query_value == '[null]' || !$hasPersonalTable->query_value) {
                $queryField         = null;
                $queryOperator      = null;
                $queryValue         = null;
            } else {
                $queryField         = json_decode($hasPersonalTable->query_field);
                $queryOperator      = json_decode($hasPersonalTable->query_operator);
                $queryValue         = json_decode($hasPersonalTable->query_value);
            }
            $query = DB::table('wslokasikerja')->select($select);

            if($hasPersonalTable->kode_lokasi_kerja) {
                $query->where('kode_lokasi_kerja', $hasPersonalTable->kode_lokasi_kerja);
            }
            if($hasPersonalTable->nama_lokasi_kerja) {
                $query->where('nama_lokasi_kerja', $hasPersonalTable->nama_lokasi_kerja);
            }
            if($queryField) {
                $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                    for($i = 0; $i < count($queryField); $i++) {
                        if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                            $date = date('Y-m-d', strtotime($queryValue[$i]));

                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $date);
                            }
                        } else {
                            if($queryOperator[$i] == '%LIKE%') {
                                $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                            } else {
                                $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                            }
                        }
                    }
                });
            }

            $data = [
                'query' => $query->get(),
                'th'    => $select
            ];

            return view('wslokasikerja.filterResult', $data);
        } else {
            $wslokasikerja=DB::table('wslokasikerja')->get();
            return view('wslokasikerja.index',['wslokasikerja'=>$wslokasikerja]);
        }
    }

    public function allData()
    {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        $wslokasikerja=DB::table('wslokasikerja')->get();
        return view('wslokasikerja',['wslokasikerja'=>$wslokasikerja]);
    }

    public function filter()
    {
        $fields = [
            [
                'text'  => 'Id Lokasi Kerja',
                'value' => 'id_lokasi_kerja'
            ],
            [
                'text'  => 'Kode Lokasi Kerja',
                'value' => 'kode_lokasi_kerja'
            ],
            [
                'text'  => 'Nama Lokasi Kerja',
                'value' => 'nama_lokasi_kerja'
            ],
            [
                'text'  => 'Id Grup Lokasi Kerja',
                'value' => 'id_grup_lokasi_kerja'
            ],
            [
                'text'  => 'ID Kantor',
                'value' => 'id_kantor'
            ],
            [
                'text'  => 'Zona Waktu',
                'value' => 'zona_waktu'
            ],
            [
                'text'  => 'Alamat',
                'value' => 'alamat'
            ],
            [
                'text'  => 'Provinsi',
                'value' => 'provinsi'
            ],
            [
                'text'  => 'Kecamatan',
                'value' => 'kecamatan'
            ],
            [
                'text'  => 'Kelurahan',
                'value' => 'kelurahan'
            ],
            [
                'text'  => 'Kabupaten Kota',
                'value' => 'kabupaten_kota'
            ],
            [
                'text'  => 'Kode Pos',
                'value' => 'kode_pos'
            ],
            [
                'text'  => 'Negara',
                'value' => 'negara'
            ],
            [
                'text'  => 'Nomor Telepon',
                'value' => 'nomor_telepon'
            ],
            [
                'text'  => 'Email',
                'value' => 'email'
            ],
            [
                'text'  => 'Fax',
                'value' => 'fax'
            ],
            [
                'text'  => 'Keterangan',
                'value' => 'keterangan'
            ],
            [
                'text'  => 'Status Rekaman',
                'value' => 'status_rekaman'
            ],
            [
                'text'  => 'Tanggal Mulai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ],
            [
                'text'  => 'Tanggal Selesai Efektif',
                'value' => 'tanggal_mulai_efektif'
            ]
        ];

        $operators = [
            '=', '<>', '%LIKE%'
        ];

        $data = [
            'fields'    => $fields,
            'operators' => $operators,
        ];

        return view('wslokasikerja.filter', $data);
    }

    public function detail($id_lokasi_kerja){
        $wslokasikerja = DB::table('wslokasikerja')->where('id_lokasi_kerja', $id_lokasi_kerja)->get();
        return view('wslokasikerja.detail',['wslokasikerja'=> $wslokasikerja]);
    }
    public function filterSubmit(Request $request)
    {
        $queryField         = $request->queryField;
        $queryOperator      = $request->queryOperator;
        $queryValue         = $request->queryValue;
        $displayedColumn    = $request->displayedColumn;

        $displayedColumn = explode(',', $displayedColumn);

        $select = [];

        if($displayedColumn) {
            for($i = 0; $i < count($displayedColumn); $i++)  {
                array_push($select, $displayedColumn[$i]);
            }

            $select[] = 'id_lokasi_kerja';
        } else {
            $select = ['id_lokasi_kerja', 'kode_lokasi_kerja', 'nama_lokasi_kerja'];
        }

        $query = DB::table('wslokasikerja')->select($select);

        if($request->kode_lokasi_kerja) {
            $query->where('kode_lokasi_kerja', $request->kode_lokasi_kerja);
        }

        if($request->nama_lokasi_kerja) {
            $query->where('nama_lokasi_kerja', $request->nama_lokasi_kerja);
        }

        if(count($queryField) > 0) {
            $query->where(function($sub) use ($queryField, $queryOperator, $queryValue) {
                for($i = 0; $i < count($queryField); $i++) {
                    if($queryField[$i] == 'tanggal_mulai_perusahaan' || $queryField[$i] == 'tanggal_selesai_perusahaan' || $queryField[$i] == 'tanggal_mulai_efektif' || $queryField[$i] == 'tanggal_selesai_efektif') {
                        $date = date('Y-m-d', strtotime($queryValue[$i]));

                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$date.'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $date);
                        }
                    } else {
                        if($queryOperator[$i] == '%LIKE%') {
                            $sub->where($queryField[$i], 'LIKE', '%'.$queryValue[$i].'%');
                        } else {
                            $sub->where($queryField[$i], $queryOperator[$i], $queryValue[$i]);
                        }
                    }
                }
            });
        }

        $hasPersonalTable = DB::table('wstampilantabledashboarduser_lk')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser_lk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 0,
            ]);
        }

        DB::table('wstampilantabledashboarduser_lk')->insert([
            'kode_lokasi_kerja'   => $request->kode_lokasi_kerja,
            'nama_lokasi_kerja'   => $request->nama_lokasi_kerja,
            'query_field'       => count($queryField) > 0 ? json_encode($queryField) : false,
            'query_operator'    => count($queryOperator) > 0 ? json_encode($queryOperator) : false,
            'query_value'       => count($queryValue) > 0 ? json_encode($queryValue) : false,
            'select'            => json_encode($select),
            'user_id'           => Auth::user()->id,
        ]);

        return Redirect::to('/list_lk');
    }
    public function simpan(Request $request){
        DB::table('wslokasikerja')->insert([
            'kode_lokasi_kerja'   =>$request->kode_lokasi_kerja,
            'nama_lokasi_kerja'   =>$request->nama_lokasi_kerja,
            'id_grup_lokasi_kerja'         =>$request->id_grup_lokasi_kerja,
            'id_kantor'   =>$request->id_kantor,
            'zona_waktu'   =>$request->zona_waktu,
            'alamat'  =>$request->alamat,
            'provinsi'=>$request->provinsi,
            'kecamatan'=>$request->kecamatan,
            'kelurahan'=>$request->kelurahan,
            'kabupaten_kota'=>$request->kabupaten_kota,
            'kode_pos'=>$request->kode_pos,
            'negara'=>$request->negara,
            'nomor_telepon'=>$request->nomor_telepon,
            'email' =>$request->email,
            'fax' =>$request->fax,
            'keterangan' =>$request->keterangan,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
        ]);

        DB::table('wstampilantabledashboarduser_lk')
            ->where('user_id', Auth::user()->id)
            ->update([
                'kode_lokasi_kerja'   => NULL,
                'nama_lokasi_kerja'   => NULL,
                'query_field'       => NULL,
                'query_operator'    => NULL,
                'query_value'       => NULL,
            ]);

        return redirect('/list_lk');
    }

    public function update(Request $request) {
        DB::table('wslokasikerja')->where('id_lokasi_kerja', $request->id_lokasi_kerja)->update([
            'id_lokasi_kerja'         =>$request->id_lokasi_kerja,
            'kode_lokasi_kerja'       =>$request->kode_lokasi_kerja,
            'nama_lokasi_kerja'       =>$request->nama_lokasi_kerja,
            'id_grup_lokasi_kerja'       =>$request->id_grup_lokasi_kerja,
            'id_kantor'         =>$request->id_kantor,
            'zona_waktu'   =>$request->zona_waktu,
            'alamat'   =>$request->alamat,
            'provinsi'  =>$request->provinsi,
            'kecamatan'=>$request->kecamatan,
            'kelurahan'=>$request->kelurahan,
            'kabupaten_kota'=>$request->kabupaten_kota,
            'kode_pos'=>$request->kode_pos,
            'negara'=>$request->negara,
            'nomor_telepon'=>$request->nomor_telepon,
            'email'=>$request->email,
            'fax'=>$request->fax,
            'keterangan' =>$request->keterangan,
            'status_rekaman' =>$request->status_rekaman,
            'tanggal_mulai_efektif' =>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif' =>$request->tanggal_selesai_efektif,
            'status_rekaman' =>$request->status_rekaman,
            'tanggal_mulai_efektif'=>$request->tanggal_mulai_efektif,
            'tanggal_selesai_efektif'=>$request->tanggal_selesai_efektif,
        ]);

        return redirect('/list_lk');
    }
    public function tambah() {
        return view('wslokasikerja.tambah');
    }
    public function edit($id_lokasi_kerja) {
        $wslokasikerja = DB::table('wslokasikerja')->where('id_lokasi_kerja', $id_lokasi_kerja)->get();
        return view('wslokasikerja.edit',['wslokasikerja'=> $wslokasikerja]);
    }
    public function delete($id_lokasi_kerja) {
        $data=ListPerusahaan::drop();
        return redirect('list_lk');
    }
    public function hapus($id_lokasi_kerja){
        DB::table('wslokasikerja')->where('id_lokasi_kerja', $id_lokasi_kerja)->delete();
        return redirect('/list_lk');
    }
    // public function deleteAll($id_lokasi_kerja){
    //     $data=ListPerusahaan::drop('');
    //     return redirect('list_perusahaan');
    // }

    public function reset() {
        $hasPersonalTable = DB::table('wstampilantabledashboarduser')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($hasPersonalTable) {
            DB::table('wstampilantabledashboarduser')
            ->where('user_id', Auth::user()->id)
            ->update([
                'active'   => 1,
            ]);
        }

        return Redirect::to('/list_wslokasikerja');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        DB::table("wslokasikerja")->whereIn('id_lokasi_kerja',explode(",",$ids))->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    public function cari(Request $request){
        $cari = $request->cari;
        $wslokasikerja = DB::table('wslokasikerja')
        ->where('nama_perusahaan','like',"%".$cari."%")
        ->orWhere('kode_perusahaan','like',"%".$cari."%")
        ->orWhere('singkatan','like',"%".$cari."%")    
        ->orWhere('visi_perusahaan','like',"%".$cari."%")    
        ->orWhere('misi_perusahaan','like',"%".$cari."%")    
        ->orWhere('nilai_perusahaan','like',"%".$cari."%")    
        ->orWhere('keterangan_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_mulai_perusahaan','like',"%".$cari."%")    
        ->orWhere('tanggal_selesai_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jenis_bisnis_perusahaan','like',"%".$cari."%")    
        ->orWhere('jumlah_perusahaan','like',"%".$cari."%")
        ->orWhere('nomor_npwp_perusahaan','like',"%".$cari."%")     
        ->orWhere('lokasi_pajak','like',"%".$cari."%") 
        ->orWhere('npp','like',"%".$cari."%") 
        ->orWhere('npkp','like',"%".$cari."%") 
        ->orWhere('id_logo_perusahaan','like',"%".$cari."%") 
        ->orWhere('keterangan','like',"%".$cari."%") 
        ->orWhere('status_rekaman','like',"%".$cari."%")
        ->orWhere('tanggal_mulai_efektif','like',"%".$cari."%") 
        ->orWhere('tanggal_selesai_efektif','like',"%".$cari."%") 
        ->orWhere('pengguna_masuk','like',"%".$cari."%") 
        ->orWhere('waktu_masuk','like',"%".$cari."%") 
        ->orWhere('pengguna_ubah','like',"%".$cari."%") 
        ->orWhere('waktu_ubah','like',"%".$cari."%") 
        ->orWhere('pengguna_hapus','like',"%".$cari."%") 
        ->orWhere('waktu_hapus','like',"%".$cari."%") 
        ->orWhere('jumlah_karyawan','like',"%".$cari."%") 
        ->paginate();

        return view('wslokasikerja',['wslokasikerja' =>$wslokasikerja]);
    }

}
