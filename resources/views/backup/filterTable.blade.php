<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <style>
        </style>
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/') }}" class="text-sm text-gray-700 dark:text-gray-500 underline"></a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="container" style="width:80%;background-color:white;">
                <div class="container" style="background-color:#f7f8fc;width:60%;">
                    <form action="">
                        <div class="container-fluid mt-2" style="background-color:#c8c8c9;">
                            <b>Langkah 1. Masukkan Nama</b>
                        </div>
                        <br>

                        <div class="container" style="width:70%; text-align:right;">
                            <div class="row mb-3">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control form-control-sm" id="inputEmail3">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control form-control-sm" id="inputPassword3">
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid mt-2" style="background-color:#c8c8c9;">
                            <b>Langkah 2. Spesifikasi Kriteria Pencarian</b>
                        </div>
                        <br>
                        <div class="container">
                            <b>Saring Berdasarkan Halaman Tambahan (Pilihan)</b>
                            <table class="container" width="100%">
                                <tr>
                                    <td>Halaman</td>
                                    <td>Operator</td>
                                    <td>Nilai</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="width:50%;">
                                        <select name="" id="" style="width:50%;">input
                                            <option value="">1</option>
                                            <option value="">2</option>
                                            <option value="">3</option>
                                            <option value="">4</option>
                                        </select>
                                    </td>
                                    <td style="width:50%;">
                                        <select name="" id="" style="width:50%;">input
                                            <option value="">=</option>
                                            <option value="">2</option>
                                            <option value="">3</option>
                                            <option value="">4</option>
                                        </select>
                                    </td>
                                    <td style="width:50%;">
                                        <input type="text" class="">
                                    </td>

                                </tr>
                            </table>
                        </div>
                            <div class="container-fluid mt-2" style="background-color:#c8c8c9;">
                                <b>Langkah 3. Pilih Halaman yang ditampilkan</b>
                            </div>
                            <div class="container mt-2">
                                <table style="width:100%;">
                                    <tr style="text-align:center;">
                                        <td>Halaman tersedia</td>

                                        <td></td>

                                        <td>Halaman yang dipilih</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <textarea name="" id="" style="width:100%; aut"></textarea>
                                        </td>
                                        <td>
                                            <a href=""></a>
                                            <a href="">><span class='bi bi-arrow-left-square-fill'></span></a>
                                        </td>
                                        <td>
                                            <textarea name="" id="" style="width:100%;"></textarea>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                    </form>
                </div>
                <!-- <table>
                    <tr>
                        <td>Perusahaan</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Tampilkan</td>
                        <td>:</td>
                        <td><input type="text" class="form-control form-control-sm"></td>
                        <td><a type="submit" class="btn btn-sm btn-primary">Go</a></td>
                        <td></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td><a type="submit" class="">Ubah</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a type="submit" class="">Buat Tampilan Baru</a></td>
                    </tr>
                </table><br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Nama Perusahaan</th>
                            <th scope="col">Logo Perusahaan</th>
                            <th scope="col">Visi Perusahaan</th>
                            <th scope="col">Nilai Perusahaan</th>
                            <th scope="col">V</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td><input type="checkbox"></td>
                        </tr>
                        <tr>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td><input type="checkbox"></td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td><a class="btn btn-secondary btn-sm" href="">Tambah</a></td>
                        <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td>
                        <td><a class="btn btn-secondary btn-sm" href="">Hapus</a></td>
                    </tr>
                </table> -->
            </div>
        </div>
    </body>
</html>
