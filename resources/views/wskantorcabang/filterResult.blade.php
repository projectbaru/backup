<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kantor Cabang</title>

    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item active" href="{{route('list_kc')}}" style="background-color:#1053af;color:white;">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content" style="background-color:white;">
                <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow navbar-sm">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Douglas McGee</span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav> -->
                <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" >
                    @if (Route::has('login'))
                        <div class="hidden fixed right-0 sm:block">
                            @auth
                                <!-- <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a> -->
                            @else
                                <a href="{{ route('login') }}"
                                    class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}"
                                        class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                                @endif
                            @endauth
                            <!-- <a href="{{ url('/filter') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Filter</a> -->
                        </div>
                    @endif
                    @if($query)
                            @if ($message = Session::get('success'))
                                <div class="alert alert-danger alert-success fade show" role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            @if ($message = Session::get('danger'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                                <h5 style="color:black;">Kelas Cabang</h5><hr>
                                <table style="font-size:12px;">                                
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                    <!-- <form action="{{route('cari_wsperusahaan')}}" class="form-inline" method="GET">
                                        <input class="form-control form-control-sm" type="search" name="cari" placeholder="Cari data ..." aria-label="Cari data ...">
                                        <a class="btn btn-outline-secondary my-2 my-sm-0  btn-sm" type="submit"><i class="fas fa-search"></i>&nbsp;Cari</a>
                                    </form> -->
                                    </td>
                                    <td>
                                        <!-- <a type="submit" class="btn btn-sm btn-primary">Go</a> -->
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <!-- <a class="{{route('ubah_tampilanperusahaan')}}">Ubah</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                        <a href="{{route ('ubah_tampilanorganisasi')}}" class="">Buat Tampilan Baru</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                        <!-- <a href="{{ '/list_semua_wsperusahaan' }}" class="">Tampilkan semua data</a> -->
                                    </td>
                                </tr>
                            </table><br>
                            <form action="{{ URL::to('/list_kac/hapus_banyak') }}" method="POST">
                                @csrf
                                    <table class="table table-border table-striped table-hover table-sm" style="font-size:10px;">
                                        <thead>
                                            @for($i = 0; $i < count($th); $i++)
                                                @if($th[$i] == 'id_kantor')
                                                    <th></th>
                                                @endif
                                                @if($th[$i] == 'kode_kantor')
                                                    <th>Kode Kantor</th>
                                                @endif
                                                @if($th[$i] == 'nama_kantor')
                                                    <th>Nama Kantor</th>
                                                @endif
                                                @if($th[$i] == 'kode_kelas')
                                                    <th>Kode Kelas+</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th>Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th>Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th>Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_selesai_efektif')
                                                    <th>Tanggal Selesai Efektif</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th>Aksi</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody>
                                            @foreach($query as $row)
                                                <tr>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        @if($th[$i] == 'id_kelas_cabang')
                                                            <td>{{ $row->id_kelas_cabang ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_kelas_cabang')
                                                            <td>{{ $row->kode_kelas_cabang ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_kelas_cabang')
                                                            <td>{{ $row->nama_kelas_cabang ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_organisasi')
                                                            <td>{{ $row->nama_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tipe_area')
                                                            <td>{{ $row->tipe_area ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'grup_organisasi')
                                                            <td>{{ $row->grup_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'unit_kerja')
                                                            <td>{{ $row->unit_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai')
                                                            <td>{{ $row->tanggal_mulai ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai')
                                                            <td>{{ $row->tanggal_selesai ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_induk_organisasi')
                                                            <td>{{ $row->id_induk_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'induk_organisasi')
                                                            <td>{{ $row->induk_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_struktur_organisasi')
                                                            <td>{{ $row->nama_struktur_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'versi_struktur_organisasi')
                                                            <td>{{ $row->versi_struktur_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_tingkat_organisasi')
                                                            <td>{{ $row->kode_tingkat_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nomor_indeks_organisasi')
                                                            <td>{{ $row->nomor_indeks_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tingkat_organisasi')
                                                            <td>{{ $row->tingkat_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'urutan_organisasi')
                                                            <td>{{ $row->urutan_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan_organisasi')
                                                            <td>{{ $row->keterangan_organisasi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'leher_struktur')
                                                            <td>{{ $row->leher_struktur ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'aktif')
                                                            <td>{{ $row->aktif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }} </td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_masuk')
                                                            <td>{{ $row->pengguna_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_masuk')
                                                            <td>{{ $row->waktu_masuk ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_ubah')
                                                            <td>{{ $row->pengguna_ubah ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'pengguna_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'waktu_hapus')
                                                            <td>{{ $row->pengguna_hapus ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($i == count($th) - 1)
                                                            <td>                                                                                                        
                                                                <a href="{{ URL::to('/list_wsorganisasi/edit/'.$row->id_organisasi) }}" class="">Edit</a>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_organisasi }}">
                                                            </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                            <td><a class="btn btn-secondary btn-sm" href="{{route('tambah_wsorganisasi')}}">Tambah</a></td>
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td>
                                                <button class="btn btn-secondary btn-sm" href="">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                    @else
                        <h4>Data tidak ditemukan :(</h4>
                    @endif
                    </div>
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <!-- <span>Copyright &copy; Your Website 2021</span> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

</body>

</html>
