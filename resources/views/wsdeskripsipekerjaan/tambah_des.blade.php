<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Deskripsi Pekerjaan</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<style>
    .autocomplete {
        position: relative;
        display: inline-block;
    }

    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }

    .autocomplete-items {
        padding: 10px;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
    }

    /*when hovering an item:*/
    .autocomplete-items:hover {
        background-color: #e9e9e9;
    }

    /*when navigating through the items using the arrow keys:*/
    .autocomplete-active {
        background-color: DodgerBlue !important;
        color: #ffffff;
    }
</style>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">Interface</div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header" style="color:black;">Menu Master:</h6>
                        <a class="collapse-item" href="{{ route('list_p') }}">Perusahaan</a>
                        <a class="collapse-item" href="{{ route('list_a') }}">Alamat</a>
                        <a class="collapse-item" href="{{ route('list_o') }}">Organisasi</a>
                        <a class="collapse-item" href="{{ route('list_to') }}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{ route('list_g') }}">Golongan</a>
                        <a class="collapse-item" href="{{ route('list_tg') }}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{ route('list_po') }}">Posisi</a>
                        <a class="collapse-item" href="{{ route('list_tp') }}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{ route('list_lk') }}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{ route('list_glk') }}">Grup Lokasi Kerja</a>
                        <a class="collapse-item" href="{{ route('list_kc') }}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <!-- <a class="collapse-item active" href="{{ route('list_dp') }}" style="background-color:#1053af;color:white;">Dekripsi Pekerjaan</a> -->
                        <a class="collapse-item active" aria-expanded="true" aria-controls="collapseTwo" href="{{route('list_dp')}}" data-target="#collapseTre" style="background-color:#1053af;color:white;">Deskripsi Pekerjaan</a>
                        <div id="collapseTre" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                                <!-- <h6 class="collapse-header" style="color:black;"></h6> -->
                                <a class="collapse-item" href="{{route('tambah_hdp')}}">Header Deskripsi Pekerjaan</a>
                                <a class="collapse-item active" href="{{route('list_dp')}}">Detail Deskripsi</a>
                            </div>
                        </div>
                        <a class="collapse-item" href="{{ route('list_j') }}">Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_kj') }}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_tkj') }}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_usm') }}">Upah Standar Minimum</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
            <div id="content">
                <br>

                <div class="container-fluid" style="background-color:white;">
                    <div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">
                        <div class="container" style="width:100%; ">
                            <br>
                            <div>

                                <div class="container-fluid mt-2" style="border:1px solid #d9d9d9;border-radius:10px;">
                                    <div class="" style=""><br>
                                        <h5><b style="color:black;">Tambah Deskripsi Pekerjaan</b></h5>
                                    </div>
                                    <form autocomplete="off" action="{{ URL::to('/simpan_dp') }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="">

                                                <hr>
                                                <b style="color:black;">Informasi</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode
                                                                Posisi</label>
                                                            <div class="col-sm-10">
                                                                <select name="kode_posisi" id="produk00" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datapos['looks'] as $p )
                                                                    <option value="{{$p->kode_posisi}}">{{$p->kode_posisi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                                Posisi</label>
                                                            <div class="col-sm-10">
                                                                <select name="nama_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datapos['looks'] as $v )
                                                                    <option value="{{$v->nama_posisi}}">{{$v->nama_posisi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="descriptionInputContainer">
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi
                                                                Pekerjaan 1</label>
                                                            <div class="col-sm-3">
                                                                <textarea name="deskripsi_pekerjaan[]" rows="4" cols="100" class="form-control form-control-sm" required></textarea>
                                                            </div>
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                                                            <div class="col-sm-2">
                                                                <select name="pdca[]" class="form-control form-control-sm" required>
                                                                    <option value="" selected disabled>--- Belum
                                                                        dipilih ---</option>
                                                                    <option value="a">a</option>
                                                                    <option value="b">b</option>
                                                                    <option value="c">c</option>
                                                                </select>
                                                            </div>
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                                                            <div class="col-sm-2">
                                                                <select name="bsc[]" class="form-control form-control-sm" required>
                                                                    <option value="" selected disabled>--- Belum
                                                                        dipilih ---</option>
                                                                    <option value="a">a</option>
                                                                    <option value="b">b</option>
                                                                    <option value="c">c</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <button class="btn btn-sm btn-primary" type="button" id="addDescriptionInput">
                                                                    <i class="fa-solid fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">Rekaman Informasi</b>
                                                <div class="col" style="font-size: 10px;">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                        <div class="col-sm-10">
                                                            <input type="date" style="font-size:11px;" name="tanggal_mulai_efektif" class="form-control form-control-sm" require id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                        <div class="col-sm-10">
                                                            <input type="date" style="font-size:11px;" name="tanggal_selesai_efektif" class="form-control form-control-sm" require id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" style="font-size:11px;" rows="4" cols="50" name="keterangan" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container" style="width: 100%;">
                                            <table>
                                                <tr>
                                                    <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                                    <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                                    <td>
                                                        <a href="{{ route('list_dp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;">Batal</a>
                                                    </td>
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;">Simpan</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br>
                                    </form>
                                </div>
                                <br>
                            </div>
                        </div>
                        <!-- <footer class="sticky-footer bg-white">
                            <div class="container my-auto">
                                <div class="copyright text-center my-auto">
                                    <span>Copyright &copy; Your Website 2021</span>
                                </div>
                            </div>
                        </footer> -->
                    </div>
                </div>
                <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
                <!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Select "Logout" below if you are ready to end your current session.
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                <a class="btn btn-primary" href="login.html">Logout</a>
                            </div>
                        </div>
                    </div>
                </div> -->
                <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
                <script src="{{ asset('js/jquery.min.js') }}"></script>
                <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

                @include('js.wsdeskripsipekerjaan.tambah')
</body>

</html>