<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Deskripsi Pekerjaan</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
					<a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                    <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                    <a class="collapse-item " href="{{route('list_o')}}">Organisasi</a>
                    <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                    <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                    <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                    <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                    <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                    <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                    <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a> 
                    <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                    <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                    <a class="collapse-item active" href="{{route('list_dp')}}" style="background-color:#1053af;color:white;">Dekripsi Pekerjaan</a>
                    <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                    <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
			<!-- <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                        <img class="img-profile rounded-circle" src="img/undraw_profile.svg"></a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Settings </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout </a>
                        </div>
                    </li>
                </ul>
			</nav> -->
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style=""><br>
					<div class="container" style="width:90%; ">
						<div>
							<div class="container mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
                            <h5 style="color:black;">Detail Deskripsi Pekerjaan</h5>	
                            <hr>	
                            <form action="{{route('simpan_dp')}}" method="post"> {{ csrf_field() }}
                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_dp', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_dp', $wsdeskripsipekerjaan->id_deskripsi_pekerjaan)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_dp')}}">Batal</a></td>
                                    </tr>
                                </table><br>
                                    <div class="container-fluid mt-2" style="">
                                        <b>Informasi Deskripsi Pekerjaan</b>
                                        <div class="row">
                                            <div class="col-6">
                                                <table style="font-size:12px;">
                                                    <tr>
                                                        <td>Kode Posisi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="kode_posisi" value="{{$wsdeskripsipekerjaan->kode_posisi}}">{{ $wsdeskripsipekerjaan->kode_posisi ?? 'NO DATA' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama Posisi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="nama_posisi" value="{{$wsdeskripsipekerjaan->nama_posisi}}">{{ $wsdeskripsipekerjaan->nama_posisi ?? 'NO DATA' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td><br></td>
                                                        <td></td>
                                                    </tr>
                                                    {{-- {{dd($deskripsiPekerjaan)}} --}}
                                                    {{-- <tr>
                                                            <td>Deskripsi Pekerjaan</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="deskripsi_pekerjaan" value="{{$deskripsiPekerjaan}}">{{ $deskripsiPekerjaan ?? 'NO DATA' }}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td><input type="hidden" name="deskripsi_pekerjaan" value="{{$loop->iteration }}">{{ $loop->iteration }}</td>
                                                    </tr> --}}
                                                        <br>
                                                    @foreach ($deskripsiPekerjaan as $descPekerjaan)
                                                        <tr>
                                                            <td>Deskripsi Pekerjaan {{ $loop->iteration }}</td>
                                                            <td>:</td>
                                                            <td><input type="hidden" name="deskripsi_pekerjaan" value="{{$descPekerjaan['deskripsi'] }}">{{ $descPekerjaan['deskripsi'] ?? 'NO DATA' }}</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                                            <td>PDAC:</td>
                                                            <td><input type="hidden" name="pdac" value="{{$descPekerjaan['pdca'] }}">{{ $descPekerjaan['pdca'] ?? 'NO DATA' }}</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                                                            <td>BSC:</td>
                                                            <td><input type="hidden" name="bsc" value="{{$descPekerjaan['bsc'] }}">{{ $descPekerjaan['bsc'] ?? 'NO DATA' }}</td>
                                                        </tr>
                                                        <tr>
                                                            
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td></td>
                                                        <td><br></td>
                                                        <td></td>
                                                    </tr>
                                                    <!-- {{-- <tr>
                                                        <td>Kualifikasi Jabatan</td>
                                                        <td>:</td>
                                                        <td>{{ $kualifikasiJabatan ?? 'NO DATA' }}</td>
                                                    </tr> --}}
                                                    @foreach ($kualifikasiJabatan as $jabatan)
                                                        <tr>
                                                            <td>Kualifikasi Jabatan {{ $loop->iteration }}</td>
                                                            <td>:</td>
                                                            <td>{{ $jabatan['jabatan'] ?? 'NO DATA' }}</td>
                                                        </tr>
                                                    @endforeach
                                                    {{-- {{dd($tanggungJawab)}} --}}
                                                    @foreach ($tanggungJawab as $tanggung)
                                                        <tr>
                                                            <td>Tanggung Jawab {{ $loop->iteration }}</td>
                                                            <td>:</td>
                                                            <td>{{ $tanggung ?? 'NO DATA' }}</td>
                                                        </tr>
                                                    @endforeach
                                                    @foreach ($wewenang as $w)
                                                        <tr>
                                                            <td>Wewenang {{ $loop->iteration }}</td>
                                                            <td>:</td>
                                                            <td>{{ $w ?? 'NO DATA' }}</td>
                                                        </tr>
                                                    @endforeach -->
                                                   
                                                </table>
                                            </div>
                                        </div>
                                        <hr>
                                        <b>Rekaman Informasi</b>
                                         <table style="font-size:12px;">
                                            <tr>
                                                <td>Tanggal Mulai Efektif</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$wsdeskripsipekerjaan->tanggal_mulai_efektif}}">{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal Selesai Efektif</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$wsdeskripsipekerjaan->tanggal_selesai_efektif}}">{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif ?? 'NO DATA' }}</td>
                                            </tr>
                                            <tr>
                                                <td>Keterangan</td>
                                                <td>:</td>
                                                <td><input type="hidden" name="keterangan" value="{{$wsdeskripsipekerjaan->keterangan}}">{{ $wsdeskripsipekerjaan->keterangan ?? 'NO DATA' }}</td>
                                            </tr>
                                         </table>               
                                    </div>
                                    <br>
                            </form>
							</div>
							<br>

                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>