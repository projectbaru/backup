@extends('template.default')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        @if (Request::is('list_semua_wsperusahaan'))
        <a type="submit" href="{{ route('ubah_tamhdp') }}" class="">Buat Tampilan Baru</a>|&nbsp;&nbsp;<a type="submit" href="{{ '/set_back_display' }}" class="">Kembali</a>
        @else
        <a type="submit" href="{{ route('ubah_tamhdp') }}" class="">Buat Tampilan Baru</a>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            @if($query)
            <form action="{{ URL::to('/list_dp/hapus_banyak') }}" method="POST">
                @csrf
                <table id="example" class="table table-bordered table-striped table-hover table-sm" style="font-size:10px;">
                    <thead>
                        <th>No</th>
                        @for($i = 0; $i < count($th); $i++) <!-- @if($th[$i]=='id_deskripsi_pekerjaan' ) <th>
                            </th>
                            @endif -->
                            @if($th[$i] == 'kode_deskripsi')
                            <th>Kode Deskripsi</th>
                            @endif
                            @if($th[$i] == 'nama_posisi')
                            <th>Nama Posisi</th>
                            @endif
                            @if($th[$i] == 'keterangan')
                            <th>Keterangan</th>
                            @endif

                            @if($th[$i] == 'deskripsi_pekerjaan')
                            <th>Deskripsi Pekerjaan</th>
                            @endif


                            @if($th[$i] == 'tanggal_mulai_efektif')
                            <th>Tanggal Mulai Efektif</th>
                            @endif
                            @if($th[$i] == 'tanggal_selesai_efektif')
                            <th>Tanggal Selesai Efektif</th>
                            @endif
                            @if($th[$i] == 'nomor_dokumen')
                            <th>Nomor Dokumen</th>
                            @endif
                            @if($th[$i] == 'edisi')
                            <th>Edisi</th>
                            @endif
                            @if($th[$i] == 'tanggal_edisi')
                            <th>Tanggal Edisi</th>
                            @endif
                            @if($th[$i] == 'nomor_revisi')
                            <th>Nomor Revisi</th>
                            @endif
                            @if($th[$i] == 'tanggal_revisi')
                            <th>Tanggal Revisi</th>
                            @endif
                            @if($th[$i] == 'nama_jabatan')
                            <th>Nama Jabatan</th>
                            @endif
                            @if($th[$i] == 'nama_karyawan')
                            <th>Nama Karyawan</th>
                            @endif
                            @if($th[$i] == 'divisi')
                            <th>Divisi</th>
                            @endif
                            @if($th[$i] == 'lokasi_kerja')
                            <th>Lokasi Kerja</th>
                            @endif
                            @if($th[$i] == 'dibuat_oleh')
                            <th>Dibuat Oleh</th>
                            @endif
                            @if($th[$i] == 'diperiksa_oleh')
                            <th>Diperiksa Oleh</th>
                            @endif
                            @if($th[$i] == 'fungsi_jabatan')
                            <th>Fungsi Jabatan</th>
                            @endif
                            @if($th[$i] == 'tanggung_jawab')
                            <th>Tangguna Jawab</th>
                            @endif
                            @if($th[$i] == 'departemen')
                            <th>Departemen</th>
                            @endif
                            @if($th[$i] == 'kode_posisi')
                            <th>Kode Posisi</th>
                            @endif
                            @if($th[$i] == 'nama_lokasi_kerja')
                            <th>Nama Lokasi Kerja</th>
                            @endif
                            @if($th[$i] == 'nama_pengawas')
                            <th>Nama Pengawas</th>
                            @endif
                            @if($th[$i] == 'lingkup_aktivitas')
                            <th>Lingkup Aktivitas</th>
                            @endif
                            @if($th[$i] == 'id_logo_perusahaan')
                            <th>ID Logo Perusahaan</th>
                            @endif

                            @if($i == count($th) - 1)
                            <th>Aksi</th>
                            <th>V</th>
                            @endif
                            @endfor
                    </thead>
                    <tbody>
                        @php $b=1; @endphp
                        @foreach($query as $row)
                        <tr>
                            <td>{{$b++;}}</td>
                            @for($i = 0; $i < count($th); $i++) @if($th[$i]=='kode_deskripsi' ) <td>{{ $row->kode_deskripsi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_posisi')
                                <td>{{ $row->nama_posisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'keterangan')
                                <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'deskripsi_pekerjaan')
                                <td><?= $row->detail_deskripsi ?? 'NO DATA' ?></td>
                                @endif
                                
                                @if($th[$i] == 'tanggal_mulai_efektif')
                                <td>{{ $row->tanggal_mulai_efektif ? date('d-m-Y', strtotime($row->tanggal_mulai_efektif)) : 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tanggal_selesai_efektif')
                                <td>{{ $row->tanggal_selesai_efektif ? date('d-m-Y', strtotime($row->tanggal_selesai_efektif)) : 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nomor_dokumen')
                                <td>{{ $row->nomor_dokumen ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'edisi')
                                <td>{{ $row->edisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tanggal_edisi')
                                <td>{{ $row->tanggal_edisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nomor_revisi')
                                <td>{{ $row->nomor_revisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tanggal_revisi')
                                <td>{{ $row->tanggal_revisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_jabatan')
                                <td>{{ $row->nama_jabatan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_karyawan')
                                <td>{{ $row->nama_karyawan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'divisi')
                                <td>{{ $row->divisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'lokasi_kerja')
                                <td>{{ $row->lokasi_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'dibuat_oleh')
                                <td>{{ $row->dibuat_oleh ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'diperiksa_oleh')
                                <td>{{ $row->diperiksa_oleh ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'fungsi_jabatan')
                                <td>{{ $row->fungsi_jabatan ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'tanggung_jawab')
                                <td>{{ $row->tanggung_jawab ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'departemen')
                                <td>{{ $row->departemen ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'kode_posisi')
                                <td>{{ $row->kode_posisi ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_lokasi_kerja')
                                <td>{{ $row->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'nama_pengawas')
                                <td>{{ $row->nama_pengawas ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'lingkup_aktivitas')
                                <td>{{ $row->lingkup_aktivitas ?? 'NO DATA' }}</td>
                                @endif
                                @if($th[$i] == 'id_logo_perusahaan')
                                <td>{{ $row->id_logo_perusahaan ?? 'NO DATA' }}</td>
                                @endif
                                @if($i == count($th) - 1)
                                <td>
                                    <a href="{{ URL::to('/list_hdp/detail/'.$row->id_deskripsi_pekerjaan) }}">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a href="{{ URL::to('/list_hdp/edit/'.$row->id_deskripsi_pekerjaan) }}">Edit</a>
                                </td>
                                <td>
                                    <input type="checkbox" name="multiDelete[]" value="{{ $row->id_deskripsi_pekerjaan }}">
                                </td>
                                @endif
                                @endfor
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_hdp')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                        <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                        <td>
                            <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Hapus</button>
                        </td>
                    </tr>
                </table>
            </form>
            @else
            <h4>Data tidak ditemukan :(</h4>
            @endif
        </div>
    </div>
</div>
@endsection