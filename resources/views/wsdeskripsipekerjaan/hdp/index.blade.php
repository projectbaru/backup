@extends('template.default')
@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        @if (Request::is('list_semua_wsperusahaan'))
        <a type="submit" href="{{ route('ubah_tampilandp') }}" class="">Buat Tampilan Baru</a>|&nbsp;&nbsp;<a type="submit" href="{{ '/set_back_display' }}" class="">Kembali</a>
        @else
        <a type="submit" href="{{ route('ubah_tamhdp') }}" class="">Buat Tampilan Baru</a>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <form action="{{ URL::to('/list_dp/hapus_banyak') }}" method="POST">
                @csrf
                <table id="example" class="table table-striped table-bordered table-sm" style="font-size: 12px; border:1px solid #d9d9d9;">
                    <thead style="color:black;">
                        <tr>
                            <th>No</th>
                            <th scope="col">Kode Deskripsi</th>
                            <th scope="col">Nama Posisi</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">Deskripsi Pekerjaan</th>
                            <th scope="col">Aksi</th>
                            <th scope="col">V</th>

                        </tr>
                    </thead>
                    <tbody>
                        @php $b=1; @endphp
                        @foreach ($wsdeskripsi_hdp as $data)
                        <tr>
                            <td>{{$b++;}}</td>
                            <td>{{ $data->kode_deskripsi }}</td>
                            <td>{{ $data->nama_posisi }}</td>
                            <td>{{ $data->keterangan }}</td>
                            <td><?= $data->detail_deskripsi ?></td>
                            <td>
                                <a href="{{ URL::to('/list_hdp/detail/'.$data->id_deskripsi_pekerjaan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="{{ URL::to('/list_hdp/edit/'.$data->id_deskripsi_pekerjaan) }}" class="">Edit</a>
                            </td>
                            <td>
                                <input type="checkbox" name="multiDelete[]" value="{{ $data->id_deskripsi_pekerjaan }}">
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_hdp')}}">Tambah</a></td>
                        <td>
                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="javascript:;">Hapus</button>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
@endsection
@section('add-scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endsection