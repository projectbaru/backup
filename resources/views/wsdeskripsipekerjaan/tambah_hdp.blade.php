<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title></title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
{{-- MALDINI --}}
<style>
    .autocomplete {
        position: relative;
        display: inline-block;
    }

    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }

    .autocomplete-items {
        padding: 10px;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
    }

    /*when hovering an item:*/
    .autocomplete-items:hover {
        background-color: #e9e9e9;
    }

    /*when navigating through the items using the arrow keys:*/
    .autocomplete-active {
        background-color: DodgerBlue !important;
        color: #ffffff;
    }
</style>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">Interface</div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header" style="color:black;">Menu Master:</h6>
                        <a class="collapse-item" href="{{ route('list_p') }}">Perusahaan</a>
                        <a class="collapse-item" href="{{ route('list_a') }}">Alamat</a>
                        <a class="collapse-item" href="{{ route('list_o') }}">Organisasi</a>
                        <a class="collapse-item" href="{{ route('list_to') }}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{ route('list_g') }}">Golongan</a>
                        <a class="collapse-item" href="{{ route('list_tg') }}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{ route('list_po') }}">Posisi</a>
                        <a class="collapse-item" href="{{ route('list_tp') }}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{ route('list_lk') }}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{ route('list_glk') }}">Grup Lokasi Kerja</a>
                        <a class="collapse-item" href="{{ route('list_kc') }}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <!-- <a class="collapse-item active" href="{{ route('list_dp') }}" style="background-color:#1053af;color:white;">Dekripsi Pekerjaan</a> -->
                        <a class="collapse-item active" aria-expanded="true" aria-controls="collapseTwo" href="{{route('list_dp')}}" data-target="#collapseTre" style="background-color:#1053af;color:white;">Deskripsi Pekerjaan</a>
                        <div id="collapseTre" class="collapse show" aria-labelledby="headingTwo"
                            data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                                <!-- <h6 class="collapse-header" style="color:black;"></h6> -->
                                <a class="collapse-item active" href="{{route('list_hdp')}}">Header Deskripsi Pekerjaan</a>
                                <a class="collapse-item" href="{{route('list_dp')}}" >Detail Deskripsi</a>
                            </div>
                        </div>
                        <a class="collapse-item" href="{{ route('list_j') }}">Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_kj') }}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_tkj') }}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_usm') }}">Upah Standar Minimum</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
            <div id="content">
                <br>
                <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg"></a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal"
                                    data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout </a>
                            </div>
                        </li>
                    </ul>
                </nav> -->
                <div class="container-fluid" style="background-color:white;">
                    <div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">
                        <div class="container" style="width:100%; ">
                            <br>
                            <div>
                               
                                <div class="container-fluid mt-2"
                                    style="border:1px solid #d9d9d9;border-radius:10px;">
                                    <div class="" style=""><br>
                                    <h5><b style="color:black;">Tambah Deskripsi Pekerjaan</b></h5>
                                </div> 
                                    <form autocomplete="off" action="{{ URL::to('/simpan_hdp') }}" method="POST"
                                        enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <hr>
                                        <div class="form-group container" style="width:90%;">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col" style="font-size: 10px;">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm"
                                                                class="col-sm-2 col-form-label col-form-label-sm"
                                                                style="font-size:11px;">Kode Deskripsi
                                                                Pekerjaan</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="kode_deskripsi"
                                                                    class="form-control form-control-sm" require
                                                                    id="colFormLabelSm" placeholder=""  value="DP-{{now()->isoFormat('hms')}}" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm"
                                                                class="col-sm-2 col-form-label col-form-label-sm"
                                                                style="font-size:11px;">Nomor Dokumen</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="nomor_dokumen"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm"
                                                                class="col-sm-2 col-form-label col-form-label-sm"
                                                                style="font-size:11px;">Edisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="edisi"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                                Edisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="date" style="font-size:11px;"
                                                                    name="tanggal_edisi"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Nomor
                                                                Revisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="nomor_revisi"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                                Revisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="date" style="font-size:11px;"
                                                                    name="tanggal_revisi"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                                Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="nama_jabatan"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                                Karyawan</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="nama_karyawan"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Divisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="divisi"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Departemen</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="departemen"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                                Lokasi Kerja</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="nama_lokasi_kerja"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <!-- <b style="color:black;">Detail Pajak Perusahaan</b> -->
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label  for="colFormLabelSm" style="font-size:11px;"
                                                                class=" col-sm-2 col-form-label col-form-label-sm">Nama
                                                                Pengawas</label>
                                                            <div class="col-sm-10 autocomplete">
                                                                <input id="myInput" type="text" style="font-size:11px;"
                                                                    name="nama_pengawas"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Fungsi
                                                                Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="fungsi_jabatan"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Lingkup
                                                                Aktivitas</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="lingkup_aktivitas"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="responsibilityInputContainer">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm"
                                                                required>Tanggung Jawab 1</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="tanggung_jawab[]"
                                                                    class="form-control form-control-sm">
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <button type="button" class="btn btn-sm btn-primary" id="addResponsibilityInput">
                                                                    <i class="fa-solid fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="authorityInputContainer">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Wewenang
                                                                1</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="wewenang[]"
                                                                    class="form-control form-control-sm" required>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <button type="button" class="btn btn-sm btn-primary" id="addAuthorityInput">
                                                                    <i class="fa-solid fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="qualificationInputContainer">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi
                                                                Jabatan 1</label>
                                                            <div class="col-sm-3">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="kualifikasi_jabatan[]"
                                                                    class="form-control form-control-sm" required>
                                                            </div>
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Tipe
                                                                Kualifikasi 1</label>
                                                            <div class="col-sm-4">
                                                                <select name="tipe_kualifikasi[]"
                                                                    class="form-control form-control-sm" required>
                                                                    <option value="" selected disabled>--- Belum
                                                                        dipilih ---</option>
                                                                    <option value="a">a</option>
                                                                    <option value="b">b</option>
                                                                    <option value="c">c</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <button type="button" class="btn btn-sm btn-primary" id="addQualificationInput">
                                                                    <i class="fa-solid fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">ID</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">ID
                                                                Logo Perusahaan</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;"
                                                                    name="id_logo_perusahaan"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">Rekaman Informasi</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                                Mulai Efektif</label>
                                                            <div class="col-sm-10">
                                                                <input type="date" style="font-size:11px;"
                                                                    name="tanggal_mulai_efektif"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Tanggal
                                                                Selesai Efektif</label>
                                                            <div class="col-sm-10">
                                                                <input type="date" style="font-size:11px;"
                                                                    name="tanggal_selesai_efektif"
                                                                    class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;"
                                                                class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                                                            <div class="col-sm-10">
                                                                <textarea type="text" style="font-size:11px;" name="keterangan" class="form-control form-control-sm"
                                                                    id="colFormLabelSm" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="container" style="width: 100%;">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <a href="{{ route('list_p') }}"
                                                            class="btn btn-danger btn-sm"
                                                            style="border:none;border-radius:5px;">Batal</a>
                                                    </td>
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm"
                                                            style="border:none;border-radius:5px;">Simpan</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br>
                                    </form>
                                </div>
                                <br>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
                
                <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
                <script src="{{ asset('js/jquery.min.js') }}"></script>
                <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
                <script>
                    function autocomplete(inp, arr) {
                        var currentFocus;
                        inp.addEventListener("input", function(e) {
                            var a, b, i, val = this.value;
                            closeAllLists();
                            if (!val) {
                                return false;
                            }
                            currentFocus = -1;
                            a = document.createElement("DIV");
                            a.setAttribute("id", this.id + "autocomplete-list");
                            a.setAttribute("class", "autocomplete-items");
                            this.parentNode.appendChild(a);
                            for (i = 0; i < arr.length; i++) {
                                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                                    b = document.createElement("DIV");
                                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                                    b.innerHTML += arr[i].substr(val.length);
                                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                                    b.addEventListener("click", function(e) {
                                        inp.value = this.getElementsByTagName("input")[0].value;
                                        closeAllLists();
                                    });
                                    a.appendChild(b);
                                }
                            }
                        });
                        inp.addEventListener("keydown", function(e) {
                            var x = document.getElementById(this.id + "autocomplete-list");
                            if (x) x = x.getElementsByTagName("div");
                            if (e.keyCode == 40) {
                                currentFocus++;
                                addActive(x);
                            } else if (e.keyCode == 38) { //up
                                currentFocus--;
                                addActive(x);
                            } else if (e.keyCode == 13) {
                                e.preventDefault();
                                if (currentFocus > -1) {
                                    if (x) x[currentFocus].click();
                                }
                            }
                        });

                        function addActive(x) {
                            if (!x) return false;
                            removeActive(x);
                            if (currentFocus >= x.length) currentFocus = 0;
                            if (currentFocus < 0) currentFocus = (x.length - 1);
                            x[currentFocus].classList.add("autocomplete-active");
                        }

                        function removeActive(x) {
                            for (var i = 0; i < x.length; i++) {
                                x[i].classList.remove("autocomplete-active");
                            }
                        }

                        function closeAllLists(elmnt) {
                            var x = document.getElementsByClassName("autocomplete-items");
                            for (var i = 0; i < x.length; i++) {
                                if (elmnt != x[i] && elmnt != inp) {
                                    x[i].parentNode.removeChild(x[i]);
                                }
                            }
                        }
                        document.addEventListener("click", function(e) {
                            closeAllLists(e.target);
                        });
                    }
                    var nama = [];
                    @foreach ($data as $d)
                        nama.push(@json($d->name));
                    @endforeach
                    autocomplete(document.getElementById("myInput"), nama);
                </script>
                @include('js.wsdeskripsipekerjaan.tambah')
</body>

</html>
