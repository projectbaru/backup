<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Deskripsi Pekerjaan</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<style>
    .autocomplete {
        position: relative;
        display: inline-block;
    }

    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }

    .autocomplete-items {
        padding: 10px;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
    }

    /*when hovering an item:*/
    .autocomplete-items:hover {
        background-color: #e9e9e9;
    }

    /*when navigating through the items using the arrow keys:*/
    .autocomplete-active {
        background-color: DodgerBlue !important;
        color: #ffffff;
    }
</style>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">Interface</div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header" style="color:black;">Menu Master:</h6>
                        <a class="collapse-item" href="{{ route('list_p') }}">Perusahaan</a>
                        <a class="collapse-item" href="{{ route('list_a') }}">Alamat</a>
                        <a class="collapse-item" href="{{ route('list_o') }}">Organisasi</a>
                        <a class="collapse-item" href="{{ route('list_to') }}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{ route('list_g') }}">Golongan</a>
                        <a class="collapse-item" href="{{ route('list_tg') }}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{ route('list_po') }}">Posisi</a>
                        <a class="collapse-item" href="{{ route('list_tp') }}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{ route('list_lk') }}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{ route('list_glk') }}">Grup Lokasi Kerja</a>
                        <a class="collapse-item" href="{{ route('list_kc') }}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item active" href="{{ route('list_dp') }}" style="background-color:#1053af;color:white;">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{ route('list_j') }}">Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_kj') }}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_tkj') }}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{ route('list_usm') }}">Upah Standar Minimum</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
            <div id="content">
                <br>

                <div class="container-fluid" style="background-color:white;">
                    <div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">
                        <div class="container" style="width:100%; ">
                            <br>
                            <div>

                                <div class="container-fluid mt-2" style="border:1px solid #d9d9d9;border-radius:10px;">
                                    <div class="" style=""><br>
                                        <h5><b style="color:black;">Ubah Deskripsi Pekerjaan</b></h5>
                                    </div>
                                    <hr>
                                    <form autocomplete="off" action="{{ URL::to('/update_dp') }}" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="">
                                                <b style="color:black;">Informasi</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode
                                                                Posisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;" name="kode_posisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->kode_posisi }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Nama
                                                                Posisi</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" style="font-size:11px;" name="nama_posisi" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wsdeskripsipekerjaan->nama_posisi }}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="descriptionInputContainer">

                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">Rekaman Informasi</b>
                                                <div class="col" style="font-size: 10px;">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                        <div class="col-sm-10">
                                                            <input type="date" style="font-size:11px;" name="tanggal_mulai_efektif" value="{{ $wsdeskripsipekerjaan->tanggal_mulai_efektif }}" class="form-control form-control-sm" require id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                        <div class="col-sm-10">
                                                            <input type="date" style="font-size:11px;" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{ $wsdeskripsipekerjaan->tanggal_selesai_efektif }}" require id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                        <div class="col-sm-10">
                                                            <textarea type="text" style="font-size:11px;" rows="4" cols="50" name="keterangan" value="{{ $wsdeskripsipekerjaan->keterangan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $wsdeskripsipekerjaan->keterangan }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="container" style="width: 100%;">
                                            <table>
                                                <tr>
                                                    <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                                    <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                                    <td>
                                                        <a href="{{ route('list_dp') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                    </td>
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br>
                                    </form>
                                </div>
                                <br>
                            </div>
                        </div>
                        <!-- <footer class="sticky-footer bg-white">
                            <div class="container my-auto">
                                <div class="copyright text-center my-auto">
                                    <span>Copyright &copy; Your Website 2021</span>
                                </div>
                            </div>
                        </footer> -->
                    </div>
                </div>
                <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>

                <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
                <script src="{{ asset('js/jquery.min.js') }}"></script>
                <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

                @include('js.wsdeskripsipekerjaan.edit')
</body>

</html>