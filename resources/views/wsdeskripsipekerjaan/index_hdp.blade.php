<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Deskripsi Pekerjaan</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header" style="color:black;">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}" >Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item active" aria-expanded="true" aria-controls="collapseTwo" href="{{route('list_dp')}}" data-target="#collapseTre" style="background-color:#1053af;color:white;">Deskripsi Pekerjaan</a>
                        <div id="collapseTre" class="collapse show" aria-labelledby="headingTwo"
                            data-parent="#accordionSidebar">
                            <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                                <!-- <h6 class="collapse-header" style="color:black;"></h6> -->
                                <a class="collapse-item active" href="{{route('list_hdp')}}">Header Deskripsi Pekerjaan</a>
                                <a class="collapse-item" href="{{route('list_dp')}}" >Detail Deskripsi</a>
                            </div>
                        </div>
                           

                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column" >
            <div id="content" style="background-color:white;">
                <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow navbar-sm">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Douglas McGee</span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    <a class="" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">@csrf</form>
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav>                                             -->
                <div >
                </div><br>
                <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" >
                    @if (Route::has('login'))
                        <div class="hidden fixed right-0 sm:block">
                            @auth
                                <!-- <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a> -->
                            @else
                                <a href="{{ route('login') }}"
                                    class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}"
                                        class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                                @endif
                            @endauth
                            <!-- <a href="{{ url('/filter') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Filter</a> -->
                        </div>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-danger alert-success fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if ($message = Session::get('danger'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                                <h5 style="color:black;">Header Deskripsi Pekerjaan</h5><hr>
                                <table style="font-size:12px;">
                                    <tr>
                                        <td>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr style="font-size:12px;color:black;">
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <!-- <form action="{{route('cari_wsperusahaan')}}" class="form-inline" method="GET">
                                                <input class="form-control form-control-sm" type="search" name="cari" placeholder="Cari data" aria-label="Cari data">
                                                <a class="btn btn-outline-secondary my-2 my-sm-0  btn-sm" type="submit"><i class="fas fa-search"></i>&nbsp;Cari</a>
                                            </form> -->
                                            <!-- <input type="text" style="font-size:11px;" class="form-control form-control-sm"></td> -->
                                        <td>
                                            <!-- <a type="submit" class="btn btn-sm btn-primary" style="font-size:11px;">Go</a> -->
                                        </td>
                                        <td></td>
                                        <td></td>
                                        @if (Request::is('list_semua_wsperusahaan'))
                                            <td style="text-decoration:none;">
                                                <!-- <a type="submit" class="" href="{{ route('') }}">Ubah</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                <a
                                                    type="submit" href="{{ route('ubah_tampilandp') }}"
                                                    class="">Buat Tampilan Baru</a>|&nbsp;&nbsp;<a
                                                    type="submit" href="{{ '/set_back_display' }}"
                                                    class="">Kembali</a>
                                            </td>
                                        @else
                                            <td style="text-decoration:none;">
                                                <!-- <a type="submit" class="" href="{{ route('ubah_tampilanperusahaan') }}">Ubah</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                                <a
                                                    type="submit" href="{{ route('ubah_tamhdp') }}"
                                                    class="">Buat Tampilan Baru</a>
                                            </td>
                                        @endif
                                    </tr>
                                </table><hr>
                                <form action="{{ URL::to('/list_dp/hapus_banyak') }}" method="POST">
                                @csrf
                                <table id="example" class="table table-striped table-bordered table-sm" style="font-size: 12px; border:1px solid #d9d9d9;">
                                    <thead style="color:black;">
                                        <tr>
                                            <th>No</th>
                                            <th scope="col">Kode Deskripsi</th>
                                            {{-- <th scope="col">Logo Perusahaan</th> --}}
                                            <th scope="col">Nama Posisi</th>
                                            <th scope="col">Keterangan</th>
                                            <th scope="col">Deskripsi Pekerjaan</th>
                                            <th scope="col">Aksi</th>
                                            <th scope="col">V</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $b=1; @endphp
                                        @foreach ($wsdeskripsi_hdp as $data)
                                        {{-- {{dd($data)}} --}}
                                            <tr>
                                                <td>{{$b++;}}</td>
                                                <td>{{ $data->kode_deskripsi }}</td>
                                                {{-- <td>{{$data -> logo_perusahaan}}</td> --}}
                                                <td>{{ $data->nama_posisi }}</td>
                                                <td>{{ $data->keterangan }}</td>
                                                <td>{{ $data->deskripsi_pekerjaan }}</td>
                                                <td>
                                                    <a href="{{ URL::to('/list_hdp/detail/'.$data->id_deskripsi_pekerjaan) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                                    <a href="{{ URL::to('/list_hdp/edit/'.$data->id_deskripsi_pekerjaan) }}" class="">Edit</a>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="multiDelete[]" value="{{ $data->id_deskripsi_pekerjaan }}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('tambah_hdp')}}">Tambah</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td>
                                    </tr>
                                </table>
                                </form>
                            </div>
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <!-- <span>Copyright &copy; Your Website 2021</span> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
        $('#example').DataTable();
        });
    </script>
</body>
</html>
