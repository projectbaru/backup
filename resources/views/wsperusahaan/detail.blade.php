<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Perusahaan</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
                        <a class="collapse-item active" href="{{route('list_p')}}" style="background-color:#1053af;color:white;">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
		
		<br>
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style=""><br>
					<div class="container" style="width:90%; ">
						<div>
							<div class="container mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
                            <h5 style="color:black;">Detail Perusahaan</h5>	
                            <hr>	
                            <form action="{{route('simpan_pd')}}" method="post">{{ csrf_field() }}
                                @foreach($wsperusahaan as $data)
                                <table>
                                    <tr>
                                        
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_p', $data->id_perusahaan)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_p', $data->id_perusahaan)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_p')}}">Batal</a></td>

                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                <b style="color:black;">Informasi Perusahaan</b><br><br>
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Logo Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="perusahaan_logo" value="{{$data->perusahaan_logo}}"><img src="{{url('/data_file/'.$data -> perusahaan_logo)}}" name="perusahaan_logo" value="{{$data->perusahaan_logo}}" width="100px" alt=""></td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_perusahaan" value="{{$data->kode_perusahaan}}">{{$data->kode_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Singkatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="singkatan" value="{{$data->singkatan}}">{{$data->singkatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Visi Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="visi_perusahaan" value="{{$data->visi_perusahaan}}">{{$data->visi_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Misi Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="misi_perusahaan" value="{{$data->misi_perusahaan}}">{{$data->misi_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nilai Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nilai_perusahaan" value="{{$data->nilai_perusahaan}}">{{$data->nilai_perusahaan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table>
                                                <!-- <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                </tr> -->
                                                <tr>
                                                    <td>Tanggal Mulai Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_perusahaan" value="{{$data->tanggal_mulai_perusahaan}}">{{$data->tanggal_mulai_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_perusahaan" value="{{$data->tanggal_selesai_perusahaan}}">{{$data->tanggal_selesai_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="jenis_perusahaan" value="{{$data->jenis_perusahaan}}">{{$data->jenis_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jenis Bisnis Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="jenis_bisnis_perusahaan" value="{{$data->jenis_bisnis_perusahaan}}">{{$data->jenis_bisnis_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Karyawan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="jumlah_karyawan" value="{{$data->jumlah_karyawan}}">{{$data->jumlah_karyawan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                <hr>
                                <b style="color:black;">Detail Pajak Perusahaan</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Nomor NPWP Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nomor_npwp_perusahaan" value="{{$data->nomor_npwp_perusahaan}}">{{$data -> nomor_npwp_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Lokasi Pajak</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="lokasi_pajak" value="{{$data->lokasi_pajak}}">{{$data -> lokasi_pajak}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" style="font-size:12px;">
                                            <table>
                                                <tr>
                                                    <td>NPP</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="npp" value="{{$data->npp}}">{{$data -> npp}}</td>
                                                </tr>
                                                <tr>
                                                    <td>NPKP</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="npkp" value="{{$data->npkp}}">{{$data -> npkp}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                <b style="color:black;">ID Logo</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>ID Logo Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="id_logo_perusahaan" value="{{$data->id_logo_perusahaan}}">{{$data -> id_logo_perusahaan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    
                                    </div>
                                <hr>
                                <b style="color:black;">Rekaman Informasi</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data -> tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data -> tanggal_selesai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data -> keterangan}}</td>
                                                </tr>
                                            </table><br>
                                        </div>

                                    </div>

                                    
                                @endforeach
                            </form>
							</div>
							<br>

                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>