<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Lokasi Kerja</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

   
</head>
<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0"> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item " href="{{route('list_a')}}" >Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item active" href="{{route('list_lk')}}" style="background-color:#1053af;color:white;">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_glk')}}" >Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
                    </div>
                </div>
            </li>
            
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column" >
            <div id="content" style="background-color:white;">
                                                 
                <div >
                </div><br>
                
                <div class="container-fluid" style="padding-bottom:20px;padding-top:20px; border:1px solid #d9d9d9;border-radius:10px;width:90%;" >
                    @if (Route::has('login'))
                        <div class="hidden fixed right-0 sm:block">
                            @auth
                                <!-- <a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a> -->
                            @else
                                <a href="{{ route('login') }}"
                                    class="text-sm text-gray-700 dark:text-gray-500 underline">Log in</a>
                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}"
                                        class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Register</a>
                                @endif
                            @endauth
                            <!-- <a href="{{ url('/filter') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Filter</a> -->
                        </div>
                    @endif
                    @if($query)
                            @if ($message = Session::get('success'))
                                <div class="alert alert-danger alert-success fade show" role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            @if ($message = Session::get('danger'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <h5 style="color:black;">Lokasi Kerja</h5><hr>
                            <table style="font-size:12px">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style="font-size:12px;color:black;">
                                    <td></td>
                                    <td></td>
                                    <td>
                                    <!-- <form action="{{route('cari_wsperusahaan')}}" class="form-inline" method="GET">
                                        <input class="form-control form-control-sm" type="search" name="cari" placeholder="Cari data ..." aria-label="Cari data ...">
                                        <a class="btn btn-outline-secondary my-2 my-sm-0  btn-sm" type="submit"><i class="fas fa-search"></i>&nbsp;Cari</a>
                                    </form> -->
                                    </td>
                                    <td>
                                        <!-- <a type="submit" class="btn btn-sm btn-primary">Go</a> -->
                                        <!-- <input type="text" style="font-size:11px;" class="form-control form-control-sm"></td> -->
                                    </td>
                                    <td></td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <!-- <a class="{{route('ubah_tampilanperusahaan')}}">Ubah</a>&nbsp;&nbsp;|&nbsp;&nbsp; -->
                                        <a href="{{route ('ubah_tampilanlk')}}" class="">Buat Tampilan Baru</a>
                                        <!-- <a href="{{ '/list_semua_wsperusahaan' }}" class="">Tampilkan semua data</a> -->
                                    </td>
                                </tr>
                            </table><hr>
                            <form action="{{ URL::to('/list_lk/hapus_banyak') }}" method="POST">
                                @csrf
                                    <table id="example" class="table table-bordered table-striped table-hover table-sm" style="font-size:10px;">
                                        <thead>
                                            <th>No</th>
                                            @for($i = 0; $i < count($th); $i++)
                                                <!-- @if($th[$i] == 'id_lokasi_kerja')
                                                    <th></th>
                                                @endif -->
                                                @if($th[$i] == 'kode_lokasi_kerja')
                                                    <th>Kode Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'nama_lokasi_kerja')
                                                    <th>Nama Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'id_grup_lokasi_kerja')
                                                    <th>Id Grup Lokasi Kerja</th>
                                                @endif
                                                @if($th[$i] == 'id_kantor')
                                                    <th>Id Kantor</th>
                                                @endif
                                                @if($th[$i] == 'zona_waktu')
                                                    <th>Zona Waktu</th>
                                                @endif
                                                @if($th[$i] == 'alamat')
                                                    <th>Alamat</th>
                                                @endif
                                                @if($th[$i] == 'provinsi')
                                                    <th>Provinsi</th>
                                                @endif
                                                @if($th[$i] == 'kecamatan')
                                                    <th>Kecamatan</th>
                                                @endif
                                                @if($th[$i] == 'kelurahan')
                                                    <th>Kelurahan</th>
                                                @endif
                                                @if($th[$i] == 'kabupaten_kota')
                                                    <th>Kabupaten Kota</th>
                                                @endif
                                                @if($th[$i] == 'kode_pos')
                                                    <th>Kode Pos</th>
                                                @endif
                                                @if($th[$i] == 'negara')
                                                    <th>Negara</th>
                                                @endif
                                                @if($th[$i] == 'nomor_telepon')
                                                    <th>Nomor Telepon</th>
                                                @endif
                                                @if($th[$i] == 'email')
                                                    <th>Email</th>
                                                @endif
                                                @if($th[$i] == 'fax')
                                                    <th>Fax</th>
                                                @endif
                                                @if($th[$i] == 'keterangan')
                                                    <th>Keterangan</th>
                                                @endif
                                                @if($th[$i] == 'status_rekaman')
                                                    <th>Status Rekaman</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th>Tanggal Mulai Efektif</th>
                                                @endif
                                                @if($th[$i] == 'tanggal_mulai_efektif')
                                                    <th>Tanggal Selesai Efektif</th>
                                                @endif
                                                
                                                @if($i == count($th) - 1)
                                                    <th>Aksi</th>
                                                    <th>V</th>
                                                @endif
                                            @endfor
                                        </thead>
                                        <tbody>
                                            @php $b=1; @endphp
                                            @foreach($query as $row)
                                                <tr>
                                                    <td>{{$b++;}}</td>
                                                    @for($i = 0; $i < count($th); $i++)
                                                        <!-- @if($th[$i] == 'id_lokasi_kerja')
                                                            <td>{{ $row->id_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif -->
                                                        @if($th[$i] == 'kode_lokasi_kerja')
                                                            <td>{{ $row->kode_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nama_lokasi_kerja')
                                                            <td>{{ $row->nama_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_grup_lokasi_kerja')
                                                            <td>{{ $row->id_grup_lokasi_kerja ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'id_kantor')
                                                            <td>{{ $row->id_kantor ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'zona_waktu')
                                                            <td>{{ $row->zona_waktu ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'alamat')
                                                            <td>{{ $row->alamat ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'provinsi')
                                                            <td>{{ $row->provinsi ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kecamatan')
                                                            <td>{{ $row->kecamatan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kelurahan')
                                                            <td>{{ $row->kelurahan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kabupaten_kota')
                                                            <td>{{ $row->kabupaten_kota ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'kode_pos')
                                                            <td>{{ $row->kode_pos ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'negara')
                                                            <td>{{ $row->negara ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'nomor_telepon')
                                                            <td>{{ $row->nomor_telepon ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'email')
                                                            <td>{{ $row->email ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'fax')
                                                            <td>{{ $row->fax ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'keterangan')
                                                            <td>{{ $row->keterangan ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'status_rekaman')
                                                            <td>{{ $row->status_rekaman ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_mulai_efektif')
                                                            <td>{{ $row->tanggal_mulai_efektif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        @if($th[$i] == 'tanggal_selesai_efektif')
                                                            <td>{{ $row->tanggal_selesai_efektif ?? 'NO DATA' }}</td>
                                                        @endif
                                                        
                                                        @if($i == count($th) - 1)
                                                            <td> 
                                                                <a href="{{ URL::to('/list_lk/detail/'.$row->id_lokasi_kerja) }}" class="">Detail</a>&nbsp;&nbsp;|&nbsp;&nbsp;                                                                                                   
                                                                <a href="{{ URL::to('/list_lk/edit/'.$row->id_lokasi_kerja) }}" class="">Edit</a>
                                                            </td>
                                                            <td>
                                                                <input type="checkbox" name="multiDelete[]" value="{{ $row->id_lokasi_kerja }}">

                                                            </td>
                                                        @endif
                                                    @endfor
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <table>
                                        <tr>
                                        <td><a class="btn btn-success btn-sm" href="{{route('tambah_lk')}}" style="font-size:11px;border-radius:5px;">Tambah</a></td>
                                            <!-- <td><a class="btn btn-secondary btn-sm" href="">Ubah</a></td> -->
                                            <td>
                                                <button class="btn btn-danger btn-sm" href="" style="font-size:11px;border-radius:5px;">Hapus</button>
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                            @else
                                    <h4>Data tidak ditemukan :(</h4>
                                @endif
                    </div>
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <!-- <span>Copyright &copy; Your Website 2021</span> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
        $('#example').DataTable();
        });
    </script>
</body>
</html>
