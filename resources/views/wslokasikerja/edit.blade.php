<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Lokasi Kerja</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    
</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item active" href="{{route('list_lk')}}" style="background-color:#1053af;color:white;">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 

                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
            <div id="content">
                <br>
                <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-1 static-top">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav> -->
                <div class="container-fluid" style="background-color:white;">
                    <div class="relative flex items-top justify-center min-h-screen sm:items-center" style="width:100%;background-color:white; ">
                        <div class="" style="width:100%;background-color:white; "><br>
                            <div class="container" style="width:100%;border:1px solid #d9d9d9;border-radius:10px;">
                                    <div class="" style=""><br>
                                        <h5><b style="color:black;">Edit Lokasi Kerja</b></h5>
                                    </div>    
                                    <div class="container-fluid mt-2">
                                    <form action="{{route('update_lk')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        <div class="form-group">
                                            <div class="">
                                                @foreach($wslokasikerja as $data)
                                                <div class="row">
                                                    <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                                <div class="col-sm-10">
                                                                <input type="hidden" name="id_lokasi_kerja" value="{{ $data->id_lokasi_kerja }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                               
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="kode_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_lokasi_kerja}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="nama_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_lokasi_kerja}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Grup Lokasi Kerja</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="id_grup_lokasi_kerja" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->id_grup_lokasi_kerja}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Kantor Cabang</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="id_kantor" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->id_kantor}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Zona Waktu</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="zona_waktu" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->zona_waktu}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alamat</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="alamat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->alamat}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Provinsi</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="provinsi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->provinsi}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kecamatan</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="kecamatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kecamatan}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kelurahan</label>
                                                                <div class="col-sm-10">
                                                                <input required type="text" name="kelurahan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kelurahan}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kabupaten/Kota</label>
                                                            <div class="col-sm-10">
                                                                <select required name="kabupaten_kota" id="kabupaten_kota" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" {{ $data->kabupaten_kota == '' ? 'selected' : NULL }}>--Belum Dipilih--</option>
                                                                    <option value="jakarta_barat" {{ $data->kabupaten_kota == 'jakarta_barat' ? 'selected' : NULL }}>Jakarta Barat</option>
                                                                    <option value="jakarta_selatan" {{ $data->kabupaten_kota == 'jakarta_selatan' ? 'selected' : NULL }}>Jakarta Selatan</option>
                                                                </select>
                                                            <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pos</label>
                                                            <div class="col-sm-10">
                                                            <input required type="text" name="kode_pos" class="form-control form-control-sm" value="{{$data->kode_pos}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Negara</label>
                                                            <div class="col-sm-10">
                                                                <select required name="negara" id="negara" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="" {{ $data->negara == '' ? 'selected' : NULL }}>--Belum Dipilih--</option>
                                                                    <option value="indonesia" {{ $data->negara == 'indonesia' ? 'selected' : NULL }}>Indonesia</option>
                                                                    <option value="-">-</option>
                                                                </select>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Telepon</label>
                                                            <div class="col-sm-10">
                                                            <input required type="text" name="nomor_telepon" class="form-control form-control-sm" id="colFormLabelSm" value="{{$data->nomor_telepon}}" style="font-size:11px;" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Email</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="email" class="form-control form-control-sm" value="{{$data->email}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Fax</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="fax" class="form-control form-control-sm" value="{{$data->fax}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="keterangan" class="form-control form-control-sm" value="{{$data->keterangan}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input required type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_mulai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input required type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>                                                        
                                                <hr>
                                                @endforeach
                                                <div class="container" style="width: 100%;">
                                                    <table>
                                                        <tr>
                                                 
                                                            <td>
                                                                <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_lk',$data->id_lokasi_kerja)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('list_lk') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                <br>
                            </div>
                        </div>
                        <footer class="sticky-footer bg-white">
                            <div class="container my-auto">
                                <div class="copyright text-center my-auto">
                                    <!-- <span>Copyright &copy; Your Website 2021</span> -->
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div> -->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
        $('#example').DataTable();
        });
    </script>
</body>
</html>
