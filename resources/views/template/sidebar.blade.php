 <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black;">
     <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/')}}">
         <div class="sidebar-brand-icon rotate-n-15">
             <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
         </div>
         <div class="sidebar-brand-text mx-3"></div>
     </a>
  
     <hr class="sidebar-divider my-0">
     <li class="nav-item active">
         <a class="nav-link {{  request()->is('/') ? 'active' : '' }}" href="{{url('/')}}">
             <span>Dashboard</span>
         </a>
     </li>
     <hr class="sidebar-divider">
     <div class="sidebar-heading">
         Interface
     </div>
     <li class="nav-item">
         <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
             <span>Master WS</span>
         </a>
         <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
             <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                 <h6 class="collapse-header" style="color:black;">Menu Master:</h6>
                 <a class="collapse-item {{ request()->is('/list_p') ? 'active' : '' }}" href="{{route('list_p')}}">Perusahaan</a>
                 <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                 <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                 <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                 <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                 <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                 <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                 <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                 <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                 <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a>
                 <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                 <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                 <a class="collapse-item {{ Request::path() === 'list_dp' || Request::path() === 'list_hdp' ? 'active' : ''}} {{ ( request()->is('list_dp/*') || request()->is('list_hdp/*') ) ? 'active' : '' }}" aria-expanded="true" aria-controls="collapseTwo" href="{{route('list_dp')}}" data-target="#collapseTre">Deskripsi Pekerjaan</a>
                 <div id="collapseTre" class="collapse {{ Request::path() === 'list_dp' || Request::path() === 'list_hdp' ? 'show' : ''}} {{ ( request()->is('list_dp/*') || request()->is('list_hdp/*') ) ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                     <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                         <a class="collapse-item {{ request()->is('list_hdp') ? 'active' : '' }}" href="{{route('list_hdp')}}">Header Deskripsi Pekerjaan</a>
                         <a class="collapse-item {{ request()->is('list_dp') ? 'active' : '' }}" href="{{route('list_dp')}}">Detail Deskripsi</a>
                     </div>
                 </div>
                 <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                 <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                 <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                 <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
             </div>
         </div>
     </li>
     <hr class="sidebar-divider d-none d-md-block">
     <div class="text-center d-none d-md-inline">
         <button class="rounded-circle border-0" id="sidebarToggle"></button>
     </div>
 </ul>