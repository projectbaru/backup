<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Jabatan</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header">Menu Master:</h6>
						<a class="collapse-item " href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item active" href="{{route('list_j')}}" style="background-color:#1053af;color:white;">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content"><br>
			<!-- <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                        <img class="img-profile rounded-circle" src="img/undraw_profile.svg"></a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Settings </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout </a>
                        </div>
                    </li>
                </ul>
			</nav> -->
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">			
					<div class="container" style="width:100%; ">
						<div>
							<br>
							<div class="container-fluid mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
								<div class="" style="">
                                    <h5><b style="color:black;">Ubah Jabatan</b></h5>
                                </div>  
                                    <form action="{{route('update_j')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        <div class="form-group">
                                            <div class="">
                                                @foreach($wsjabatan as $data)
                                                <div class="row">
                                                    <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                                <div class="col-sm-10">
                                                                <input type="hidden" name="id_jabatan" value="{{ $data->id_jabatan }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                               
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="kode_jabatan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_jabatan}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="nama_jabatan" required class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_jabatan}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="kode_kelompok_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_kelompok_jabatan}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Kelompok Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="nama_kelompok_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_kelompok_jabatan}}" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="grup_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->grup_jabatan}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Jabatan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="deskripsi_jabatan" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi_jabatan}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_mulai" required class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tanggal_mulai}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_selesai" class="form-control form-control-sm" value="{{$data->tanggal_selesai}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Magang</label>
                                                            <div class="col-sm-1">

                                                            <input type="checkbox" name="magang" class="form-control form-control-sm" style="font-size:5px;" id="colFormLabelSm" placeholder="" value="{{$data->magang}}" {{ $data->magang == 'ya' ? 'checked' : NULL }} >
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <select name="tipe_jabatan" id="" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="-" {{ $data->tipe_jabatan == '' ? 'selected' : NULL }}>None</option>
                                                                    <option value="manajerial" {{ $data->tipe_jabatan == 'manajerial' ? 'selected' : NULL }}>Manajerial</option>
                                                                    <option value="administratif" {{ $data->tipe_jabatan == 'administratif' ? 'selected' : NULL }}>Administratif</option>
                                                                    <option value="analis" {{ $data->tipe_jabatan == 'analis' ? 'selected' : NULL }}>Analis</option>

                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                        
                                                    </div> 
                                                </div>
                                                <hr>
                                                <b>Informasi Lainnya</b>
                                                <br>
                                                <br>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                            <div class="col-sm-10">
                                                                <select required style="font-size:11px;" name="dari_golongan" id="produk00" class="form-control form-control-sm" >
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datadg['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->dari_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                            <div class="col-sm-10">
                                                                <select required style="font-size:11px;" name="deskripsi_pekerjaan" id="produk00" class="form-control form-control-sm" >
                                                                    <option value="" selected disabled>--- Pilih ---</option>
                                                                    @foreach ($datadg['looks'] as $v )
                                                                        <option value="{{$v->nama_golongan}}" {{ $v->nama_golongan == $data->sampai_golongan ? 'selected' : NULL }}>{{$v->nama_golongan}}</option>
                                                                    @endforeach
                                                                </select>                                                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b>Rekaman Informasi</b>
                                                <br><br>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_mulai_efektif" class="form-control form-control-sm" required style="font-size:11px;" value="{{$data->tanggal_mulai_efektif}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                            <div class="col-sm-10">
                                                            <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                            <div class="col-sm-10">
                                                            <textarea type="text" rows="4" cols="50" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->keterangan}}" id="colFormLabelSm" placeholder="">{{$data->keterangan}}</textarea>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>                                          
                                                </div>  
                                                
                                                @endforeach
                                                <div class="container" style="width: 100%;">
                                                    <table>
                                                        <tr>
                
                                                        <!-- <td>
                                                            <a href="" class="btn">Hapus</a>
                                                        </td> -->
                                                            <td>
                                                                <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_j',$data->id_jabatan)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('list_j') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                            </div>
							<br>
                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>