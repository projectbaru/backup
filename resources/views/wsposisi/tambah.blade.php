<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Posisi</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item active" href="{{route('list_po')}}" style="background-color:#1053af;color:white;">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
            <div id="content">
                <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-1 static-top">
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="navbar-nav ml-auto">
                        <div class="topbar-divider d-none d-sm-block"></div>

                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                                <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Settings
                                </a>
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Activity Log
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </nav> -->
                <br>
                <div class="container-fluid" style="background-color:white;">
                    <div class="relative flex items-top justify-center min-h-screen sm:items-center" style="width:100%;background-color:white; ">
                        <div class="" style="width:100%;background-color:white; "><br>
                            <div class="container" style="width:100%;border:1px solid #d9d9d9;border-radius:10px;">
                                    <div class="" style=""><br>
                                        <h5><b style="color:black;">Tambah Posisi</b></h5>
                                    </div> 
                                    <div class="container-fluid mt-2">
                                        <form action="{{route('simpan_po')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                            <hr>
                                            <div class="form-group container" style="color:black;">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="nama_perusahaan" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($datap['looks'] as $p )
                                                                        <option value="{{$p->nama_perusahaan}}">{{$p->nama_perusahaan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Posisi</label>
                                                                <div class="col-sm-10">
                                                                    <!-- <select required name="kode_posisi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($datapo['looks'] as $po )
                                                                        <option value="{{$po->kode_posisi}}">{{$po->kode_posisi}}</option>
                                                                        @endforeach
                                                                    </select> -->
                                                                    <input type="text" value="POS-{{now()->isoFormat('hms')}}" readonly required name="kode_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" required name="nama_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode MPP</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" required name="kode_mpp" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="tingkat_posisi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($datatp['looks'] as $po )
                                                                        <option value="{{$po->urutan_tingkat}}">{{$po->urutan_tingkat}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-lael col-form-label-sm" style="font-size:11px;">Detail Tingkat Posisi</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" name="detail_tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="nama_jabatan" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($dataj['looks'] as $j )
                                                                        <option value="{{$j->nama_jabatan}}">{{$j->nama_jabatan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <!-- <input type="text" required name="nama_jabatan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="nama_organisasi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($datao['looks'] as $o )
                                                                        <option value="{{$o->nama_organisasi}}">{{$o->nama_organisasi}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="nama_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Organisasi</label>
                                                                <div class="col-sm-10">
                                                                    <select required name="tingkat_organisasi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datao['looks'] as $o )
                                                                        <option value="{{$o->tingkat_organisasi}}">{{$o->tingkat_organisasi}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                <!-- <input type="text" required name="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Posisi</label>
                                                                <div class="col-sm-10">
                                                                    <select name="tipe_posisi" id="tipe_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                                                        <option value="none">None</option>
                                                                        <option value="tunggal">Tunggal</option>
                                                                        <option value="banyak">Banyak</option>
                                                                    </select>
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan Posisi</label>
                                                                <div class="col-sm-10">
                                                                    <textarea type="text" name="deskripsi_pekerjaan_posisi" rows="4" cols="50" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi Atasan</label>
                                                                <div class="col-sm-10">
                                                                    <!-- <select required name="tingkat_organisasi" id="produk00" class="form-control form-control-sm"  style="font-size:11px;">
                                                                        <option value="" selected >--- Pilih ---</option>
                                                                        @foreach ($datapo['looks'] as $po )
                                                                        <option value="{{$po->nama_posisi_atasan}}">{{$po->nama_posisi_atasan}}</option>
                                                                        @endforeach
                                                                    </select> -->
                                                                    <input type="text" name="nama_posisi_atasan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    
                                                    <b style="color:black;">Informasi Lainnya</b><hr>
                                                   
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                                <div class="col-sm-10">
                                                                    <select required style="font-size:11px;" name="dari_golongan" class="form-control form-control-sm">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datag['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <!-- <input type="text" name="dari_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                                <div class="col-sm-10">
                                                                    <select required style="font-size:11px;" name="sampai_golongan" class="form-control form-control-sm">
                                                                        <option value="" selected disabled>--- Pilih ---</option>
                                                                        @foreach ($datag['looks'] as $p )
                                                                        <option value="{{$p->nama_golongan}}">{{$p->nama_golongan}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <!-- <input type="text" name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Area</label>
                                                            <div class="col-sm-10">
                                                                <select name="tipe_area" id="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="BO">BO</option>
                                                                    <option value="HO">HO</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                    <b style="color:black;">Flag Posisi</b>
                                                    <hr>
                                                    <div class="row">  
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Posisi Aktif</label>
                                                                <div class="col-sm-5">
                                                                <input type="checkbox" name="posisi_aktif" value="yes" style="font-size:11px;" id="colFormLabelSm">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Komisi</label>
                                                                <div class="col-sm-5">
                                                                    <input type="checkbox" name="komisi" value="yes" style="font-size:11px;" id="colFormLabelSm">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kepala Fungsional</label>
                                                                <div class="col-sm-5">
                                                                    <input type="checkbox" value="yes" name="kepala_fungsional" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Flag Operasional</label>
                                                                <div class="col-sm-5">
                                                                <input type="checkbox" value="yes" name="flag_operasional" style="font-size:11px;" class="" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status Posisi</label>
                                                                <div class="col-sm-5">

                                                                    <div class="form-check col" style="font-size:11px;">
                                                                        <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios1" value="kosong">
                                                                        <label class="form-check-label" for="exampleRadios1">
                                                                            Kosong
                                                                        </label><br>
                                                                        <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios2" value="terisi">
                                                                        <label class="form-check-label" for="exampleRadios2">
                                                                            Terisi
                                                                        </label><br>
                                                                        <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios3" value="terisi(pjt)">
                                                                        <label class="form-check-label" for="exampleRadios3">
                                                                            Terisi (PJT)
                                                                        </label><br>
                                                                    </div>     
                                                                </div>
                                                            </div>
                                                        </div> 
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Surat</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" name="nomor_surat" style="font-size:11px;" class="form-control form-control-sm" class="" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
                                                                <div class="col-sm-10">
                                                                <input type="number" name="jumlah_karyawan_dengan_posisi_ini" class="form-control form-control-sm" id="colFormLabelSm" style="font-size:11px;" placeholder="">
                                                                </div>
                                                            </div>                                           
                                                        </div> 
                                                        </div><br>
                                                        <b style="color:black;">Rekaman Informasi</b>
                                                        <hr>
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" required name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">keterangan</label>
                                                                <div class="col-sm-10">
                                                                    <textarea type="text" name="keterangan" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="" rows="4" cols="50"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>                                                         
                                                    <hr>
                                                </div>
                                            </div>
                                            <div class="container" style="width: 100%;">
                                                <table>
                                                    <tr>
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_po') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </div>
            
                                        </form>
                                    </div>
                                    <br>
                            </div>
                        </div>
                        
                        <footer class="sticky-footer bg-white">
                            <div class="container my-auto">
                                <div class="copyright text-center my-auto">
                                    <!-- <span>Copyright &copy; Your Website 2021</span> -->
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div> -->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
