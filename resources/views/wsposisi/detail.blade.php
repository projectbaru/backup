<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Posisi</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header">Menu Master:</h6>
					<a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                    <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                    <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                    <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                    <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                    <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                    <a class="collapse-item" href="{{route('list_po')}}" style="background-color:#1053af;color:white;">Posisi</a>
                    <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                    <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                    <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                    <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                    <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                    <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                    <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                    <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
			<!-- <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                        <img class="img-profile rounded-circle" src="img/undraw_profile.svg"></a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Settings </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout </a>
                        </div>
                    </li>
                </ul>
			</nav> -->
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style=""><br>
					<div class="container" style="width:90%; ">
						<div>
							<div class="container mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
                            <h5 style="color:black;">Detail Posisi</h5>	
                            <hr>	
                            <form action="{{route('simpan_po')}}" method="post">{{ csrf_field() }}
                                @foreach($wsposisi as $data)
                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_po', $data->id_posisi)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_po', $data->id_posisi)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_po')}}">Batal</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Nama Perusahaan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Kode Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_posisi" value="{{$data->kode_posisi}}">{{$data->kode_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                                                </tr>                          
                                                <tr>
                                                    <td>Kode MPP</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_mpp" value="{{$data->kode_mpp}}">{{$data->kode_mpp}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tingkat Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tingkat_posisi" value="{{$data->tingkat_posisi}}">{{$data->tingkat_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Detail Tingkat Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="detail_tingkat_posisi" value="{{$data->detail_tingkat_posisi}}">{{$data->detail_tingkat_posisi}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Nama Jabatan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_jabatan" value="{{$data->nama_jabatan}}">{{$data->nama_jabatan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Organisasi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_organisasi" value="{{$data->nama_organisasi}}">{{$data->nama_organisasi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tingkat Organisasi </td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tingkat_organisasi" value="{{$data->tingkat_organisasi}}">{{$data->tingkat_organisasi}}</td>
                                                </tr>                          
                                                <tr>
                                                    <td>Tipe Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tipe_posisi" value="{{$data->tipe_posisi}}">{{$data->tipe_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Deskripsi Pekerjaan Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="deskripsi_pekerjaan_posisi" value="{{$data->deskripsi_pekerjaan_posisi}}">{{$data->deskripsi_pekerjaan_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Posisi Atasan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_posisi_atasan" value="{{$data->nama_posisi_atasan}}">{{$data->nama_posisi_atasan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>
                                    <b style="color:black;">Informasi Lainnya</b>
                                    <hr>
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Dari Golongan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="dari_golongan" value="{{$data->dari_golongan}}">{{$data->dari_golongan}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Sampai Golongan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="sampai_golongan" value="{{$data->sampai_golongan}}">{{$data->sampai_golongan}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tipe Area</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tipe_area" value="{{$data->tipe_area}}">{{$data->tipe_area}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div><br>
                                    <b style="color:black;">Flag Posisi</b>
                                    <hr>
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Posisi Aktif</td>
                                                    <td>:</td>
                                                    <td><input type="checkbox" onclick="return false" name="posisi_aktif" value="{{$data->posisi_aktif}}" {{ $data->posisi_aktif == 'yes' ? 'checked' : NULL }}></td>
                                                </tr>
                                                <tr>
                                                    <td>Komisi</td>
                                                    <td>:</td>
                                                    <td><input type="checkbox" onclick="return false" name="komisi" value="{{$data->komisi}}" {{ $data->komisi == 'yes' ? 'checked' : NULL }}></td>
                                                </tr>
                                                <tr>
                                                    <td>Kepala Fungsional</td>
                                                    <td>:</td>
                                                    <td><input type="checkbox" onclick="return false" name="kepala_fungsional" value="{{$data->kepala_fungsional}}" {{ $data->kepala_fungsional == 'yes' ? 'checked' : NULL }}></td>
                                                </tr>
                                                <tr>
                                                    <td>Flag Operasional</td>
                                                    <td>:</td>
                                                    <td>
                                                    <input type="checkbox" onclick="return false" name="flag_operasional" value="{{$data->flag_operasional}}" {{ $data->flag_operasional == 'yes' ? 'checked' : NULL }}>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Status Posisi</td>
                                                    <td>:</td>
                                                    <td><br>
                                                        <div class="form-group row">
                                                            <div class="col-sm-5">
                                                                <div class="form-check col">
                                                                    <input class="form-check-input" onclick="return false"  type="radio" name="status_posisi" id="exampleRadios1" value="kosong" {{ $data->status_posisi == 'kosong' ? 'checked' : NULL }}>
                                                                    <label class="form-check-label" for="exampleRadios1">
                                                                        Kosong
                                                                    </label><br>
                                                                    <input class="form-check-input" onclick="return false"  type="radio" name="status_posisi" id="exampleRadios2" value="terisi" {{ $data->status_posisi == 'terisi' ? 'checked' : NULL }}>
                                                                    <label class="form-check-label" for="exampleRadios2">
                                                                        Terisi
                                                                    </label><br>
                                                                    <input class="form-check-input" onclick="return false"  type="radio" name="status_posisi" id="exampleRadios3" value="terisi(pjt)" {{ $data->status_posisi == 'terisi(pjt)' ? 'checked' : NULL }}>
                                                                    <label class="form-check-label" for="exampleRadios3">
                                                                        Terisi (PJT)
                                                                    </label><br>
                                                                </div>     
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Nomor Surat</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nomor_surat" value="{{$data->nomor_surat}}">{{$data->nomor_surat}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Karyawan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="jumlah_karyawan_dengan_posisi_ini" value="{{$data->jumlah_karyawan_dengan_posisi_ini}}">{{$data->jumlah_karyawan_dengan_posisi_ini}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div><br>
                                    <b style="color:black;">Rekaman Informasi</b>
                                    <hr>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Keterangan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                    </div>
                                @endforeach
                            </form><br>
							</div>
							<br>

                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>