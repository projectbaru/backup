<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Posisi</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

</head>

<body id="page-top">
    <div id="wrapper" style="background-color:white;">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black;">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>

            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <!-- <i class="fas fa-fw fa-cog"></i> -->
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item active" href="{{route('list_po')}}" style="background-color:#1053af;color:white;">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top:-30px;background-color:white;">
            <div id="content" style="background-color:white;">
                <div>
                    <!-- <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="navbar-nav ml-auto">
                            <div class="topbar-divider d-none d-sm-block"></div>
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="mr-2 d-none d-lg-inline text-gray-600 small">Douglas McGee</span>
                                    <img class="img-profile rounded-circle" src="img/undraw_profile.svg">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                    aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profile
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Settings
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Activity Log
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Logout
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </nav> -->
                    <div class="container pt-4 mt-3"  style="width:60%; border:1px solid #d9d9d9; border-radius:10px;">                
                    <form action="{{route('update_tampilanpo')}}" class="" method="POST">
                        @csrf
                        <input type="hidden" name="displayedColumn" id="displayedColumnInput" value="kode_posisi,nama_posisi,nama_jabatan">
                        <div class="my-3">
                            <div style="">
                                <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 1. Masukkan Nama</h6>
                                <br>
                                <div class="row mb-3">
                                    <label for="nama_posisi" class="col-sm-2 col-form-label" style="font-size:12px;">Nama</label>
                                    <div class="col-sm-10">
                                        <input type="text" style="font-size:10px;" name="nama_posisi" class="form-control form-control-sm" id="nama_posisi">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="kode_posisi" class="col-sm-2 col-form-label" style="font-size:12px;">Kode</label>
                                    <div class="col-sm-10">
                                        <input type="text" style="font-size:10px;" name="kode_posisi" class="form-control form-control-sm" id="kode_posisi">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 2. Spesifikasi Kriteria Pencarian</h6>
                                
                                <table class="table table-borderless">
                                    <thead style="font-size:12px;">
                                        <th>Halaman</th>
                                        <th>Operator</th>
                                        <th>Nilai</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody id="queryInputContainer">
                                        <tr>
                                            <td>
                                                <select name="queryField[]" id="queryField1"  class="queryField form-control" data-number="1">
                                                    <option class="form-control form-control-sm" value="" selected null>-- kosong --</option>
                                                    @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++)
                                                    <option class="form-control form-control-sm" value="{{ $fields[$i]['value'] }}">{{ $fields[$i]['text'] }}</option>
                                                    @endfor
                                                </select>
                                            </td>
                                            <td>
                                                <select  name="queryOperator[]" id="queryOperator1" class="queryOperator form-control" data-number="1">
                                                    <option class="form-control" value="" selected null>-- kosong --</option>
                                                    @for($i = 0; $i < count($operators); $i++)
                                                        <option class="form-control" value="{{ $operators[$i] }}">{{ $operators[$i] }}</option>
                                                    @endfor
                                                </select>
                                            </td>
                                            <td id="queryValueContainer1">
                                                <input type="text" name="queryValue[]" class="form-control"  id="queryValue1">
                                            </td>
                                            <td>
                                                <button type="button" class="addQueryInput" style="border-radius:5px;border:1px solid white;font-size:20px;">
                                                    <i class="fa-solid fa-plus"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="">
                                <h6 style="background-color:#464646;font-size:13px;color:white;padding:3px;">Langkah 3. Pilih halaman yang ditampilkan</h6>
                                <br>
                                <div class="row" style="">
                                    <div class="col-md-5">
                                        <h5 style="font-size:12px;text-align:center;color:black;">Halaman Tersedia</h5>
                                        <select id="availableColumn" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                            @for($i = 0; $i < array_sum(array_map("count", $fields)) / 2; $i++)
                                                <option value="{{ $fields[$i]['value'] }}">{{ $fields[$i]['text'] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="" style="" class="">
                                        <div style="margin-top: 70px;background-color:#6a6a6a;border-radius:5px;">
                                        <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrow-right fa-sm" style="color:white;"></i>
                                        </button>
                                        <br>
                                        <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrow-left fa-sm" style="color:white;"></i>
                                        </button>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <h5 style="font-size:12px;text-align:center;color:black;">Halaman yang dipilih</h5>        
                                        <select id="displayedColumn" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                            <option value="kode_posisi">Kode Posisi</option>
                                            <option value="nama_posisi">Nama Posisi</option>
                                            <option value="nama_jabatan">Nama Jabatan</option>
                                        </select>
                                    </div>
                                    <div class="" style="">
                                        <div style="margin-top: 50px; background-color:#6a6a6a;border-radius:5px;">
                                        <button  class=" btn btn-sm" type="button" id="movetoFirst" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrows-up-to-line fa-sm" style="color:white;"></i>
                                        </button>
                                        <br>
                                        <button class=" btn btn-sm" type="button" id="moveup" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrow-up fa-1x" style="color:white;"></i>
                                        </button>
                                        <br>
                                        <button class=" btn btn-sm" type="button" id="movedown" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrow-down fa-1x" style="color:white;"></i>
                                        </button>
                                        <br>
                                        <button class=" btn btn-sm" type="button" id="movetoLast" style="border-bottom:1px solid white;">
                                            <i class="fa-solid fa-arrows-down-to-line fa-sm" style="color:white;"></i>
                                        </button>
                                        </div>
                                    </div>
                                    <div class="container-fluid form-group form-group-sm" style="text-align">
                                    <br>
                                    <div style="font-size:11px;">
                                        <input type="submit" value="Submit"  class="btn btn-primary btn-sm" style="border-radius:5px;font-size:11px;">
                                        <a  href="{{route('list_po')}}" value="batal" class="btn btn-danger btn-sm" style="border-radius:5px;font-size:11px;">Batal</a>
                                    </div>
                                </div>
                            
                            </div><br>
                            </div><br>
                        </div>
                    </form>
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <!-- <span>Copyright &copy; Your Website 2021</span> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
@include('js.wsperusahaan.filter')
</html>
