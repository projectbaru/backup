<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Posisi</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header">Menu Master:</h6>
						<a class="collapse-item " href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item active" href="{{route('list_po')}}"  style="background-color:#1053af;color:white;">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content"><br>
			<!-- <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                        <img class="img-profile rounded-circle" src="img/undraw_profile.svg"></a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Settings </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout </a>
                        </div>
                    </li>
                </ul>
			</nav> -->
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">			
					<div class="container" style="width:100%; ">
						<div>
							<br>
							<div class="container-fluid mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
								<div class="container" style="">
                                    <h5><b style="color:black;">Ubah Posisi</b></h5>
                                </div>   
                                    <form action="{{route('update_po')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                        <hr>
                                        <div class="form-group container" style="width:90%;">
                                            <div class="">
                                                @foreach($wsposisi as $data)
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                            <div class="col-sm-10">
                                                            <input type="hidden" name="id_posisi" value="{{ $data->id_posisi }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <!-- <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Perusahaan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="kode_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_perusahaan}}">
                                                            </div>
                                                        </div> -->
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                            <div class="col-sm-10">
                                                                <select name="nama_perusahaan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($datap['looks'] as $p)
                                                                        <option value="{{$p->nama_perusahaan}}" {{ $p->nama_perusahaan == $data->nama_perusahaan ? 'selected' : NULL }}>{{$p->nama_perusahaan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_perusahaan}}"> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Posisi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" required name="kode_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_posisi}}" readonly id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" required name="nama_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_posisi}}" id="colFormLabelSm" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode MPP</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" required name="kode_mpp" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_mpp}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Posisi</label>
                                                            <div class="col-sm-10">
                                                                <select name="tingkat_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($datatp['looks'] as $tp)
                                                                        <option value="{{$tp->urutan_tingkat}}" {{ $tp->urutan_tingkat == $data->tingkat_posisi ? 'selected' : NULL }}>{{$tp->urutan_tingkat}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" name="tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tingkat_posisi}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Detail Tingkat Posisi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="detail_tingkat_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->detail_tingkat_posisi}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Jabatan</label>
                                                            <div class="col-sm-10">
                                                                <select name="nama_jabatan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($dataj['looks'] as $j)
                                                                        <option value="{{$j->nama_jabatan}}" {{ $j->nama_jabatan == $data->nama_jabatan ? 'selected' : NULL }}>{{$j->nama_jabatan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" required name="nama_jabatan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_jabatan}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                                                            <div class="col-sm-10">
                                                                <select name="nama_organisasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($datao['looks'] as $o)
                                                                        <option value="{{$o->nama_organisasi}}" {{ $o->nama_organisasi == $data->nama_organisasi ? 'selected' : NULL }}>{{$o->nama_organisasi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" required name="nama_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_organisasi}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tingkat Organisasi</label>
                                                            <div class="col-sm-10">
                                                                <select name="tingkat_organisasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($datao['looks'] as $o)
                                                                        <option value="{{$o->tingkat_organisasi}}" {{ $o->tingkat_organisasi == $data->tingkat_organisasi ? 'selected' : NULL }}>{{$o->tingkat_organisasi}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" required name="tingkat_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tingkat_organisasi}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Posisi</label>
                                                            <div class="col-sm-10">
                                                                <select name="tipe_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value="none" {{ $data->tipe_posisi == 'none' ? 'selected' : NULL }}>None</option>
                                                                    <option value="tunggal" {{ $data->tipe_posisi == 'tunggal' ? 'selected' : NULL }}>Tunggal</option>
                                                                    <option value="banyak" {{ $data->tipe_posisi == 'banyak' ? 'selected' : NULL }}>Banyak</option>

                                                                </select>
                                                            <!-- <input type="text" name="tipe_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tipe_posisi}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Deskripsi Pekerjaan Posisi</label>
                                                            <div class="col-sm-10">
                                                            <textarea type="text" name="deskripsi_pekerjaan_posisi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->deskripsi_pekerjaan_posisi}}" id="colFormLabelSm" placeholder="">{{$data->deskripsi_pekerjaan_posisi}}</textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi Atasan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="nama_posisi_atasan" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_posisi_atasan}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <hr>
                                                <b style="color:black;">Informasi Lainnya</b>
                                                <br>
                                                <br>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Dari Golongan</label>
                                                            <div class="col-sm-10">
                                                                <select name="dari_golongan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($datadg['looks'] as $v)
                                                                        <option value="{{$v->nama_golongan}}" {{ $v->nama_golongan == $data->dari_golongan ? 'selected' : NULL }}>{{$v->nama_golongan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" name="dari_golongan" style="font-size:11px;" value="{{$data->dari_golongan}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Sampai Golongan</label>
                                                            <div class="col-sm-10">
                                                                <select name="sampai_golongan" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                                    <option value=""></option>
                                                                    @foreach ($datadg['looks'] as $p)
                                                                        <option value="{{$p->nama_golongan}}" {{ $p->nama_golongan == $data->sampai_golongan ? 'selected' : NULL }}>{{$p->nama_golongan}}</option>
                                                                    @endforeach
                                                                </select>
                                                            <!-- <input type="text" name="sampai_golongan" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->sampai_golongan}}" id="colFormLabelSm" placeholder=""> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Area</label>
                                                            <div class="col-sm-10">
                                                                
                                                                <select name="tipe_area" id="tipe_area" class="form-control form-control-sm" style="font-size:11px;">
                                                                    <option value="BO" {{ $data->tipe_area == 'BO' ? 'selected' : NULL }}>BO</option>
                                                                    <option value="HO" {{ $data->tipe_area == 'HO' ? 'selected' : NULL }}>HO</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <hr> 
                                                <b style="color:black;">Flag Posisi</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Posisi Aktif</label>
                                                            <div class="col-sm-5">
                                                              
                                                            <input type="checkbox" name="posisi_aktif" style="font-size:11px;" class="" id="colFormLabelSm" value="yes" placeholder="" {{ $data->posisi_aktif == 'yes' ? 'checked' : NULL }}>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Komisi</label>
                                                            <div class="col-sm-5">
                                                            <input type="checkbox" name="komisi" class="" id="colFormLabelSm" style="font-size:11px;" placeholder="" value="yes" placeholder="" {{ $data->komisi == 'yes' ? 'checked' : NULL }}>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kepala Fungsional</label>
                                                            <div class="col-sm-5">
                                                            <input type="checkbox" name="kepala_fungsional" style="font-size:11px;" class="" id="colFormLabelSm" placeholder="" value="yes" placeholder="" {{ $data->kepala_fungsional == 'yes' ? 'checked' : NULL }}> 
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Flag Operasional</label>
                                                            <div class="col-sm-5">
                                                            <input type="checkbox" name="flag_operasional" style="font-size:11px;" class="" id="colFormLabelSm" placeholder="" value="yes" placeholder="" {{ $data->flag_operasional == 'yes' ? 'checked' : NULL }}>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status Posisi</label>
                                                            <div class="col-sm-5">
                                                                <div class="form-check col">
                                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios1" value="kosong" {{ $data->status_posisi == 'kosong' ? 'checked' : NULL }}>
                                                                    <label class="form-check-label" for="exampleRadios1">
                                                                        Kosong
                                                                    </label><br>
                                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios2" value="terisi" {{ $data->status_posisi == 'terisi' ? 'checked' : NULL }}>
                                                                    <label class="form-check-label" for="exampleRadios2">
                                                                        Terisi
                                                                    </label><br>
                                                                    <input class="form-check-input" type="radio" name="status_posisi" id="exampleRadios3" value="terisi(pjt)" {{ $data->status_posisi == 'terisi(pjt)' ? 'checked' : NULL }}>
                                                                    <label class="form-check-label" for="exampleRadios3">
                                                                        Terisi (PJT)
                                                                    </label><br>
                                                                </div>     
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Surat</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="nomor_surat" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nomor_surat}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Jumlah Karyawan</label>
                                                            <div class="col-sm-10">
                                                            <input type="number" name="jumlah_karyawan_dengan_posisi_ini" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->jumlah_karyawan_dengan_posisi_ini}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <b style="color:black;">Rekaman Informasi</b>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-5">
                                                                    <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm"  value="{{$data->tanggal_mulai_efektif}}" id="colFormLabelSm" placeholder="">
                                                                </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                    <div class="col-sm-5">
                                                                    <input type="date" name="tanggal_selesai_efektif" class="form-control form-control-sm"  id="colFormLabelSm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                    <div class="col-sm-5">
                                                                    <textarea rows="4" cols="50" type="text" name="keterangan" class="form-control form-control-sm" value="{{$data->keterangan}}"   id="colFormLabelSm" style="font-size:11px;" placeholder="">{{$data->keterangan}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                @endforeach
                                                <div class="container" style="width: 100%;">
                                                    <table>
                                                        <tr>
                
                                                        <!-- <td>
                                                            <a href="" class="btn">Hapus</a>
                                                        </td> -->
                                                            <td>
                                                                <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                            </td>
                                                            <td>
                                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_po',$data->id_posisi)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                            </td>
                                                            <td>
                                                                <a href="{{ route('list_po') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <br>
                        </div>
                    </div>
                    <footer class="sticky-footer bg-white">
                        <div class="container my-auto">
                            <div class="copyright text-center my-auto">
                                <!-- <span>Copyright &copy; Your Website 2021</span> -->
                            </div>
                        </div>
                    </footer>
                </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
