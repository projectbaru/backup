<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Tingkat Golongan</title>
    <link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <!-- <i class="fas fa-laugh-wink"></i> -->
                </div>
                <div class="sidebar-brand-text mx-3"></div>
            </a>
            <hr class="sidebar-divider my-0">
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
                    <span>Dashboard</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Interface
            </div>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <span>Master WS</span>
                </a>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
                        <h6 class="collapse-header">Menu Master:</h6>
                        <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item active" href="{{route('list_tg')}}" style="background-color:#1053af;color:white;">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>
        </ul>
        <div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
            <div id="content"><br>
                
                <div class="container-fluid" style="background-color:white;">
                    <div class="relative flex items-top justify-center min-h-screen sm:items-center" style="width:100%;background-color:white; ">
                        <div class="" style="width:100%;background-color:white; "><br>
                            <div class="container" style="width:100%;border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;">
                                    <div class="container" style="">
                                        <h5><b style="color:black;">Edit Tingkat Golongan</b></h5>
                                    </div>
                                    <div class="container-fluid mt-2">
                                        <form action="{{ route('update_tg') }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="items" id="items" value ="selectedItems">
                                            <hr>
                                            <div class="form-group container" style="width:90%;">
                                                <div class="">
                                                    <b style="color:black;">Informasi Tingkat Golongan</b><br><br>
                                                    <div class="row" style="color:black;">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;"></label>
                                                                <div class="col-sm-10">
                                                                    <input type="hidden" name="id_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->id_tingkat_golongan }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->nama_perusahaan }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Golongan</label>
                                                                <div class="col-sm-10">
                                                                <input type="text" readonly required name="kode_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->kode_tingkat_golongan }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Tingkat Golongan</label>
                                                                <div class="col-sm-10">
                                                                <input type="number" required name="nama_tingkat_golongan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->nama_tingkat_golongan }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Tampilan</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" required name="urutan_tampilan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->urutan_tampilan }}">
                                                                <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                                                                <div class="col-sm-10">
                                                                    <input required type="date" name="tanggal_mulai" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->tanggal_mulai }}">
                                                                </div>
                                                            </div>


                                                        </div> -->
                                                    </div>
                                                    <hr>
                                                    <b style="color:black;">List Tingkat Golongan</b>
                                                    <br>
                                                    <br>
                                                    <div class="row" style="">
                                                        <div class="col-md-5">
                                                            <select id="defaultList" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                                                @foreach($items as $item)
                                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="" style="" class="">
                                                            <div style="margin-top: 70px;background-color:#6a6a6a;border-radius:5px;">
                                                            <button class="btn btn-sm" type="button" id="addtoDisplay" style="border-bottom:1px solid white;">
                                                                <i class="fa-solid fa-arrow-right fa-sm" style="color:white;"></i>
                                                            </button>
                                                            <br>
                                                            <button class="btn btn-sm" type="button" id="returntoAvailable" style="border-bottom:1px solid white;">
                                                                <i class="fa-solid fa-arrow-left fa-sm" style="color:white;"></i>
                                                            </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            @if(count($selectedOptionItems) > 0)
                                                                <select id="chosenItems" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;">
                                                                    @for($i = 0; $i < count($selectedOptionItems); $i++)
                                                                        @foreach($items as $item)
                                                                            @if($item->id == $selectedOptionItems[$i])
                                                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    @endfor
                                                                </select>
                                                            @else
                                                                <select id="chosenItems" multiple="true" size="10" style="width:100%;font-size:11px;border-radius:10px;padding:10px;"></select>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <b style="color:black;">Rekaman Informasi</b>
                                                    <br>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_mulai_efektif" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wstingkatgolongan->tanggal_mulai_efektif }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                                <div class="col-sm-10">
                                                                <input type="date" name="tanggal_selesai_efektif" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" value="{{ $wstingkatgolongan->tanggal_selesai_efektif }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                                <div class="col-sm-10">
                                                                <textarea type="text" rows="4" cols="50" name="keterangan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" value="{{ $wstingkatgolongan->keterangan }}">{{$wstingkatgolongan->keterangan}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><hr>
                                                </div>
                                            </div>
                                            <div class="container" style="width: 90%;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_tg') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </form>
                                    </div>
                                    <br>
                            </div>
                        </div>
                        <footer class="sticky-footer bg-white">
                            <div class="container my-auto">
                                <div class="copyright text-center my-auto">
                                    <!-- <span>Copyright &copy; Your Website 2021</span> -->
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div> -->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    @include('js.wstingkatgolongan.edit')
</body>
</html>
