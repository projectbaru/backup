<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title></title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
					<a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
					<a class="collapse-item active" href="{{route('list_a')}}" style="background-color:#1053af;color:white;">Alamat</a>
					<a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
					<a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
					<a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
					<a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
					<a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
					<a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
					<a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
					<a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
					<a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                    <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                    <a class="collapse-item" href="{{ route('list_kac') }}">Kantor Cabang</a>
					<a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
					<a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
					<a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
					<a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
					<a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
			<!-- <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
                </button>
                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small"></span>
                        <img class="img-profile rounded-circle" src="img/undraw_profile.svg"></a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Settings </a>
                            <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout </a>
                        </div>
                    </li>
                </ul>
			</nav> -->
            <br>
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style=""><br>			
					<div class="container" style="width:100%; border:1px solid #d9d9d9;padding-top:20px;border-radius:10px;">
                        <div class="container-fluid mt-2">
                            <div class="" style="">
                                <b style="color:black;">Edit Alamat</b></h5>
                            </div>  
                            <form action="{{route('update_a')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                @foreach($wsalamat as $data)
                                <hr>
                                <div class="form-group container" style="width:90%;">
                                    <div class="">
                                        <div class="row">
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                    <div class="col-sm-10">
                                                    <input type="hidden" name="id_alamat" value="{{ $data->id_alamat }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Alamat</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" name="kode_alamat" required style="font-size:11px;" value="{{ $data->kode_alamat }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Perusahaan</label>
                                                    <div class="col-sm-10">
                                                        <select name="kode_perusahaan" class="form-control form-control-sm vendorId" >
                                                            <option value=""></option>
                                                            @foreach ($datas['looks'] as $v)
                                                                <option value="{{$v->kode_perusahaan}}" {{ $v->kode_perusahaan == $data->kode_perusahaan ? 'selected' : NULL }}>{{$v->kode_perusahaan}}</option>
                                                            @endforeach
                                                        </select>
                                                    <!-- <input type="text" name="kode_perusahaan" required value="{{ $data->kode_perusahaan }}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" name="nama_perusahaan" required value="{{ $data->nama_perusahaan }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Status Alamat</label>
                                                    <div class="col-sm-10">
                                                        <select name="status_alamat" id="status_alamat" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="ya">Ya</option>
                                                            <option value="tidak">Tidak</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Jenis Alamat</label>
                                                    <div class="col-sm-10">
                                                        <select name="jenis_alamat" id="jenis_alamat" class="form-control form-control-sm" style="font-size:11px;">
                                                            <option value="Alamat Kantor Pusat">Alamat Kantor Pusat</option>
                                                            <option value="Alamat Cabang">Alamat Cabang</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Alamat</label>
                                                    <div class="col-sm-10">
                                                        <textarea type="text" name="alamat" rows="8" cols="50" required value="" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->alamat }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Provinsi</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" name="kota" style="font-size:11px;" value="{{ $data->kota }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kode Pos</label>
                                                    <div class="col-sm-10">
                                                    <input type="number" style="font-size:11px;" required name="kode_pos" value="{{ $data->kode_pos }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Telepon</label>
                                                    <div class="col-sm-10">
                                                    <input type="number" style="font-size:11px;" name="telepon" value="{{ $data->telepon }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                
                                            </div>
                                        </div>  
                                        <hr>
                                        <b style="color:black;">Rekaman Informasi</b>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" name="tanggal_mulai_efektif" required value="{{ $data->tanggal_mulai_efektif }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tanggal Selesai Efektif</label>
                                                    <div class="col-sm-10">
                                                    <input type="date" style="font-size:11px;" name="tanggal_selesai_efektif" value="{{ $data->tanggal_selesai_efektif }}"  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Keterangan</label>
                                                    <div class="col-sm-10">
                                                    <textarea type="text" style="font-size:11px;" rows="8" cols="50" name="keterangan" value=""  class="form-control form-control-sm" id="colFormLabelSm" placeholder="">{{ $data->keterangan }}</textarea>
                                                    </div>
                                                </div>
                                                <hr>      
                                            </div>
                                        </div>
                                        <div class="container" style="width: 100%;">
                                            <table>
                                                <tr>
                                                <!-- <td>
                                                    <a href="" class="btn">Hapus</a>
                                                </td> -->
                                                    <td>
                                                        <button type="submit" class="btn btn-primary btn-sm" style="border:none;border-radius:5px;font-size:11px;">Simpan</button>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-danger btn-sm" href="{{route('hapus_a',$data->id_alamat)}}" class="btn" style="border:none;border-radius:5px;font-size:11px;">Hapus</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('list_a') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div> @endforeach
                            </form>
                            <h5><b>
                            </div>
                        <br>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>
           
                        