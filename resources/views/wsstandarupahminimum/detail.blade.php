<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Standar Upah Minimum</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
					<a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                    <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                    <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                    <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                    <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                    <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                    <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                    <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                    <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                    <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                    <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                    <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                    <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                    <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                    <a class="collapse-item active" href="{{route('list_usm')}}" style="background-color:#1053af;color:white;">Upah Standar Minimum</a>
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
		
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style=""><br>
					<div class="container" style="width:90%; ">
						<div>
							<div class="container mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
                            <h5 style="color:black;">Detail Standar Upah Minimum</h5>	
                            <hr>	
                            <form action="{{route('simpan_usm')}}" method="post">{{ csrf_field() }}
                                @foreach($wsusm as $data)
                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_usm', $data->id_standar_upah_minimum)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" >Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_usm', $data->id_standar_upah_minimum)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_usm')}}">Batal</a></td>
                                        <!-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
                                        <!-- <td>
                                            <button class="btn btn-danger btn-sm" style="border-radius:5px; font-size:11px;" href="">Hapus</button>
                                        </td> -->
                                    </tr>
                                </table><br>
                                    <div class="row">
                                        <div class="col" >
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_posisi" value="{{$data->kode_posisi}}">{{$data->kode_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Posisi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_posisi" value="{{$data->nama_posisi}}">{{$data->nama_posisi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Pendidikan</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tingkat_pendidikan" value="{{$data->tingkat_pendidikan}}">{{$data->tingkat_pendidikan}}</td>
                                                </tr>
                                            </table>
                                        </div>
										
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Kode Lokasi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="kode_lokasi" value="{{$data->kode_lokasi}}">{{$data->kode_lokasi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Lokasi</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="nama_lokasi" value="{{$data->nama_lokasi}}">{{$data->nama_lokasi}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Upah Minimum</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="upah_minimum" value="{{$data->upah_minimum}}">{{$data->upah_minimum}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <hr>
                                    <b style="color:black;">Informasi Lainnya</b>
                                    <div class="row">
                                        <div class="col">
                                            <table style="font-size:12px;">
                                                <tr>
                                                    <td>Tanggal Mulai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tanggal Selesai Efektif</td>
                                                    <td>:</td>
                                                    <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>
                                @endforeach
                            </form>
							</div>
							<br>

                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>