<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Standar Upah Minimum</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
                    <a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                    <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                    <a class="collapse-item" href="{{route('list_o')}}">Organisasi</a>
                    <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                    <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                    <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                    <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                    <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                    <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                    <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                    <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                    <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                    <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                    <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                    <a class="collapse-item active" href="{{route('list_usm')}}" style="background-color:#1053af;color:white;">Upah Standar Minimum</a> 
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">			
					<div class="container" style="width:100%; ">
						<div>
							<br>
							<div class="container-fluid mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
								<div class="" style="">
                                    <h5><b style="color:black;">Ubah Standar Upah Minimum</b></h5>
                                </div>   
                            <form action="{{route('update_usm')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                @foreach($wsusm as $data)
                                <hr>
                                <div class="form-group container" style="">
                                    <div class="">
                                        <div class="row" >
                                            <div class="col" style="font-size: 10px;">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                    <div class="col-sm-10">
                                                    <input type="hidden" name="id_standar_upah_minimum" value="{{ $data->id_standar_upah_minimum }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Posisi</label>
                                                    <div class="col-sm-10">
                                                    <input type="text" name="kode_posisi" value="{{ $data->kode_posisi }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Posisi</label>
                                                    <div class="col-sm-10">
                                                    <!-- <input type="text" name="nama_posisi" value="{{ $data->nama_posisi }}" style="font-size:11px;"  class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                    <select name="nama_posisi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                        <option value=""></option>
                                                        <option value="-">-</option>
                                                        @foreach ($posisi['looks'] as $v)
                                                            <option value="{{$v->nama_posisi}}" {{ $v->nama_posisi == $data->nama_posisi ? 'selected' : NULL }}>{{$v->nama_posisi}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Pendidikan</label>
                                                        <div class="col-sm-10">
                                                            <select name="tingkat_pendidikan" id="tipe_posisi" class="form-control form-control-sm" style="font-size:11px;">
                                                                <!-- <option value="-">None</option> -->
                                                                <option value="SD" {{ $data->tingkat_pendidikan == 'SD' ? 'selected' : NULL }}>SD</option>
                                                                <option value="SLTP" {{ $data->tingkat_pendidikan == 'SLTP' ? 'selected' : NULL }}>SLTP</option>
                                                                <option value="SLTA" {{ $data->tingkat_pendidikan == 'SLTA' ? 'selected' : NULL }}>SLTA</option>
                                                                <option value="D1" {{ $data->tingkat_pendidikan == 'D1' ? 'selected' : NULL }}>D1</option>
                                                                <option value="D2" {{ $data->tingkat_pendidikan == 'D2' ? 'selected' : NULL }}>D2</option>
                                                                <option value="D3" {{ $data->tingkat_pendidikan == 'D3' ? 'selected' : NULL }}>D3</option>
                                                                <option value="S1" {{ $data->tingkat_pendidikan == 'S1' ? 'selected' : NULL }}>S1</option>
                                                                <option value="S2" {{ $data->tingkat_pendidikan == 'S2' ? 'selected' : NULL }}>S2</option>
                                                                <option value="S3" {{ $data->tingkat_pendidikan == 'S3' ? 'selected' : NULL }}>S3</option>
                                                            </select>
                                                        <!-- <input type="text" name="tipe_organisasi" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder=""></textarea> -->
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Lokasi</label>
                                                    <div class="col-sm-10">
                                                        <select name="kode_lokasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                            @foreach ($lokasi['looks'] as $v)
                                                                <option value="{{$v->kode_grup_lokasi_kerja}}" {{ $v->kode_grup_lokasi_kerja == $data->kode_lokasi ? 'selected' : NULL }}>{{$v->kode_grup_lokasi_kerja}}</option>
                                                            @endforeach
                                                        </select>
                                                    <!-- <input type="kode_lokasi" name="kode_lokasi" value="{{ $data->kode_lokasi }}"  style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Lokasi</label>
                                                    <div class="col-sm-10">
                                                    <!-- <input type="text" name="nama_lokasi" value="{{ $data->nama_lokasi }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder=""> -->
                                                        <select name="nama_lokasi" class="form-control form-control-sm vendorId" style="font-size:11px;">
                                                            <option value="" selected disabled>--- Pilih ---</option>
                                                            <option value="-">-</option>
                                                            @foreach ($lokasi['looks'] as $v)
                                                                <option value="{{$v->nama_grup_lokasi_kerja}}" {{ $v->nama_grup_lokasi_kerja == $data->nama_lokasi ? 'selected' : NULL }}>{{$v->nama_grup_lokasi_kerja}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Upah Minimum</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="upah_minimum" value="{{ $data->upah_minimum }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <b style="color:black;">Informasi Lainnya</b>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                    <div class="col-sm-10">
                                                        <input type="date" name="tanggal_mulai_efektif" value="{{ $data->tanggal_mulai_efektif }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                    <div class="col-sm-10">
                                                        <input type="date" name="tanggal_selesai_efektif" value="{{ $data->tanggal_selesai_efektif }}" style="font-size:11px;" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                    </div>
                                                </div>                
                                            </div>
                                            <hr>
                                        </div>
                                    </div><hr>
                                </div> @endforeach
                                <div class="container" style="width: 100%;">
                                    <table>
                                        <tr>
                                        <!-- <td>
                                            <a href="" class="btn">Hapus</a>
                                        </td> -->
                                            <td>
                                                <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger btn-sm" href="{{route('hapus_usm',$data->id_standar_upah_minimum)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                            </td>
                                            <td>
                                                <a href="{{ route('list_usm') }}" class="btn btn-danger btn-sm" style="border:none;border-radius:5px;font-size:11px;">Batal</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </form><br>
                        </div>
                        <br>
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <!-- <span>Copyright &copy; Your Website 2021</span> -->
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
