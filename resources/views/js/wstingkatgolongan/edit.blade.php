<script>
     $('#addtoDisplay').click(function (e) {
        e.preventDefault();

        var text    = $('#defaultList :selected').text();
        var value   = $('#defaultList :selected').val();

        var filter  = $('#chosenItems').find(`[value=${value}]`).length;

        if(filter == 0) {
            $('#chosenItems').append(`
                <option value="${value}">${text}</option>
            `);
        }

        var selected = $('#chosenItems option').toArray().map(item => item.value);

        $('#items').val(selected);
    });

    $('#returntoAvailable').click(function (e) {
        e.preventDefault();

        var value = $('#chosenItems :selected').val();

        if($('#chosenItems').find('option').length == 0) {
            $('#chosenItems').find('option').remove().end();

            $('#items').val('');
        } else {
            $('#chosenItems').find(`[value=${value}]`).remove();

            var selected = $('#chosenItems option').toArray().map(item => item.value);

            $('#items').val(selected);
        }
    });
</script>
