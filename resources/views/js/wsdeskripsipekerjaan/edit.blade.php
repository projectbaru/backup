<script>
    $(function () {
        // SECTION tanggung jawab
        var wrapper     = $('#responsibilityInputContainer');

        @if(count($wsdeskripsipekerjaan_tanggung_jawab) > 0)
            var x = '{{ count($wsdeskripsipekerjaan_tanggung_jawab) }}';
        @else
            var x = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($wsdeskripsipekerjaan_tanggung_jawab as $row)
            @if($i == 0)
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">T
                            anggung Jawab {{ $i + 1 }}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" style="font-size:11px;" name="tanggung_jawab[]" class="form-control form-control-sm" value="{{ $row->tanggung_jawab }}" required>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-primary btn-sm" id="addResponsibilityInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper).append(`
                    <div id="responsibilityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">T
                            anggung Jawab {{ $i + 1 }}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" style="font-size:11px;" name="tanggung_jawab[]" class="form-control form-control-sm" required value="{{ $row->tanggung_jawab }}">
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-danger btn-sm removeResponsibilityInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addResponsibilityInput').click(function (e) {
            e.preventDefault();

            if(x < 1) {
                x = 1;
            }

            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">T
                        anggung Jawab ${x}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="tanggung_jawab[]" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;"  class="btn btn-sm btn-danger removeResponsibilityInput" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeResponsibilityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });

        $('.removeResponsibilityInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#responsibilityClone${number}`).remove();

            x--;
        });
        // !SECTION tanggung jawab
        // SECTION wewenang
        var wrapper_wewenang     = $('#authorityInputContainer');

        @if(count($wsdeskripsipekerjaan_wewenang) > 0)
            var y = '{{ count($wsdeskripsipekerjaan_wewenang) }}';
        @else
            var y = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($wsdeskripsipekerjaan_wewenang as $row)
            @if($i == 0)
                $(wrapper_wewenang).append(`
                    <div id="authorityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Wewenang {{ $i + 1 }}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" style="font-size:11px;" name="wewenang[]" class="form-control form-control-sm" value="{{ $row->wewenang }}" required>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;" class="btn btn-sm btn-primary" id="addAuthorityInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper_wewenang).append(`
                    <div id="authorityClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Wewenang {{ $i + 1 }}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" style="font-size:11px;" name="wewenang[]" class="form-control form-control-sm" value="{{ $row->wewenang }}" required>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-sm btn-danger removeAuthorityInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addAuthorityInput').click(function (e) {
            e.preventDefault();

            if(y < 1) {
                y = 1
            }

            y++;

            $(wrapper_wewenang).append(`
                <div id="authorityClone${y}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Wewenang ${y}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="wewenang[]" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" style="font-size:11px;"  class="btn btn-sm btn-danger removeAuthorityInput" data-number="${y}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeAuthorityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#authorityClone${number}`).remove();

                y--;
            });
        });

        $('.removeAuthorityInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#authorityClone${number}`).remove();

            y--;
        });
        // !SECTION wewenang
        // SECTION jabatan
        var wrapper_jabatan     = $('#qualificationInputContainer');

        @if(count($wsdeskripsipekerjaan_kualifikasi_jabatan) > 0)
            var z = '{{ count($wsdeskripsipekerjaan_kualifikasi_jabatan) }}';
        @else
            var z = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($wsdeskripsipekerjaan_kualifikasi_jabatan as $row)
            @if($i == 0)
                $(wrapper_jabatan).append(`
                    <div id="qualificationClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi Jabatan {{ $i + 1 }}</label>
                        <div class="col-sm-3">
                            <input type="text" style="font-size:11px;" name="kualifikasi_jabatan[]" class="form-control form-control-sm" value="{{ $row->kualifikasi_jabatan }}" required>
                        </div>
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tipe Kualifikasi {{ $i + 1 }}</label>
                        <div class="col-sm-4">
                            <select name="tipe_kualifikasi[]"  style="font-size:11px;"  class="form-control form-control-sm" required>
                                <option value="a" {{ $row->tipe_kualifikasi == 'a' ? 'selected' : null }}>a</option>
                                <option value="b" {{ $row->tipe_kualifikasi == 'b' ? 'selected' : null }}>b</option>
                                <option value="c" {{ $row->tipe_kualifikasi == 'c' ? 'selected' : null }}>c</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <button type="button"  style="font-size:11px;"  class="btn btn-sm btn-primary button" id="addQualificationInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper_jabatan).append(`
                    <div id="qualificationClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi Jabatan {{ $i + 1 }}</label>
                        <div class="col-sm-3">
                            <input type="text" style="font-size:11px;" name="kualifikasi_jabatan[]" class="form-control form-control-sm" value="{{ $row->kualifikasi_jabatan }}" required>
                        </div>
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tipe Kualifikasi {{ $i + 1 }}</label>
                        <div class="col-sm-4">
                            <select name="tipe_kualifikasi[]" style="font-size:11px;"  class="form-control form-control-sm" required>
                                <option value="a" {{ $row->tipe_kualifikasi == 'a' ? 'selected' : null }}>a</option>
                                <option value="b" {{ $row->tipe_kualifikasi == 'b' ? 'selected' : null }}>b</option>
                                <option value="c" {{ $row->tipe_kualifikasi == 'c' ? 'selected' : null }}>c</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" style="font-size:11px;"  class="btn btn-sm btn-danger removeQualificationInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addQualificationInput').click(function (e) {
            e.preventDefault();

            if(z < 1) {
                z = 1
            }

            z++;

            $(wrapper_jabatan).append(`
                <div id="qualificationClone${z}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi Jabatan ${z}</label>
                    <div class="col-sm-3">
                        <input type="text" style="font-size:11px;" name="kualifikasi_jabatan[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tipe Kualifikasi ${z}</label>
                    <div class="col-sm-4">
                        <select name="tipe_kualifikasi[]" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="a">a</option>
                            <option value="b">b</option>
                            <option value="c">c</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-primary removeQualificationInput" data-number="${z}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeQualificationInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#qualificationClone${number}`).remove();

                z--;
            });
        });

        $('.removeQualificationInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#qualificationClone${number}`).remove();

            z--;
        });
        // !SECTION jabatan
        // SECTION deskripsi pekerjaan
        var wrapper_deskripsi     = $('#descriptionInputContainer');

        @if(count($wsdeskripsipekerjaan_deskripsi_pekerjaan) > 0)
            var aa = '{{ count($wsdeskripsipekerjaan_deskripsi_pekerjaan) }}';
        @else
            var aa = 1;
        @endif

        @php
            $i = 0;
        @endphp
        @foreach($wsdeskripsipekerjaan_deskripsi_pekerjaan as $row)
            @if($i == 0)
                $(wrapper_deskripsi).append(`
                    <div id="descriptionClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan {{ $i + 1 }}</label>
                        <div class="col-sm-3">
                            <input name="deskripsi_pekerjaan[]" class="form-control form-control-sm" style="font-size:11px;" required value="{{ $row->deskripsi_pekerjaan }}">
                        </div>
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                        <div class="col-sm-2">
                            <select name="pdca[]" style="font-size:11px;" class="form-control form-control-sm" required>
                                <option value="" selected disabled>--- Belum dipilih ---</option>
                                <option value="a" {{ $row->pdca == 'a' ? 'selected' : null }}>a</option>
                                <option value="b" {{ $row->pdca == 'b' ? 'selected' : null }}>b</option>
                                <option value="c" {{ $row->pdca == 'c' ? 'selected' : null }}>c</option>
                            </select>
                        </div>
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                        <div class="col-sm-2">
                            <select name="bsc[]" style="font-size:11px;" class="form-control form-control-sm" required>
                                <option value="" selected disabled>--- Belum dipilih ---</option>
                                <option value="a" {{ $row->bsc == 'a' ? 'selected' : null }}>a</option>
                                <option value="b" {{ $row->bsc == 'b' ? 'selected' : null }}>b</option>
                                <option value="c" {{ $row->bsc == 'c' ? 'selected' : null }}>c</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-sm btn-primary" id="addDescriptionInput">
                                <i class="fa-solid fa-plus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @else
                $(wrapper_deskripsi).append(`
                    <div id="descriptionClone{{ $i + 1 }}" class="mt-3 col-sm-12 row">
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan {{ $i + 1 }}</label>
                        <div class="col-sm-3">
                            <input name="deskripsi_pekerjaan[]" style="font-size:11px;" class="form-control form-control-sm" required value="{{ $row->deskripsi_pekerjaan }}">
                        </div>
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                        <div class="col-sm-2">
                            <select name="pdca[]" style="font-size:11px;" class="form-control form-control-sm" required>
                                <option value="" selected disabled>--- Belum dipilih ---</option>
                                <option value="a" {{ $row->pdca == 'a' ? 'selected' : null }}>a</option>
                                <option value="b" {{ $row->pdca == 'b' ? 'selected' : null }}>b</option>
                                <option value="c" {{ $row->pdca == 'c' ? 'selected' : null }}>c</option>
                            </select>
                        </div>
                        <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                        <div class="col-sm-2">
                            <select name="bsc[]" style="font-size:11px;" class="form-control form-control-sm" required>
                                <option value="" selected disabled>--- Belum dipilih ---</option>
                                <option value="a" {{ $row->bsc == 'a' ? 'selected' : null }}>a</option>
                                <option value="b" {{ $row->bsc == 'b' ? 'selected' : null }}>b</option>
                                <option value="c" {{ $row->bsc == 'c' ? 'selected' : null }}>c</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn btn-sm btn-danger removeDescriptionInput" data-number="{{ $i + 1 }}">
                                <i class="fa-solid fa-minus"></i>
                            </button>
                        </div>
                    </div>
                `);
            @endif
            {{ $i++ }}
        @endforeach

        $('#addDescriptionInput').click(function (e) {
            e.preventDefault();

            if(aa < 1) {
                aa = 1
            }

            aa++;

            $(wrapper_deskripsi).append(`
                <div id="descriptionClone${aa}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan ${aa}</label>
                    <div class="col-sm-3">
                        <input type="text" name="deskripsi_pekerjaan[]" style="font-size:11px;" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                    <div class="col-sm-2">
                        <select name="pdca[]" style="font-size:11px;" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="a">a</option>
                        </select>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                    <div class="col-sm-2">
                        <select name="bsc[]" style="font-size:11px;" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="a">a</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-danger removeDescriptionInput" data-number="${aa}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeDescriptionInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#descriptionClone${number}`).remove();
                aa--;
            });
        });

        $('.removeDescriptionInput').click(function (e) {
            e.preventDefault();

            var number = $(this).data('number');

            $(`#descriptionClone${number}`).remove();

            aa--;
        });
        // !SECTION deskripsi pekerjaan
    });
</script>
