<script>
    // SECTION tambah input kueri tanggung jawab
    $(function () {
        var wrapper     = $('#responsibilityInputContainer');
        var x           = 1;

        $('#addResponsibilityInput').click(function (e) {
            e.preventDefault();

            if(x < 1) {
                x++;
            }

            x++;

            $(wrapper).append(`
                <div id="responsibilityClone${x}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">T
                        anggung Jawab ${x}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="tanggung_jawab[]" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-danger removeResponsibilityInput" data-number="${x}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeResponsibilityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#responsibilityClone${number}`).remove();

                x--;
            });
        });
    });
    // !SECTION tambah input kueri tanggung jawab
    // SECTION tambah input kueri wewenang
        $(function () {
        var wrapper     = $('#authorityInputContainer');
        var y           = 1;

        $('#addAuthorityInput').click(function (e) {
            e.preventDefault();

            if(y < 1) {
                y++;
            }

            y++;

            $(wrapper).append(`
                <div id="authorityClone${y}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Wewenang ${y}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" style="font-size:11px;" name="wewenang[]" class="form-control form-control-sm" required>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-danger removeAuthorityInput" data-number="${y}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeAuthorityInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#authorityClone${number}`).remove();

                y--;
            });
        });
    });
    // !SECTION tambah input kueri wewenang
    // SECTION tambah input kueri kualifikasi
    $(function () {
        var wrapper     = $('#qualificationInputContainer');
        var z           = 1;

        $('#addQualificationInput').click(function (e) {
            e.preventDefault();

            if(z < 1) {
                z++;
            }

            z++;

            $(wrapper).append(`
                <div id="qualificationClone${z}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Kualifikasi Jabatan ${z}</label>
                    <div class="col-sm-3">
                        <input type="text" style="font-size:11px;" name="kualifikasi_jabatan[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Tipe Kualifikasi ${z}</label>
                    <div class="col-sm-4">
                            <select name="tipe_kualifikasi[]" class="form-control form-control-sm" required>
                                    <option value="" selected>--- Belum dipilih ---</option>
                                    <option value="a">a</option>
                                    <option value="b">b</option>
                                    <option value="c">c</option>
                            </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-danger removeQualificationInput" data-number="${z}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeQualificationInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#qualificationClone${number}`).remove();

                z--;
            });
        });
    });
    // !SECTION tambah input kueri kualifikasi
    // SECTION tambah input kueri deskripsi
    $(function () {
        var wrapper     = $('#descriptionInputContainer');
        var aa           = 1;

        $('#addDescriptionInput').click(function (e) {
            e.preventDefault();

            if(aa < 1) {
                aa++;
            }

            aa++;

            $(wrapper).append(`
                <div id="descriptionClone${aa}" class="mt-3 col-sm-12 row">
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-2 col-form-label col-form-label-sm">Deskripsi Pekerjaan ${aa}</label>
                    <div class="col-sm-3">
                        <input name="deskripsi_pekerjaan[]" class="form-control form-control-sm" required>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">PDCA</label>
                    <div class="col-sm-2">
                        <select name="pdca[]" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="a">a</option>
                            <option value="b">b</option>
                            <option value="c">c</option>
                        </select>
                    </div>
                    <label for="colFormLabelSm" style="font-size:11px;" class="col-sm-1 col-form-label col-form-label-sm">BSC</label>
                    <div class="col-sm-2">
                        <select name="bsc[]" class="form-control form-control-sm" required>
                            <option value="" selected disabled>--- Belum dipilih ---</option>
                            <option value="a">a</option>
                            <option value="b">b</option>
                            <option value="c">c</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="btn btn-danger btn-sm removeDescriptionInput" data-number="${aa}">
                            <i class="fa-solid fa-minus"></i>
                        </button>
                    </div>
                </div>
            `);

            $('.removeDescriptionInput').click(function (e) {
                e.preventDefault();

                var number = $(this).data('number');

                $(`#descriptionClone${number}`).remove();

                aa--;
            });
        });
    });
    // !SECTION tambah input kueri deskripsi
</script>
