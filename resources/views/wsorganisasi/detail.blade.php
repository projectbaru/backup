<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Organisasi</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">

</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
					<a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                    <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                    <a class="collapse-item active" href="{{route('list_o')}}" style="background-color:#1053af;color:white;">Organisasi</a>
                    <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                    <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                    <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                    <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                    <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                    <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                    <a class="collapse-item" href="{{route('list_usm')}}">Grup Lokasi Kerja</a> 
                    <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                    <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                    <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                    <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                    <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                    <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a>
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
			<br>
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style=""><br>
					<div class="container" style="width:90%; ">
						<div>
							<div class="container mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
                            <h5 style="color:black;">Detail Organisasi</h5>	
                            <hr>	
                            <form action="{{route('simpan_o')}}" method="post"> {{ csrf_field() }}
                            @foreach($wsorganisasi as $data)
                                <table>
                                    <tr>
                                        <td><a class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('edit_o', $data->id_organisasi)}}">Ubah</a></td>
                                        <td><button type="submit" class="btn btn-success btn-sm" style="font-size:11px;border-radius:5px;">Salin</button></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('hapus_o', $data->id_organisasi)}}">Hapus</a></td>
                                        <td><a class="btn btn-danger btn-sm" style="font-size:11px;border-radius:5px;" href="{{route('list_o')}}">Batal</a></td>
                                    </tr>
                                </table><br>
                                <div class="container-fluid mt-2" style="">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col" >
                                                <table style="font-size:12px;">
                                                    <tr>
                                                        <td>Nama Perusahaan</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="nama_perusahaan" value="{{$data->nama_perusahaan}}">{{$data->nama_perusahaan}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kode Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="kode_organisasi" value="{{$data->kode_organisasi}}">{{$data->kode_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nama Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="nama_organisasi" value="{{$data->nama_organisasi}}">{{$data->nama_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tipe Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="tipe_area" value="{{$data->tipe_area}}">{{$data->tipe_area}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Grup Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="grup_organisasi" value="{{$data->grup_organisasi}}">{{$data->grup_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kode Pusat Biaya</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="kode_pusat_biaya" value="{{$data->kode_pusat_biaya}}">{{$data->kode_pusat_biaya}}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col" style="font-size:12px;">
                                                <table style="font-size:12px;">
                                                    <tr>
                                                        <td>Tanggal Mulai</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="tanggal_mulai" value="{{$data->tanggal_mulai}}">{{$data->tanggal_mulai}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Selesai</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="tanggal_selesai" value="{{$data->tanggal_selesai}}">{{$data->tanggal_selesai}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>ID Induk Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="id_induk_organisasi" value="{{$data->id_induk_organisasi}}">{{$data->id_induk_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Induk Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="induk_organisasi" value="{{$data->induk_organisasi}}">{{$data->induk_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Unit Kerja</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="unit_kerja" value="{{$data->unit_kerja}}">{{$data->unit_kerja}}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <hr>
                                        <b style="color:black;">Informasi Lainnya</b>
                                        <div class="row">
                                            <div class="col">
                                                <table style="font-size:12px;">
                                                    <tr>
                                                        <td>Nama Struktur Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="nama_struktur_organisasi" value="{{$data->nama_struktur_organisasi}}">{{$data->nama_struktur_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Versi Struktur Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="versi_struktur_organisasi" value="{{$data->versi_struktur_organisasi}}">{{$data->versi_struktur_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kode Tingkat Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="kode_tingkat_organisasi" value="{{$data->kode_tingkat_organisasi}}">{{$data->kode_tingkat_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor Indeks Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="nomor_indeks_organisasi" value="{{$data->nomor_indeks_organisasi}}">{{$data->nomor_indeks_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Urutan Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="urutan_organisasi" value="{{$data->urutan_organisasi}}">{{$data->urutan_organisasi}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Keterangan Organisasi</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="keterangan_organisasi" value="{{$data->keterangan_organisasi}}">{{$data->keterangan_organisasi}}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col">
                                                <table style="font-size:12px;">
                                                    <tr>
                                                        <td>Leher Struktur</td>
                                                        <td>:</td>
                                                        <td><input type="checkbox" onclick="return false" name="leher_struktur" value="{{$data->leher_struktur}}" {{ $data->leher_struktur == 'Yes' ? 'checked' : NULL }}></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Aktif</td>
                                                        <td>:</td>
                                                        <td><input type="checkbox" name="aktif" onclick="return false"  value="{{$data->aktif}}" {{ $data->aktif == 'Yes' ? 'checked' : NULL }}></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <hr>
                                        <b style="color:black;">Rekaman Informasi</b>
                                        <div class="row">
                                            <div class="col">
                                                <table style="font-size:12px;">
                                                    <tr>
                                                        <td>Tanggal Mulai Efektif</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="tanggal_mulai_efektif" value="{{$data->tanggal_mulai_efektif}}">{{$data->tanggal_mulai_efektif}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Selesai Efektif</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="tanggal_selesai_efektif" value="{{$data->tanggal_selesai_efektif}}">{{$data->tanggal_selesai_efektif}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Keterangan</td>
                                                        <td>:</td>
                                                        <td><input type="hidden" name="keterangan" value="{{$data->keterangan}}">{{$data->keterangan}}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <br>
                                @endforeach
                            </form>
							</div>
							<br>

                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>