<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Organisasi</title>
<link href="{{ asset('css/all.min.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" rel="stylesheet">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
</head>
<body id="page-top">
<div id="wrapper">
	<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: black; ">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<div class="sidebar-brand-icon rotate-n-15">
			<!-- <i class="fas fa-laugh-wink"></i> -->
		</div>
		<div class="sidebar-brand-text mx-3"></div>
		</a>
		<hr class="sidebar-divider my-0">
		<li class="nav-item active">
			<a class="nav-link" href="index.html">
			<!-- <i class="fas fa-fw fa-tachometer-alt"></i> -->
			<span>Dashboard</span>
			</a>
		</li>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">Interface</div>
		<li class="nav-item">
			<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
			<!-- <i class="fas fa-fw fa-cog"></i> -->
			<span>Master WS</span>
			</a>
			<div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
				<div class="bg-white py-2 collapse-inner rounded" style="font-size:11px;">
					<h6 class="collapse-header" style="color:black;">Menu Master_</h6>
						<a class="collapse-item" href="{{route('list_p')}}">Perusahaan</a>
                        <a class="collapse-item" href="{{route('list_a')}}">Alamat</a>
                        <a class="collapse-item active" href="{{route('list_o')}}" style="background-color:#1053af;color:white;">Organisasi</a>
                        <a class="collapse-item" href="{{route('list_to')}}">Tingkat organisasi</a>
                        <a class="collapse-item" href="{{route('list_g')}}">Golongan</a>
                        <a class="collapse-item" href="{{route('list_tg')}}">Tingkat Golongan</a>
                        <a class="collapse-item" href="{{route('list_po')}}">Posisi</a>
                        <a class="collapse-item" href="{{route('list_tp')}}">Tingkat Posisi</a>
                        <a class="collapse-item" href="{{route('list_lk')}}">Lokasi Kerja</a>
                        <a class="collapse-item" href="{{route('list_glk')}}">Grup Lokasi Kerja</a> 
                        <a class="collapse-item" href="{{route('list_kc')}}">Kelas Cabang</a>
                        <a class="collapse-item" href="{{route('list_kac')}}">Kantor Cabang</a>
                        <a class="collapse-item" href="{{route('list_dp')}}">Dekripsi Pekerjaan</a>
                        <a class="collapse-item" href="{{route('list_j')}}">Jabatan</a>
                        <a class="collapse-item" href="{{route('list_kj')}}">Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_tkj')}}">Tingkat Kelompok Jabatan</a>
                        <a class="collapse-item" href="{{route('list_usm')}}">Upah Standar Minimum</a> 
				</div>
			</div>
		</li>
		<hr class="sidebar-divider d-none d-md-block">
		<div class="text-center d-none d-md-inline">
			<button class="rounded-circle border-0" id="sidebarToggle"></button>
		</div>
	</ul>
	<div id="content-wrapper" class="d-flex flex-column" style="margin-top: -20px;">
		<div id="content">
            <br>
			<div class="container-fluid" style="background-color:white;">
				<div class="relative flex items-top justify-center min-h-screen sm:items-center" style="">			
					<div class="container" style="width:100%; ">
						<div>
							<br>
							<div class="container-fluid mt-2" style="border:1px solid #d9d9d9;padding-top:40px;border-radius:10px;" >
								<div class="container" style="">
                                    <h5><b style="color:black;">Ubah Organisasi</b></h5>
                                </div>   
                                <form action="{{route('update_o')}}" method="post" enctype="multipart/form-data">{{ csrf_field() }}
                                    <hr>
                                    <div class="form-group container" style="width:100%;">
                                        <div class="">
                                            @foreach($wsorganisasi as $data)
                                            <div class="row">
                                                <div class="col">
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm"></label>
                                                            <div class="col-sm-10">
                                                            <input type="hidden"  name="id_organisasi" value="{{ $data->id_organisasi }}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                            
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Perusahaan</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" required name="nama_perusahaan" class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->nama_perusahaan}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Organisasi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="kode_organisasi" readonly class="form-control form-control-sm" style="font-size:11px;" id="colFormLabelSm" placeholder="" value="{{$data->kode_organisasi}}">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Organisasi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" required name="nama_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->nama_organisasi}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tipe Organisasi</label>
                                                            <div class="col-sm-10">
                                                            <select name="tipe_area" class="form-control form-control-sm vendorId" >
                                                            <option value=""></option>
                                                            @foreach ($datas['looks'] as $v)
                                                                <option value="{{$v->tipe_area}}" {{ $v->tipe_area == $data->tipe_area ? 'selected' : NULL }}>{{$v->tipe_area}}</option>
                                                            @endforeach
                                                        </select>
                                                            <!-- <input type="text" required name="tipe_area" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tipe_area}}" id="colFormLabelSm" placeholder=""></textarea> -->
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Grup Organisasi</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="grup_organisasi" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->grup_organisasi}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Pusat Biaya</label>
                                                            <div class="col-sm-10">
                                                            <input type="text" name="kode_pusat_biaya" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->kode_pusat_biaya}}" id="colFormLabelSm" placeholder="">
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" required name="tanggal_mulai" class="form-control form-control-sm" style="font-size:11px;" value="{{$data->tanggal_mulai}}" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" name="tanggal_selesai" class="form-control form-control-sm" value="{{$data->tanggal_selesai}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">ID Induk Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" name="id_induk_organisasi" class="form-control form-control-sm" value="{{$data->id_induk_organisasi}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Induk Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" name="induk_organisasi" class="form-control form-control-sm" id="colFormLabelSm" value="{{$data->induk_organisasi}}" style="font-size:11px;" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Unit Kerja</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" required name="unit_kerja" class="form-control form-control-sm" value="{{$data->unit_kerja}}" style="font-size:11px;" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <hr>
                                            <b>Informasi Lainnya</b>
                                            <br>
                                            <br>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nama Struktur Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" required name="nama_struktur_organisasi" style="font-size:11px;" value="{{$data->nama_struktur_organisasi}}" class="form-control form-control-sm" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Versi Struktur Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <input type="text" required name="versi_struktur_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->versi_struktur_organisasi}}" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Kode Tingkat Organisasi</label>
                                                        &nbsp;&nbsp;&nbsp;<select name="tipe_area" class="form-control form-control-sm vendorId" style="width:20%;">
                                                            <option value=""></option>
                                                            @foreach ($datakto['looks'] as $v)
                                                                <option value="{{$v->kode_tingkat_organisasi}}" {{ $v->kode_tingkat_organisasi == $data->kode_tingkat_organisasi ? 'selected' : NULL }}>{{$v->kode_tingkat_organisasi}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Nomor Indeks Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <input type="number" required name="nomor_indeks_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->nomor_indeks_organisasi}}" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Urutan Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <input type="number" required name="urutan_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->urutan_organisasi}}" id="colFormLabelSm" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan Organisasi</label>
                                                        <div class="col-sm-10">
                                                        <textarea type="text" name="keterangan_organisasi" style="font-size:11px;" class="form-control form-control-sm" value="{{$data->keterangan_organisasi}}" rows="4" cols="50" id="colFormLabelSm" placeholder="">{{$data->keterangan_organisasi}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Leher Struktur</label>
                                                        <div class="col-sm-5">
                                                        <input type="checkbox" name="leher_struktur" style="font-size:11px;" class="" id="colFormLabelSm" value="{{$data->leher_struktur}}" {{ $data->leher_struktur == 'Yes' ? 'checked' : NULL }}>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Aktif</label>
                                                        <div class="col-sm-5">
                                                        <input type="checkbox" name="aktif" class="" id="colFormLabelSm" style="font-size:11px;" value="{{$data->aktif}}" {{ $data->aktif == 'Yes' ? 'checked' : NULL }}>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                            <hr>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Mulai Efektif</label>
                                                        <div class="col-sm-5">
                                                        <input type="date" required name="tanggal_mulai_efektif" id="colFormLabelSm" class="form-control form-control-sm" value="{{$data->tanggal_mulai_efektif}}" style="font-size:11px;" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Tanggal Selesai Efektif</label>
                                                        <div class="col-sm-5">
                                                        <input type="date" name="tanggal_selesai_efektif" id="colFormLabelSm" class="form-control form-control-sm" value="{{$data->tanggal_selesai_efektif}}" style="font-size:11px;" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="colFormLabelSm" class="col-sm-2 col-form-label col-form-label-sm" style="font-size:11px;">Keterangan</label>
                                                        <div class="col-sm-5">
                                                        <textarea type="text" name="" id="colFormLabelSm" class="form-control form-control-sm" style="font-size:11px;" rows="4" cols="50" value="{{$data->keterangan}}" placeholder="">{{$data->keterangan}}</textarea>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            </div>
                                            @endforeach
                                            <div >
                                                <table>
                                                    <tr>
            
                                                    <!-- <td>
                                                        <a href="" class="btn">Hapus</a>
                                                    </td> -->
                                                        <td>
                                                            <button type="submit" class="btn btn-primary btn-sm" style="font-size:11px;border:none;border-radius:5px;">Simpan</button>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-danger btn-sm" href="{{route('hapus_o',$data->id_organisasi)}}" class="btn" style="font-size:11px;border:none;border-radius:5px;">Hapus</a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('list_o') }}" class="btn btn-danger btn-sm" style="font-size:11px;border:none;border-radius:5px;">Batal</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
							<br>
                        </div>
					</div>
					<footer class="sticky-footer bg-white">
					<div class="container my-auto">
						<div class="copyright text-center my-auto">
							<!-- <span>Copyright &copy; Your Website 2021</span> -->
						</div>
					</div>
					</footer>
				</div>
			</div>
			<a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>
			<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
							<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body">
							Select "Logout" below if you are ready to end your current session.
						</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
							<a class="btn btn-primary" href="login.html">Logout</a>
						</div>
					</div>
				</div>
			</div>
			<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
			<script src="{{ asset('js/jquery.min.js') }}"></script>
			<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
			</body>
			</html>