-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Jul 2022 pada 14.09
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_hris_proses`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsalamatperusahaan`
--

CREATE TABLE `wsalamatperusahaan` (
  `id_alamat` int(11) NOT NULL,
  `kode_alamat` varchar(20) DEFAULT NULL,
  `kode_perusahaan` varchar(20) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `status_alamat` text DEFAULT NULL,
  `jenis_alamat` text DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `kota` tinytext DEFAULT NULL,
  `kode_pos` int(11) DEFAULT NULL,
  `telepon` int(11) DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan`
--

CREATE TABLE `wsdeskripsipekerjaan` (
  `id_deskripsi_pekerjaan` int(11) NOT NULL,
  `kode_deskripsi` text DEFAULT NULL,
  `nama_posisi` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `deskripsi_pekerjaan` longtext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` date DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `nomor_dokumen` varchar(50) DEFAULT NULL,
  `edisi` text DEFAULT NULL,
  `tanggal_edisi` date DEFAULT NULL,
  `nomor_revisi` text DEFAULT NULL,
  `tanggal_revisi` date DEFAULT NULL,
  `nama_jabatan` text DEFAULT NULL,
  `nama_karyawan` text DEFAULT NULL,
  `divisi` text DEFAULT NULL,
  `lokasi_kerja` text DEFAULT NULL,
  `dibuat_oleh` date DEFAULT NULL,
  `diperiksa_oleh` text DEFAULT NULL,
  `fungsi_jabatan` text DEFAULT NULL,
  `tanggung_jawab` longtext DEFAULT NULL,
  `departemen` text DEFAULT NULL,
  `kode_posisi` text DEFAULT NULL,
  `nama_lokasi_kerja` text DEFAULT NULL,
  `nama_pengawas` text DEFAULT NULL,
  `lingkup_aktivitas` text DEFAULT NULL,
  `id_logo_perusahaan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_bag`
--

CREATE TABLE `wsdeskripsipekerjaan_bag` (
  `id_deskripsi_pekerjaan_bag` int(11) NOT NULL,
  `kode_posisi` tinytext DEFAULT NULL,
  `nama_posisi` tinytext DEFAULT NULL,
  `deskripsi_pekerjaan` tinytext DEFAULT NULL,
  `pdca` tinytext DEFAULT NULL,
  `bsc` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_deskripsi_pekerjaan`
--

CREATE TABLE `wsdeskripsipekerjaan_deskripsi_pekerjaan` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `deskripsi_pekerjaan` text NOT NULL,
  `pdca` varchar(255) NOT NULL,
  `bsc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_kualifikasi_jabatan`
--

CREATE TABLE `wsdeskripsipekerjaan_kualifikasi_jabatan` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `kualifikasi_jabatan` text NOT NULL,
  `tipe_kualifikasi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_tanggung_jawab`
--

CREATE TABLE `wsdeskripsipekerjaan_tanggung_jawab` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `tanggung_jawab` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsdeskripsipekerjaan_wewenang`
--

CREATE TABLE `wsdeskripsipekerjaan_wewenang` (
  `id` int(11) NOT NULL,
  `wsdeskripsipekerjaan_id` int(11) NOT NULL,
  `wewenang` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsgolongan`
--

CREATE TABLE `wsgolongan` (
  `id_golongan` int(11) NOT NULL,
  `kode_golongan` tinytext DEFAULT NULL,
  `nama_golongan` tinytext DEFAULT NULL,
  `tingkat_golongan` tinytext DEFAULT NULL,
  `urutan_golongan` tinytext DEFAULT NULL,
  `nama_perusahaan` tinyint(100) DEFAULT NULL,
  `tingkat_posisi` tinytext DEFAULT NULL,
  `jabatan` tinytext DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` varchar(50) DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsgruplokasikerja`
--

CREATE TABLE `wsgruplokasikerja` (
  `id_grup_lokasi_kerja` int(11) NOT NULL,
  `kode_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `tipe_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `lokasi_kerja` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsgruplokasikerja_deskripsi`
--

CREATE TABLE `wsgruplokasikerja_deskripsi` (
  `id_grup_lokasi_kerja` int(11) NOT NULL,
  `kode_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `tipe_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `lokasi_kerja` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsjabatan`
--

CREATE TABLE `wsjabatan` (
  `id_jabatan` int(11) NOT NULL,
  `kode_jabatan` tinytext DEFAULT NULL,
  `nama_jabatan` tinytext DEFAULT NULL,
  `tipe_jabatan` tinytext DEFAULT NULL,
  `kode_kelompok_jabatan` varchar(25) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(30) DEFAULT NULL,
  `grup_jabatan` varchar(30) DEFAULT NULL,
  `deskripsi_jabatan` longtext DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `magang` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` tinytext DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` tinytext DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` tinytext DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wskantorcabang`
--

CREATE TABLE `wskantorcabang` (
  `id_kantor` int(11) NOT NULL,
  `kode_kantor` tinytext DEFAULT NULL,
  `nama_kantor` tinytext DEFAULT NULL,
  `kode_kelas` tinytext DEFAULT NULL,
  `tipe_kantor` tinytext DEFAULT NULL,
  `kode_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_lokasi_kerja` tinytext DEFAULT NULL,
  `nomor_npwp` tinytext DEFAULT NULL,
  `umk_ump` int(11) DEFAULT NULL,
  `ttd_spt` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wskelascabang`
--

CREATE TABLE `wskelascabang` (
  `id_kelas_cabang` int(11) NOT NULL,
  `kode_kelas_cabang` tinytext DEFAULT NULL,
  `nama_kelas_cabang` tinytext DEFAULT NULL,
  `area_kelas_cabang` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wskelompokjabatan`
--

CREATE TABLE `wskelompokjabatan` (
  `id_kelompok_jabatan` int(11) NOT NULL,
  `kode_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_kelompok_jabatan` tinytext DEFAULT NULL,
  `deskripsi_kelompok_jabatan` text DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `grup_jabatan` varchar(50) DEFAULT NULL,
  `tipe_jabatan` varchar(50) DEFAULT NULL,
  `deskripsi_pekerjaan` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` tinytext DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` tinytext DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` tinytext DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wslokasikerja`
--

CREATE TABLE `wslokasikerja` (
  `id_lokasi_kerja` int(11) NOT NULL,
  `kode_lokasi_kerja` tinytext DEFAULT NULL,
  `nama_lokasi_kerja` tinytext DEFAULT NULL,
  `id_grup_lokasi_kerja` tinytext DEFAULT NULL,
  `id_kantor` tinytext DEFAULT NULL,
  `zona_waktu` int(11) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `provinsi` tinytext DEFAULT NULL,
  `kecamatan` tinytext DEFAULT NULL,
  `kelurahan` tinytext DEFAULT NULL,
  `kabupaten_kota` text DEFAULT NULL,
  `kode_pos` int(11) DEFAULT NULL,
  `negara` tinytext DEFAULT NULL,
  `nomor_telepon` int(50) DEFAULT NULL,
  `email` tinytext DEFAULT NULL,
  `fax` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsorganisasi`
--

CREATE TABLE `wsorganisasi` (
  `id_organisasi` int(11) NOT NULL,
  `nama_perusahaan` tinytext DEFAULT NULL,
  `kode_organisasi` tinytext DEFAULT NULL,
  `nama_organisasi` tinytext DEFAULT NULL,
  `tipe_area` text DEFAULT NULL,
  `grup_organisasi` text DEFAULT NULL,
  `unit_kerja` tinytext DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `id_induk_organisasi` longtext DEFAULT NULL,
  `induk_organisasi` longtext DEFAULT NULL,
  `nama_struktur_organisasi` tinytext DEFAULT NULL,
  `versi_struktur_organisasi` tinytext DEFAULT NULL,
  `kode_tingkat_organisasi` tinytext DEFAULT NULL,
  `kode_pusat_biaya` tinytext DEFAULT NULL,
  `nomor_indeks_organisasi` int(11) DEFAULT NULL,
  `tingkat_organisasi` int(11) DEFAULT NULL,
  `urutan_organisasi` int(11) DEFAULT NULL,
  `keterangan_organisasi` longtext DEFAULT NULL,
  `leher_struktur` text DEFAULT NULL,
  `aktif` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsperusahaan`
--

CREATE TABLE `wsperusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `perusahaan_logo` text DEFAULT NULL,
  `nama_perusahaan` text DEFAULT NULL,
  `kode_perusahaan` text DEFAULT NULL,
  `singkatan` text DEFAULT NULL,
  `visi_perusahaan` text DEFAULT NULL,
  `misi_perusahaan` longtext DEFAULT NULL,
  `nilai_perusahaan` longtext DEFAULT NULL,
  `keterangan_perusahaan` longtext DEFAULT NULL,
  `tanggal_mulai_perusahaan` date DEFAULT NULL,
  `tanggal_selesai_perusahaan` date DEFAULT NULL,
  `jenis_perusahaan` text DEFAULT NULL,
  `jenis_bisnis_perusahaan` text DEFAULT NULL,
  `jumlah_perusahaan` text DEFAULT NULL,
  `nomor_npwp_perusahaan` text DEFAULT NULL,
  `lokasi_pajak` text DEFAULT NULL,
  `npp` text DEFAULT NULL,
  `npkp` text DEFAULT NULL,
  `id_logo_perusahaan` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` int(11) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `jumlah_karyawan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsposisi`
--

CREATE TABLE `wsposisi` (
  `id_posisi` int(11) NOT NULL,
  `kode_perusahaan` varchar(20) DEFAULT NULL,
  `nama_perusahaan` varchar(50) DEFAULT NULL,
  `kode_posisi` tinytext DEFAULT NULL,
  `nama_posisi` tinytext DEFAULT NULL,
  `kode_mpp` varchar(15) DEFAULT NULL,
  `tingkat_posisi` text DEFAULT NULL,
  `detail_tingkat_posisi` varchar(25) DEFAULT NULL,
  `kode_jabatan` varchar(25) DEFAULT NULL,
  `nama_jabatan` tinytext DEFAULT NULL,
  `nama_organisasi` tinytext DEFAULT NULL,
  `tingkat_organisasi` tinytext DEFAULT NULL,
  `tipe_posisi` tinytext DEFAULT NULL,
  `deskripsi_pekerjaan_posisi` varchar(1000) DEFAULT NULL,
  `kode_posisi_atasan` varchar(25) DEFAULT NULL,
  `nama_posisi_atasan` varchar(25) DEFAULT NULL,
  `tipe_area` tinytext DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `posisi_aktif` varchar(50) DEFAULT NULL,
  `komisi` varchar(50) DEFAULT NULL,
  `kepala_fungsional` varchar(50) DEFAULT NULL,
  `flag_operasional` varchar(50) DEFAULT NULL,
  `status_posisi` varchar(50) DEFAULT NULL,
  `nomor_surat` tinytext DEFAULT NULL,
  `jumlah_karyawan_dengan_posisi_ini` int(11) DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wsstandarupahminimum`
--

CREATE TABLE `wsstandarupahminimum` (
  `id_standar_upah_minimum` int(11) NOT NULL,
  `kode_posisi` varchar(20) DEFAULT NULL,
  `nama_posisi` varchar(20) DEFAULT NULL,
  `tingkat_pendidikan` varchar(20) DEFAULT NULL,
  `kode_lokasi` varchar(10) DEFAULT NULL,
  `nama_lokasi` varchar(50) DEFAULT NULL,
  `upah_minimum` int(8) DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser`
--

CREATE TABLE `wstampilantabledashboarduser` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_address_dua`
--

CREATE TABLE `wstampilantabledashboarduser_address_dua` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_alamat` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_alamat`
--

CREATE TABLE `wstampilantabledashboarduser_alamat` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_alamat` varchar(255) DEFAULT NULL,
  `kode_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_dp`
--

CREATE TABLE `wstampilantabledashboarduser_dp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_glk`
--

CREATE TABLE `wstampilantabledashboarduser_glk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_grup_lokasi_kerja` varchar(255) DEFAULT NULL,
  `nama_grup_lokasi_kerja` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_golongan`
--

CREATE TABLE `wstampilantabledashboarduser_golongan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_golongan` varchar(255) DEFAULT NULL,
  `nama_golongan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_hdp`
--

CREATE TABLE `wstampilantabledashboarduser_hdp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_jabatan`
--

CREATE TABLE `wstampilantabledashboarduser_jabatan` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_jabatan` varchar(255) DEFAULT NULL,
  `nama_jabatan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_kac`
--

CREATE TABLE `wstampilantabledashboarduser_kac` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kantor` varchar(255) DEFAULT NULL,
  `nama_kantor` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_kc`
--

CREATE TABLE `wstampilantabledashboarduser_kc` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kelas_cabang` varchar(255) DEFAULT NULL,
  `nama_kelas_cabang` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_kj`
--

CREATE TABLE `wstampilantabledashboarduser_kj` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_kelompok_jabatan` varchar(255) DEFAULT NULL,
  `nama_kelompok_jabatan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_lk`
--

CREATE TABLE `wstampilantabledashboarduser_lk` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_lokasi_kerja` varchar(255) DEFAULT NULL,
  `nama_lokasi_kerja` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_organisasi`
--

CREATE TABLE `wstampilantabledashboarduser_organisasi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_organisasi` varchar(255) DEFAULT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_po`
--

CREATE TABLE `wstampilantabledashboarduser_po` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_posisi`
--

CREATE TABLE `wstampilantabledashboarduser_posisi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tg`
--

CREATE TABLE `wstampilantabledashboarduser_tg` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `nama_tingkat_golongan` tinytext DEFAULT NULL,
  `kode_tingkat_golongan` tinytext DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tingkatorganisasi`
--

CREATE TABLE `wstampilantabledashboarduser_tingkatorganisasi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_organisasi` tinytext DEFAULT NULL,
  `nama_tingkat_organisasi` tinytext DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tkj`
--

CREATE TABLE `wstampilantabledashboarduser_tkj` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_to`
--

CREATE TABLE `wstampilantabledashboarduser_to` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_organisasi` varchar(255) DEFAULT NULL,
  `nama_tingkat_organisasi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_tp`
--

CREATE TABLE `wstampilantabledashboarduser_tp` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_tingkat_posisi` varchar(255) DEFAULT NULL,
  `nama_tingkat_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstampilantabledashboarduser_usm`
--

CREATE TABLE `wstampilantabledashboarduser_usm` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `select` longtext NOT NULL,
  `kode_posisi` varchar(255) DEFAULT NULL,
  `nama_posisi` varchar(255) DEFAULT NULL,
  `query_field` longtext DEFAULT NULL,
  `query_operator` longtext DEFAULT NULL,
  `query_value` longtext DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatgolongan`
--

CREATE TABLE `wstingkatgolongan` (
  `id_tingkat_golongan` int(11) NOT NULL,
  `nama_perusahaan` tinytext DEFAULT NULL,
  `kode_tingkat_golongan` varchar(25) DEFAULT NULL,
  `nama_tingkat_golongan` tinytext DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `kode_golongan` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` tinytext DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL,
  `items` longtext DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatgolongan_item`
--

CREATE TABLE `wstingkatgolongan_item` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatkelompokjabatan`
--

CREATE TABLE `wstingkatkelompokjabatan` (
  `id_tingkat_kelompok_jabatan` int(11) NOT NULL,
  `kode_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_tingkat_kelompok_jabatan` tinytext DEFAULT NULL,
  `kode_kelompok_jabatan` tinytext DEFAULT NULL,
  `nama_kelompok_jabatan` tinytext DEFAULT NULL,
  `dari_golongan` tinytext DEFAULT NULL,
  `sampai_golongan` tinytext DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` date DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatorganisasi`
--

CREATE TABLE `wstingkatorganisasi` (
  `id_tingkat_organisasi` int(11) NOT NULL,
  `kode_tingkat_organisasi` tinytext DEFAULT NULL,
  `nama_tingkat_organisasi` tinytext DEFAULT NULL,
  `urutan_tingkat` int(11) DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status_rekaman` varchar(50) DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` varchar(50) DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` varchar(50) DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` varchar(50) DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wstingkatposisi`
--

CREATE TABLE `wstingkatposisi` (
  `id_tingkat_posisi` int(11) NOT NULL,
  `kode_tingkat_posisi` text DEFAULT NULL,
  `nama_tingkat_posisi` text DEFAULT NULL,
  `urutan_tingkat` int(11) DEFAULT NULL,
  `urutan_tampilan` int(11) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status_rekaman` text DEFAULT NULL,
  `tanggal_mulai_efektif` date DEFAULT NULL,
  `tanggal_selesai_efektif` date DEFAULT NULL,
  `pengguna_masuk` text DEFAULT NULL,
  `waktu_masuk` date DEFAULT NULL,
  `pengguna_ubah` text DEFAULT NULL,
  `waktu_ubah` date DEFAULT NULL,
  `pengguna_hapus` text DEFAULT NULL,
  `waktu_hapus` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`) USING BTREE;

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`) USING BTREE;

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`) USING BTREE,
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`) USING BTREE;

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`) USING BTREE;

--
-- Indeks untuk tabel `wsalamatperusahaan`
--
ALTER TABLE `wsalamatperusahaan`
  ADD PRIMARY KEY (`id_alamat`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan`
  ADD PRIMARY KEY (`id_deskripsi_pekerjaan`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_bag`
--
ALTER TABLE `wsdeskripsipekerjaan_bag`
  ADD PRIMARY KEY (`id_deskripsi_pekerjaan_bag`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_deskripsi_pekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan_deskripsi_pekerjaan`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_kualifikasi_jabatan`
--
ALTER TABLE `wsdeskripsipekerjaan_kualifikasi_jabatan`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_tanggung_jawab`
--
ALTER TABLE `wsdeskripsipekerjaan_tanggung_jawab`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsdeskripsipekerjaan_wewenang`
--
ALTER TABLE `wsdeskripsipekerjaan_wewenang`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `wsdeskripsipekerjaan_id` (`wsdeskripsipekerjaan_id`) USING BTREE;

--
-- Indeks untuk tabel `wsgolongan`
--
ALTER TABLE `wsgolongan`
  ADD PRIMARY KEY (`id_golongan`) USING BTREE;

--
-- Indeks untuk tabel `wsgruplokasikerja`
--
ALTER TABLE `wsgruplokasikerja`
  ADD PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE;

--
-- Indeks untuk tabel `wsgruplokasikerja_deskripsi`
--
ALTER TABLE `wsgruplokasikerja_deskripsi`
  ADD PRIMARY KEY (`id_grup_lokasi_kerja`) USING BTREE;

--
-- Indeks untuk tabel `wsjabatan`
--
ALTER TABLE `wsjabatan`
  ADD PRIMARY KEY (`id_jabatan`) USING BTREE;

--
-- Indeks untuk tabel `wskantorcabang`
--
ALTER TABLE `wskantorcabang`
  ADD PRIMARY KEY (`id_kantor`) USING BTREE;

--
-- Indeks untuk tabel `wskelascabang`
--
ALTER TABLE `wskelascabang`
  ADD PRIMARY KEY (`id_kelas_cabang`) USING BTREE;

--
-- Indeks untuk tabel `wskelompokjabatan`
--
ALTER TABLE `wskelompokjabatan`
  ADD PRIMARY KEY (`id_kelompok_jabatan`) USING BTREE;

--
-- Indeks untuk tabel `wslokasikerja`
--
ALTER TABLE `wslokasikerja`
  ADD PRIMARY KEY (`id_lokasi_kerja`) USING BTREE;

--
-- Indeks untuk tabel `wsorganisasi`
--
ALTER TABLE `wsorganisasi`
  ADD PRIMARY KEY (`id_organisasi`) USING BTREE;

--
-- Indeks untuk tabel `wsperusahaan`
--
ALTER TABLE `wsperusahaan`
  ADD PRIMARY KEY (`id_perusahaan`) USING BTREE;

--
-- Indeks untuk tabel `wsposisi`
--
ALTER TABLE `wsposisi`
  ADD PRIMARY KEY (`id_posisi`) USING BTREE;

--
-- Indeks untuk tabel `wsstandarupahminimum`
--
ALTER TABLE `wsstandarupahminimum`
  ADD PRIMARY KEY (`id_standar_upah_minimum`);

--
-- Indeks untuk tabel `wstampilantabledashboarduser`
--
ALTER TABLE `wstampilantabledashboarduser`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_address_dua`
--
ALTER TABLE `wstampilantabledashboarduser_address_dua`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_alamat`
--
ALTER TABLE `wstampilantabledashboarduser_alamat`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_dp`
--
ALTER TABLE `wstampilantabledashboarduser_dp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_glk`
--
ALTER TABLE `wstampilantabledashboarduser_glk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_golongan`
--
ALTER TABLE `wstampilantabledashboarduser_golongan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_hdp`
--
ALTER TABLE `wstampilantabledashboarduser_hdp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_jabatan`
--
ALTER TABLE `wstampilantabledashboarduser_jabatan`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_kac`
--
ALTER TABLE `wstampilantabledashboarduser_kac`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_kc`
--
ALTER TABLE `wstampilantabledashboarduser_kc`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_kj`
--
ALTER TABLE `wstampilantabledashboarduser_kj`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_lk`
--
ALTER TABLE `wstampilantabledashboarduser_lk`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_organisasi`
--
ALTER TABLE `wstampilantabledashboarduser_organisasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_po`
--
ALTER TABLE `wstampilantabledashboarduser_po`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_posisi`
--
ALTER TABLE `wstampilantabledashboarduser_posisi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tg`
--
ALTER TABLE `wstampilantabledashboarduser_tg`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tingkatorganisasi`
--
ALTER TABLE `wstampilantabledashboarduser_tingkatorganisasi`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tkj`
--
ALTER TABLE `wstampilantabledashboarduser_tkj`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_to`
--
ALTER TABLE `wstampilantabledashboarduser_to`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_tp`
--
ALTER TABLE `wstampilantabledashboarduser_tp`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstampilantabledashboarduser_usm`
--
ALTER TABLE `wstampilantabledashboarduser_usm`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatgolongan`
--
ALTER TABLE `wstingkatgolongan`
  ADD PRIMARY KEY (`id_tingkat_golongan`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatgolongan_item`
--
ALTER TABLE `wstingkatgolongan_item`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatkelompokjabatan`
--
ALTER TABLE `wstingkatkelompokjabatan`
  ADD PRIMARY KEY (`id_tingkat_kelompok_jabatan`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatorganisasi`
--
ALTER TABLE `wstingkatorganisasi`
  ADD PRIMARY KEY (`id_tingkat_organisasi`) USING BTREE;

--
-- Indeks untuk tabel `wstingkatposisi`
--
ALTER TABLE `wstingkatposisi`
  ADD PRIMARY KEY (`id_tingkat_posisi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsalamatperusahaan`
--
ALTER TABLE `wsalamatperusahaan`
  MODIFY `id_alamat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan`
  MODIFY `id_deskripsi_pekerjaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_bag`
--
ALTER TABLE `wsdeskripsipekerjaan_bag`
  MODIFY `id_deskripsi_pekerjaan_bag` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_deskripsi_pekerjaan`
--
ALTER TABLE `wsdeskripsipekerjaan_deskripsi_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_kualifikasi_jabatan`
--
ALTER TABLE `wsdeskripsipekerjaan_kualifikasi_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_tanggung_jawab`
--
ALTER TABLE `wsdeskripsipekerjaan_tanggung_jawab`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsdeskripsipekerjaan_wewenang`
--
ALTER TABLE `wsdeskripsipekerjaan_wewenang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsgolongan`
--
ALTER TABLE `wsgolongan`
  MODIFY `id_golongan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsgruplokasikerja`
--
ALTER TABLE `wsgruplokasikerja`
  MODIFY `id_grup_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsgruplokasikerja_deskripsi`
--
ALTER TABLE `wsgruplokasikerja_deskripsi`
  MODIFY `id_grup_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsjabatan`
--
ALTER TABLE `wsjabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wskantorcabang`
--
ALTER TABLE `wskantorcabang`
  MODIFY `id_kantor` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wskelascabang`
--
ALTER TABLE `wskelascabang`
  MODIFY `id_kelas_cabang` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wskelompokjabatan`
--
ALTER TABLE `wskelompokjabatan`
  MODIFY `id_kelompok_jabatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wslokasikerja`
--
ALTER TABLE `wslokasikerja`
  MODIFY `id_lokasi_kerja` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsorganisasi`
--
ALTER TABLE `wsorganisasi`
  MODIFY `id_organisasi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsperusahaan`
--
ALTER TABLE `wsperusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsposisi`
--
ALTER TABLE `wsposisi`
  MODIFY `id_posisi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wsstandarupahminimum`
--
ALTER TABLE `wsstandarupahminimum`
  MODIFY `id_standar_upah_minimum` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser`
--
ALTER TABLE `wstampilantabledashboarduser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_address_dua`
--
ALTER TABLE `wstampilantabledashboarduser_address_dua`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_alamat`
--
ALTER TABLE `wstampilantabledashboarduser_alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_dp`
--
ALTER TABLE `wstampilantabledashboarduser_dp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_glk`
--
ALTER TABLE `wstampilantabledashboarduser_glk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_golongan`
--
ALTER TABLE `wstampilantabledashboarduser_golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_hdp`
--
ALTER TABLE `wstampilantabledashboarduser_hdp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_jabatan`
--
ALTER TABLE `wstampilantabledashboarduser_jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_kac`
--
ALTER TABLE `wstampilantabledashboarduser_kac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_kc`
--
ALTER TABLE `wstampilantabledashboarduser_kc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_kj`
--
ALTER TABLE `wstampilantabledashboarduser_kj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_lk`
--
ALTER TABLE `wstampilantabledashboarduser_lk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_organisasi`
--
ALTER TABLE `wstampilantabledashboarduser_organisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_po`
--
ALTER TABLE `wstampilantabledashboarduser_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_posisi`
--
ALTER TABLE `wstampilantabledashboarduser_posisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tg`
--
ALTER TABLE `wstampilantabledashboarduser_tg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tingkatorganisasi`
--
ALTER TABLE `wstampilantabledashboarduser_tingkatorganisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tkj`
--
ALTER TABLE `wstampilantabledashboarduser_tkj`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_to`
--
ALTER TABLE `wstampilantabledashboarduser_to`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_tp`
--
ALTER TABLE `wstampilantabledashboarduser_tp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstampilantabledashboarduser_usm`
--
ALTER TABLE `wstampilantabledashboarduser_usm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstingkatgolongan`
--
ALTER TABLE `wstingkatgolongan`
  MODIFY `id_tingkat_golongan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstingkatgolongan_item`
--
ALTER TABLE `wstingkatgolongan_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstingkatkelompokjabatan`
--
ALTER TABLE `wstingkatkelompokjabatan`
  MODIFY `id_tingkat_kelompok_jabatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstingkatorganisasi`
--
ALTER TABLE `wstingkatorganisasi`
  MODIFY `id_tingkat_organisasi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `wstingkatposisi`
--
ALTER TABLE `wstingkatposisi`
  MODIFY `id_tingkat_posisi` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
